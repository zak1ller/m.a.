//
//  EffectManager.swift
//  unspecified
//
//  Created by Min-Su Kim on 2020/07/15.
//  Copyright © 2020 Min-Su Kim. All rights reserved.
//

import Foundation
import UIKit

class EffectManager {
    enum VibrateType {
        case soft
        case light
        case medium
        case heavy
    }
    
    static func vibrate(style: VibrateType = .medium) {
         var st: UIImpactFeedbackGenerator.FeedbackStyle
    
         switch style {
         case .soft:
             if #available(iOS 13.0, *) {
                 st = .soft
             } else {
                 st = .light
             }
         case .light:
             st = .light
         case .medium:
             st = .medium
         case .heavy:
             st = .heavy
         }

         let lightImpactFeedbackGenerator = UIImpactFeedbackGenerator(style: st)
         lightImpactFeedbackGenerator.impactOccurred()
     }
}
