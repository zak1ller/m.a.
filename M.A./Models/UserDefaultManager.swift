//
//  UserDefaultManager.swift
//  M.A.
//
//  Created by Min-Su Kim on 2020/07/28.
//  Copyright © 2020 Min-Su Kim. All rights reserved.
//

import Foundation

class UserDefaultManager {
    static let shared = UserDefaultManager()
    
    let KEY_LIST_INIT = "keyListInit"
    var isListInit: Bool? {
        get {
            return UserDefaults.standard.bool(forKey: KEY_LIST_INIT)
        } set(value) {
            UserDefaults.standard.set(value, forKey: KEY_LIST_INIT)
        }
    }
    
    let KEY_MEMO_GROUP = "memoGroupKey"
    var memoGroup: String? {
        get {
            return UserDefaults.standard.string(forKey: KEY_MEMO_GROUP)
        } set(value) {
            UserDefaults.standard.setValue(value, forKey: KEY_MEMO_GROUP)
        }
    }
    
    let KEY_MEMO_BASIC_NAME = "memoBasicNameKey"
    var memoBasicName: String? {
        get {
            return UserDefaults.standard.string(forKey: KEY_MEMO_BASIC_NAME)
        } set(value) {
            UserDefaults.standard.setValue(value, forKey: KEY_MEMO_BASIC_NAME)
        }
    }
}

