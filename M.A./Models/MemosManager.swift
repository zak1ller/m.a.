//
//  MemosManager.swift
//  M.A.
//
//  Created by Min-Su Kim on 2020/08/04.
//  Copyright © 2020 Min-Su Kim. All rights reserved.
//

import Foundation

class MemosManager {
    struct Memos {
        var id = ""
        var contents = ""
        var rdate = ""
    }
    
    struct MemoGroup {
        var title = ""
        var date = ""
        var count = 0
    }
    
    static func checkExistMemoGroup(title: String, result: @escaping (_ error: String?, _ isExist: Bool) -> ()) {
        if FirebaseManager.shared.getUid() == nil {
            result(NSLocalizedString("UidError", comment: ""), false)
            return
        }
        
        var dic: [String: Any] = [:]
        dic.updateValue(FirebaseManager.shared.getUid()!, forKey: "uid")
        dic.updateValue(title, forKey: "title")
        
        let urlString = "\(url)/checkExistMemoGroup.php"
        let encodedURL = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: encodedURL!)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        
        do {
            let json_data = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            request.httpBody = json_data
        } catch  {
            result("data error", false)
            return
        }
        
        do {
            let json_data = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            request.httpBody = json_data
        } catch  {
            result("data error", false)
            return
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
            data, response, error in
            if let error = error {
                result(error.localizedDescription, false)
                return
            }
                   
            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                   
                if let resultData = jsonData as? NSDictionary {
                    if let query = resultData["query"] as? String {
                        print(query)
                    }
                    
                    if let error = resultData["error"] as? Int {
                        result(ErrorMessage.shared.translateErrorMessage(code: error), false)
                        return
                    }
                    
                    if let res = resultData["result"] as? Bool {
                        result(nil, res)
                        return
                    }
                }
            } catch {
                result(NSLocalizedString("CatchError", comment: ""), false)
            }
        })
        task.resume()
    }
    
    static func updateGroupName(title: String, newTitle: String, result: @escaping (_ error: String?) -> ()) {
        if FirebaseManager.shared.getUid() == nil {
            result(NSLocalizedString("UidError", comment: ""))
            return
        }
        
        var dic: [String: Any] = [:]
        dic.updateValue(FirebaseManager.shared.getUid()!, forKey: "uid")
        dic.updateValue(title, forKey: "title")
        dic.updateValue(newTitle.trimmingCharacters(in: .whitespacesAndNewlines), forKey: "new_title")
        
        let urlString = "\(url)/updateMemoGroupName.php"
        let encodedURL = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: encodedURL!)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        
        do {
            let json_data = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            request.httpBody = json_data
        } catch  {
            result("data error")
            return
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
            data, response, error in
            if let error = error {
                result(error.localizedDescription)
                return
            }
                   
            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                   
                if let resultData = jsonData as? NSDictionary {
                    if let query = resultData["query"] as? String {
                        print(query)
                    }
                    
                    if let error = resultData["error"] as? Int {
                        result(ErrorMessage.shared.translateErrorMessage(code: error))
                        return
                    }
                    
                    if let _ = resultData["success"] as? Bool {
                        result(nil)
                        return
                    }
                }
            } catch {
                result(NSLocalizedString("CatchError", comment: ""))
            }
        })
        task.resume()
    }
    
    
    static func deleteGroup(title: String, result: @escaping (_ error: String?) -> ()) {
        if FirebaseManager.shared.getUid() == nil {
            result(NSLocalizedString("UidError", comment: ""))
            return
        }
        
        var dic: [String: Any] = [:]
        dic.updateValue(FirebaseManager.shared.getUid()!, forKey: "uid")
        dic.updateValue(title, forKey: "title")
        
        let urlString = "\(url)/deleteMemoGroup.php"
        let encodedURL = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: encodedURL!)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        
        do {
            let json_data = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            request.httpBody = json_data
        } catch  {
            result("data error")
            return
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
            data, response, error in
            if let error = error {
                result(error.localizedDescription)
                return
            }
                   
            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                   
                if let resultData = jsonData as? NSDictionary {
                    if let query = resultData["query"] as? String {
                        print(query)
                    }
                    
                    if let error = resultData["error"] as? Int {
                        result(ErrorMessage.shared.translateErrorMessage(code: error))
                        return
                    }
                    
                    if let _ = resultData["success"] as? Bool {
                        result(nil)
                        return
                    }
                }
            } catch {
                result(NSLocalizedString("CatchError", comment: ""))
            }
        })
        task.resume()
    }
    
    static func newGroup(title: String, result: @escaping (_ error: String?) -> ()) {
        if FirebaseManager.shared.getUid() == nil {
            result(NSLocalizedString("UidError", comment: ""))
            return
        }
        
        var dic: [String: Any] = [:]
        dic.updateValue(FirebaseManager.shared.getUid()!, forKey: "uid")
        dic.updateValue(title.trimmingCharacters(in: .whitespacesAndNewlines), forKey: "title")
        
        let urlString = "\(url)/newMemoGroup.php"
        let encodedURL = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: encodedURL!)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        
        do {
            let json_data = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            request.httpBody = json_data
        } catch  {
            result("data error")
            return
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
            data, response, error in
            if let error = error {
                result(error.localizedDescription)
                return
            }
                   
            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                   
                if let resultData = jsonData as? NSDictionary {
                    if let query = resultData["query"] as? String {
                        print(query)
                    }
                    
                    if let error = resultData["error"] as? Int {
                        result(ErrorMessage.shared.translateErrorMessage(code: error))
                        return
                    }
                    
                    if let _ = resultData["success"] as? Bool {
                        result(nil)
                        return
                    }
                }
            } catch {
                result(NSLocalizedString("CatchError", comment: ""))
            }
        })
        task.resume()
    }
    
    static func getGroups(result: @escaping (_ error: String?, _ list: [MemoGroup]) -> ()) {
        if FirebaseManager.shared.getUid() == nil {
            result(NSLocalizedString("UidError", comment: ""), [])
            return
        }
        
        var dic: [String: Any] = [:]
        dic.updateValue(FirebaseManager.shared.getUid()!, forKey: "uid")
        
        let urlString = "\(url)/getMemoGroups.php"
        let encodedURL = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: encodedURL!)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        
        do {
            let json_data = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            request.httpBody = json_data
        } catch  {
            result("data error", [])
            return
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
            data, response, error in
            if let error = error {
                result(error.localizedDescription, [])
                return
            }
                   
            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                   
                if let resultData = jsonData as? NSDictionary {
                    if let query = resultData["query"] as? String {
                        print(query)
                    }
                    
                    if let error = resultData["error"] as? Int {
                        result(ErrorMessage.shared.translateErrorMessage(code: error), [])
                        return
                    }
                }
                
                if let arr = jsonData as? Array<NSDictionary> {
                    var groupList: [MemoGroup] = []
                    for value in arr {
                        var group = MemoGroup()
                        
                        group.title = value["title"] as? String ?? ""
                       
                        if group.title == "" {
                            // 기본 이름을 설정했다면 설정한 이름을, 아니라면 디폴트 이름을 반환.
                            if let name = UserDefaultManager.shared.memoBasicName {
                                group.title = name
                            } else {
                                group.title = NSLocalizedString("BasicMemoGroup", comment: "")
                            }
                        }
                        
                        group.date = value["date"] as? String ?? ""
                        group.count = value["count"] as? Int ?? 0
                        groupList.append(group)
                    }
                    
                    result(nil, groupList)
                } else {
                    result("Json data error.", [])
                }
            } catch {
                result(NSLocalizedString("CatchError", comment: ""), [])
            }
        })
        task.resume()
    }
    
    static func updateMemos(id: String, contents: String, newContents: String, result: @escaping (_ error: String?) -> ()) {
        if FirebaseManager.shared.getUid() == nil {
            result(NSLocalizedString("UidError", comment: ""))
            return
        }
        
        var dic: [String: Any] = [:]
        dic.updateValue(id, forKey: "id")
        dic.updateValue(FirebaseManager.shared.getUid()!, forKey: "uid")
        dic.updateValue(newContents, forKey: "new_contents")
        
        let urlString = "\(url)/updateMemos.php"
        let encodedURL = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: encodedURL!)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        
        do {
            let json_data = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            request.httpBody = json_data
        } catch  {
            result("data error")
            return
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
            data, response, error in
            if let error = error {
                result(error.localizedDescription)
                return
            }
                   
            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                   
                if let resultData = jsonData as? NSDictionary {
                    if let query = resultData["query"] as? String {
                        print(query)
                    }
                    
                    if let error = resultData["error"] as? Int {
                        result(ErrorMessage.shared.translateErrorMessage(code: error))
                        return
                    }
                    
                    if let _ = resultData["success"] as? Bool {
                        result(nil)
                        return
                    }
                }
            } catch {
                result(NSLocalizedString("CatchError", comment: ""))
            }
        })
        task.resume()
    }
    
    static func deleteMemos(id: String, result: @escaping (_ error: String?) -> ()) {
        if FirebaseManager.shared.getUid() == nil {
            result(NSLocalizedString("UidError", comment: ""))
            return
        }
        
        var dic: [String: Any] = [:]
        dic.updateValue(id, forKey: "id")
        dic.updateValue(FirebaseManager.shared.getUid()!, forKey: "uid")
        
        let urlString = "\(url)/deleteMemos.php"
        let encodedURL = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: encodedURL!)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        
        do {
            let json_data = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            request.httpBody = json_data
        } catch  {
            result("data error")
            return
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
            data, response, error in
            if let error = error {
                result(error.localizedDescription)
                return
            }
                   
            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                   
                if let resultData = jsonData as? NSDictionary {
                    if let query = resultData["query"] as? String {
                        print(query)
                    }
                    
                    if let error = resultData["error"] as? Int {
                        result(ErrorMessage.shared.translateErrorMessage(code: error))
                        return
                    }
                    
                    if let _ = resultData["success"] as? Bool {
                        result(nil)
                        return
                    }
                }
            } catch {
                result(NSLocalizedString("CatchError", comment: ""))
            }
        })
        task.resume()
    }
    
    static func getMemos(group: String, result: @escaping (_ error: String?, _ list: [Memos]) -> ()) {
        if FirebaseManager.shared.getUid() == nil {
            result(NSLocalizedString("UidError", comment: ""), [])
            return
        }
        
        var dic: [String: Any] = [:]
        dic.updateValue(FirebaseManager.shared.getUid()!, forKey: "uid")
        dic.updateValue(group, forKey: "group")
        
        let urlString = "\(url)/getMemos.php"
        let encodedURL = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: encodedURL!)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        
        do {
            let json_data = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            request.httpBody = json_data
        } catch  {
            result("data error", [])
            return
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
            data, response, error in
            if let error = error {
                result(error.localizedDescription, [])
                return
            }
                   
            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                   
                if let resultData = jsonData as? NSDictionary {
                    if let query = resultData["query"] as? String {
                        print(query)
                    }
                    
                    if let error = resultData["error"] as? Int {
                        result(ErrorMessage.shared.translateErrorMessage(code: error), [])
                        return
                    }
                    
                    if let _ = resultData["no_count"] as? Bool {
                        result(nil, [])
                        return
                    }
                }
                
                if let arr = jsonData as? Array<NSDictionary> {
                    var memoList: [Memos] = []
                    for value in arr {
                        var memo = Memos()
                        memo.id = value["id"] as? String ?? ""
                        memo.contents = value["contents"] as? String ?? ""
                        memo.rdate = value["date"] as? String ?? ""
                        memoList.append(memo)
                    }
                    
                    result(nil, memoList)
                } else {
                    result("Json data error.", [])
                }
            } catch {
                result(NSLocalizedString("CatchError", comment: ""), [])
            }
        })
        task.resume()
    }
    
    static func add(contents: String, group: String, result: @escaping (_ error: String?) -> ()) {
        if FirebaseManager.shared.getUid() == nil {
            result(NSLocalizedString("UidError", comment: ""))
            return
        }
        
        var dic: [String: Any] = [:]
        dic.updateValue(FirebaseManager.shared.getUid()!, forKey: "uid")
        dic.updateValue(contents, forKey: "contents")
        dic.updateValue(group, forKey: "group")
        
        let urlString = "\(url)/newMemos.php"
        let encodedURL = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: encodedURL!)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        
        do {
            let json_data = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            request.httpBody = json_data
        } catch  {
            result("data error")
            return
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
            data, response, error in
            if let error = error {
                result(error.localizedDescription)
                return
            }
                    
            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                if let resultData = jsonData as? NSDictionary {
                    if let query = resultData["query"] as? String {
                        print(query)
                    }
                    
                    if let error = resultData["error"] as? Int {
                        result(ErrorMessage.shared.translateErrorMessage(code: error))
                        return
                    }
                    
                    if let _ = resultData["success"] as? Bool {
                        result(nil)
                        return
                    }
                }
            } catch {
                result(NSLocalizedString("CatchError", comment: ""))
            }
        })
        task.resume()
    }
}
