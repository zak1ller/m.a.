//
//  StringManager.swift
//  OVManager
//
//  Created by Min-Su Kim on 2020/07/05.
//  Copyright © 2020 Min-Su Kim. All rights reserved.
//

import Foundation

class StringManager {
    static func transitionGroup(string: String) -> String {
        return "(\(string))"
    }
    
    static func transitionDescription(string: String) -> String {
        return "- "+string
    }
    
    static func transitionPronunciation(string: String) -> String {
        return "| \(string) |"
    }
    
    static func findString(str: String, findStr: String) -> Bool {
        let range = str.range(of: findStr)
        if range == nil {
            return false
        }
        if range!.isEmpty {
            return false
        } else{
            return true
        }
    }
    
    static func Decimal(value: Int) -> String{
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        let result = numberFormatter.string(from: NSNumber(value: value))!
        
        return result
    }
    
    static func makePastTimeString(str: String) -> String{
        let dateFormat = DateFormatter()
        
        dateFormat.dateFormat = "YYYY-MM-dd HH:mm:ss"
        if let date = dateFormat.date(from: str) {
            return getPastTimeString(date: date)
        }
        
        dateFormat.dateFormat = "YYYY-MM-dd_HH:mm:ss"
        if let date = dateFormat.date(from: str) {
            return getPastTimeString(date: date)
        }
        
        dateFormat.dateFormat = "yyyy-MM-dd a h:mm:ss"
        if let date = dateFormat.date(from: str) {
            return getPastTimeString(date: date)
        }
        
        dateFormat.dateFormat = "yyyy-MM-dd a hh:mm:"
        if let date = dateFormat.date(from: str) {
            return getPastTimeString(date: date)
        }
        
        return str
    }
    
    static func convertMemoDate(s: String) -> String {
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "YYYY-MM-dd HH:mm:ss"
        
        if let date = dateFormat.date(from: s) {
            let fomat = DateFormatter()
            fomat.dateFormat = "yyyy. MM. dd"
            return fomat.string(from: date)
        } else {
            return ""
        }
    }
    
    static func getPastTimeString(date: Date) -> String {
        var str = ""
        let interval = date.timeIntervalSinceNow
        let res = abs(Int(interval))
    
        if interval >= -60 {
            str = NSLocalizedString("JustBefore", comment: "")
        } else if interval >= -3600 {
            str = "\(res/60)\(NSLocalizedString("MinuteAgo", comment: ""))"
        } else if interval >= -86400 {
            str = "\(res/3600)\(NSLocalizedString("HourAgo", comment: ""))"
        } else if interval >= -(86400*7) {
            if res/86400 == 1 {
                str = NSLocalizedString("Yesterday", comment: "")
            } else {
                str = "\(res/86400)\(NSLocalizedString("DayAgo", comment: ""))"
            }
        } else {
            if res/86400*7 == 1 {
                str = "\(res/86400*7)\(NSLocalizedString("LastWeek", comment: ""))"
            } else {
                str = "\(res/86400*7)\(NSLocalizedString("WeekAgo", comment: ""))"
            }
        }
        return str
    }
    
    static func convertTodayString(today: Int) -> String {
        if today == 1 {
            return NSLocalizedString("Monday", comment: "")
        } else if today == 2 {
            return NSLocalizedString("Tuesday", comment: "")
        } else if today == 3 {
            return NSLocalizedString("Wednesday", comment: "")
        } else if today == 4 {
            return NSLocalizedString("Thursday", comment: "")
        } else if today == 5 {
            return NSLocalizedString("Friday", comment: "")
        } else if today == 6 {
            return NSLocalizedString("Saturday", comment: "")
        } else if today == 7 {
            return NSLocalizedString("Sunday", comment: "")
        } else {
            return "Wrong date"
        }
    }
    
    static func getDaysString(work: WorkManager.Work) -> String {
        var str = ""
        
        if work.monday == 1 {
            str += NSLocalizedString("MondayShort", comment: "")
        }
        
        if work.tuesday == 1 {
            if str.count == 0 {
                str += NSLocalizedString("TuesdayShort", comment: "")
            } else {
                str += ", "+NSLocalizedString("TuesdayShort", comment: "")
            }
        }
        
        if work.wednesday == 1 {
            if str.count == 0 {
                str += NSLocalizedString("WednedayShort", comment: "")
            } else {
                str += ", "+NSLocalizedString("WednedayShort", comment: "")
            }
        }
        
        if work.thursday == 1 {
            if str.count == 0 {
                str += NSLocalizedString("ThursdayShort", comment: "")
            } else {
                str += ", "+NSLocalizedString("ThursdayShort", comment: "")
            }
        }
        
        if work.friday == 1 {
            if str.count == 0 {
                str += NSLocalizedString("FridayShort", comment: "")
            } else {
                str += ", "+NSLocalizedString("FridayShort", comment: "")
            }
        }
        
        if work.saturday == 1 {
            if str.count == 0 {
                str += NSLocalizedString("SaturdayShort", comment: "")
            } else {
                str += ", "+NSLocalizedString("SaturdayShort", comment: "")
            }
        }
        
        if work.sunday == 1 {
            if str.count == 0 {
                str += NSLocalizedString("SundayShort", comment: "")
            } else {
                str += ", "+NSLocalizedString("SundayShort", comment: "")
            }
        }
        
        return str
    }
}
