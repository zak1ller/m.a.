//
//  ViewManager.swift
//  OVManager
//
//  Created by Min-Su Kim on 2020/07/09.
//  Copyright © 2020 Min-Su Kim. All rights reserved.
//

import Foundation
import UIKit

class ViewManager {
    enum LineType {
        case top, bottom, center
    }
    
    static func actionSheetForiPad() -> UIAlertController.Style {
        if StringManager.findString(str: UIDevice.current.modelName, findStr: "iPad") || StringManager.findString(str: UIDevice.current.modelName, findStr: "Simulator") {
            return .alert
        } else {
            return .actionSheet
        }
    }
    
    static func drawBorderLine(view: UIView) {
        view.layer.borderColor = UIColor(hexString: "#e8e8e8").cgColor
        view.layer.borderWidth = 1.25
    }
    
    static func appendLine(lineType: LineType, view: UIView, spacing: CGFloat? = 0) {
        let line = UIView()
        line.backgroundColor = Colors.shared.line
        
        view.addSubview(line)
        line.translatesAutoresizingMaskIntoConstraints = false
        
        let spacing = spacing ?? 0
        let height = NSLayoutConstraint(item: line, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Sizes.lineHeight)
        let leading = NSLayoutConstraint(item: line, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leading, multiplier: 1, constant: spacing)
        let trailing = NSLayoutConstraint(item: line, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: .trailing, multiplier: 1, constant: -spacing)
        
        switch lineType {
        case .top:
            NSLayoutConstraint.activate([
                NSLayoutConstraint(item: line, attribute: .top, relatedBy: .equal , toItem: view, attribute: .top, multiplier: 1, constant: 0)
                ,leading
                ,trailing
                ,height
            ])
        case .bottom:
            NSLayoutConstraint.activate([
                NSLayoutConstraint(item: line, attribute: .bottom, relatedBy: .equal , toItem: view, attribute: .bottom, multiplier: 1, constant: 0)
                ,leading
                ,trailing
                ,height
            ])
        case .center:
            NSLayoutConstraint.activate([
                NSLayoutConstraint(item: line, attribute: .centerY, relatedBy: .equal, toItem: view, attribute: .centerY, multiplier: 1, constant: 0)
                ,leading
                ,trailing
                ,height
            ])
        }
    }
}
