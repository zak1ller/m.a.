//
//  WorkManager.swift
//  unspecified
//
//  Created by Min-Su Kim on 2020/07/14.
//  Copyright © 2020 Min-Su Kim. All rights reserved.
//

import Foundation

class WorkManager {
    enum WorkStatus {
        case ready, success
    }
    
    struct Work {
        var uid = ""
        var title = ""
        var marks = ""
        var status: WorkStatus = .ready
        var rDate = ""
        var totalCount = 0
        var successCount = 0
        var failedCount = 0
        var comboCount = 0
        var maxComboCount = 0
        var successPercent: Int = 0
        var monday = 0
        var tuesday = 0
        var wednesday = 0
        var thursday = 0
        var friday = 0
        var saturday = 0
        var sunday = 0
    }
    
    struct lastDayResultData {
        var uid = ""
        var title = ""
        var lastRank = ""
        var todayRank = ""
        var lastAvg = 0.0
        var todayAvg = 0.0
        var isCheck = false
    }
    
    struct TotalAvg {
        var taskCount = 0
        var avgSuccess = 0
        var avgMark = "F"
    }
    
    static func setMessage(message: String, result: @escaping (_ error: String?) -> ()) {
        if FirebaseManager.shared.getUid() == nil {
            result(NSLocalizedString("UidError", comment: ""))
            return
        }
        
        var dic: [String: Any] = [:]
        dic.updateValue(FirebaseManager.shared.getUid()!, forKey: "uid")
        dic.updateValue(message.trimmingCharacters(in: .whitespacesAndNewlines), forKey: "message")
        
        let urlString = "\(url)/updateMessage.php"
        let encodedURL = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: encodedURL!)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        
        do {
            let json_data = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            request.httpBody = json_data
        } catch  {
            result("data error")
            return
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
            data, response, error in
            if let error = error {
                result(error.localizedDescription)
                return
            }
                
            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                if let resultData = jsonData as? NSDictionary {
                    if let query = resultData["query"] as? String {
                        print(query)
                    }
                    
                    if let error = resultData["error"] as? Int {
                        result(ErrorMessage.shared.translateErrorMessage(code: error))
                        return
                    }
                    
                    if let _ = resultData["success"] as? Bool {
                        result(nil)
                    } else {
                        result("no result")
                    }
                }
            } catch {
                result(NSLocalizedString("CatchError", comment: ""))
            }
        })
        task.resume()
    }
    
    static func getMessage(result: @escaping (_ error: String?, _ message: String?) -> ()) {
        if FirebaseManager.shared.getUid() == nil {
            result(NSLocalizedString("UidError", comment: ""), nil)
            return
        }
        
        var dic: [String: Any] = [:]
        dic.updateValue(FirebaseManager.shared.getUid()!, forKey: "uid")
        
        let urlString = "\(url)/getMessage.php"
        let encodedURL = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: encodedURL!)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        
        do {
            let json_data = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            request.httpBody = json_data
        } catch  {
            result("data error", nil)
            return
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
            data, response, error in
            if let error = error {
                result(error.localizedDescription, nil)
                return
            }
                
            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                if let resultData = jsonData as? NSDictionary {
                    if let query = resultData["query"] as? String {
                        print(query)
                    }
                    
                    if let error = resultData["error"] as? Int {
                        result(ErrorMessage.shared.translateErrorMessage(code: error), nil)
                        return
                    }
                    
                    if let message = resultData["message"] as? String {
                        result(nil, message)
                    } else {
                        result("no result", nil)
                    }
                }
            } catch {
                result(NSLocalizedString("CatchError", comment: ""), nil)
            }
        })
        task.resume()
    }
    
    static func getWorkServerToday(result: @escaping (_ error: String?, _ today: String?) -> ()) {
        let urlString = "\(url)/getToday.php"
        let encodedURL = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: encodedURL!)
        let request = NSMutableURLRequest(url: url!)
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
            data, response, error in
            if let error = error {
                result(error.localizedDescription, nil)
                return
            }
                
            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                if let resultData = jsonData as? NSDictionary {
                    if let today = Int(resultData["today"] as? String ?? "0") {
                        result(nil, StringManager.convertTodayString(today: today))
                    } else {
                        result("No data", nil)
                    }
                }
            } catch {
                result(NSLocalizedString("CatchError", comment: ""), nil)
            }
        })
        task.resume()
    }
    
    static func updateWorkDays(title: String, days: [SimpleSelectItem], result: @escaping (_ error: String?) -> ()) {
        if FirebaseManager.shared.getUid() == nil {
            result(NSLocalizedString("UidError", comment: ""))
            return
        }
        
        var dic: [String: Any] = [:]
        var daysDic: [String: Any] = [:]
        
        for value in days {
            if value.isCheck {
                daysDic.updateValue(1, forKey: value.key)
            }
        }
        
        dic.updateValue(FirebaseManager.shared.getUid()!, forKey: "uid")
        dic.updateValue(title, forKey: "title")
        dic.updateValue(daysDic, forKey: "days")
        
        let urlString = "\(url)/updateWorkDays.php"
        let encodedURL = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: encodedURL!)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        
        do {
            let json_data = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            request.httpBody = json_data
        } catch  {
            result("data error")
            return
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
            data, response, error in
            if let error = error {
                result(error.localizedDescription)
                return
            }
                
            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                if let resultData = jsonData as? NSDictionary {
                    if let query = resultData["query"] as? String {
                        print(query)
                    }
                    
                    if let error = resultData["error"] as? Int {
                        result(ErrorMessage.shared.translateErrorMessage(code: error))
                        return
                    }
                    
                    if let _ = resultData["success"] as? Bool {
                        result(nil)
                    }
                }
            } catch {
                result(NSLocalizedString("CatchError", comment: ""))
            }
        })
        task.resume()
    }
    
    static func resetWork(title: String, result: @escaping (_ error: String?) -> ()) {
        if FirebaseManager.shared.getUid() == nil {
            result(NSLocalizedString("UidError", comment: ""))
            return
        }
        
        var dic: [String: Any] = [:]
        dic.updateValue(FirebaseManager.shared.getUid()!, forKey: "uid")
        dic.updateValue(title, forKey: "title")
        
        let urlString = "\(url)/resetTask.php"
        let encodedURL = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: encodedURL!)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        
        do {
            let json_data = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            request.httpBody = json_data
        } catch  {
            result("data error")
            return
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
            data, response, error in
            if let error = error {
                result(error.localizedDescription)
                return
            }
                
            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                if let resultData = jsonData as? NSDictionary {
                    if let query = resultData["query"] as? String {
                        print(query)
                    }
                    
                    if let error = resultData["error"] as? Int {
                        result(ErrorMessage.shared.translateErrorMessage(code: error))
                        return
                    }
                    
                    if let _ = resultData["success"] as? Bool {
                        result(nil)
                    }
                }
            } catch {
                result(NSLocalizedString("CatchError", comment: ""))
            }
        })
        task.resume()
    }
    
    static func updateDeadline(deadline: Int, result: @escaping (_ error: String?) -> ()) {
        if FirebaseManager.shared.getUid() == nil {
            result(NSLocalizedString("UidError", comment: ""))
            return
        }
        
        var dic: [String: Any] = [:]
        dic.updateValue(FirebaseManager.shared.getUid()!, forKey: "uid")
        dic.updateValue(deadline, forKey: "deadline")
        
        let urlString = "\(url)/updateDeadline.php"
        let encodedURL = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: encodedURL!)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        
        do {
            let json_data = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            request.httpBody = json_data
        } catch  {
            result("data error")
            return
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
            data, response, error in
            if let error = error {
                result(error.localizedDescription)
                return
            }
                
            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                   
                if let resultData = jsonData as? NSDictionary {
                    if let query = resultData["query"] as? String {
                        print(query)
                    }
                    
                    if let error = resultData["error"] as? Int {
                        result(ErrorMessage.shared.translateErrorMessage(code: error))
                        return
                    }
                    
                    if let _ = resultData["success"] as? Bool {
                        result(nil)
                    }
                }
            } catch {
                result(NSLocalizedString("CatchError", comment: ""))
            }
        })
        task.resume()
    }
    
    static func getWorkDays(title: String, result: @escaping (_ error: String?, _ list: [SimpleSelectItem]?) -> ()) {
        if FirebaseManager.shared.getUid() == nil {
            result(NSLocalizedString("UidError", comment: ""), nil)
            return
        }
        
        var dic: [String: Any] = [:]
        dic.updateValue(FirebaseManager.shared.getUid()!, forKey: "uid")
        dic.updateValue(title, forKey: "title")
        
        let urlString = "\(url)/getWorkDays.php"
        let encodedURL = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: encodedURL!)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        
        do {
            let json_data = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            request.httpBody = json_data
        } catch  {
            result("data error", nil)
            return
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
            data, response, error in
            if let error = error {
                result(error.localizedDescription, nil)
                return
            }
                
            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                   
                if let resultData = jsonData as? NSDictionary {
                    if let query = resultData["query"] as? String {
                        print(query)
                    }
                    
                    if let error = resultData["error"] as? Int {
                        result(ErrorMessage.shared.translateErrorMessage(code: error), nil)
                        return
                    }
                }
                
                if let resultData = jsonData as? Array<NSDictionary> {
                    for value in resultData {
                        var returnList: [SimpleSelectItem] = []
                        
                        let monday = Int(value["monday"] as? String ?? "0")
                        let tuesday = Int(value["tuesday"] as? String ?? "0")
                        let wednesday = Int(value["wednesday"] as? String ?? "0")
                        let thursday = Int(value["thursday"] as? String ?? "0")
                        let friday = Int(value["friday"] as? String ?? "0")
                        let saturday = Int(value["saturday"] as? String ?? "0")
                        let sunday = Int(value["sunday"] as? String ?? "0")
                        
                        if monday == 1 {
                            returnList.append(SimpleSelectItem(title: NSLocalizedString("OnMondays", comment: ""), isCheck: true, key: "monday"))
                        } else {
                            returnList.append(SimpleSelectItem(title: NSLocalizedString("OnMondays", comment: ""), isCheck: false, key: "monday"))
                        }
                        
                        if tuesday == 1 {
                            returnList.append(SimpleSelectItem(title: NSLocalizedString("OnTuesdays", comment: ""), isCheck: true, key: "tuesday"))
                        } else {
                            returnList.append(SimpleSelectItem(title: NSLocalizedString("OnTuesdays", comment: ""), isCheck: false, key: "tuesday"))
                        }
                        
                        if wednesday == 1 {
                            returnList.append(SimpleSelectItem(title: NSLocalizedString("OnWednesdays", comment: ""), isCheck: true, key: "wednesday"))
                        } else {
                            returnList.append(SimpleSelectItem(title: NSLocalizedString("OnWednesdays", comment: ""), isCheck: false, key: "wednesday"))
                        }
                        
                        if thursday == 1 {
                            returnList.append(SimpleSelectItem(title: NSLocalizedString("OnThursdays", comment: ""), isCheck: true, key: "thursday"))
                        } else {
                            returnList.append(SimpleSelectItem(title: NSLocalizedString("OnThursdays", comment: ""), isCheck: false, key: "thursday"))
                        }
                        
                        if friday == 1 {
                            returnList.append(SimpleSelectItem(title: NSLocalizedString("OnFridays", comment: ""), isCheck: true, key: "friday"))
                        } else {
                            returnList.append(SimpleSelectItem(title: NSLocalizedString("OnFridays", comment: ""), isCheck: false, key: "friday"))
                        }
                        
                        if saturday == 1 {
                            returnList.append(SimpleSelectItem(title: NSLocalizedString("OnSaturdays", comment: ""), isCheck: true, key: "saturday"))
                        } else {
                            returnList.append(SimpleSelectItem(title: NSLocalizedString("OnSaturdays", comment: ""), isCheck: false, key: "saturday"))
                        }
                        
                        if sunday == 1 {
                            returnList.append(SimpleSelectItem(title: NSLocalizedString("OnSundays", comment: ""), isCheck: true, key: "sunday"))
                        } else {
                            returnList.append(SimpleSelectItem(title: NSLocalizedString("OnSundays", comment: ""), isCheck: false, key: "sunday"))
                        }
                        
                        result(nil, returnList)
                        return
                    }
                    
                    // 반복문 종료 후에도 값을 얻지 못 했을 경우
                    result(NSLocalizedString("Unknown error", comment: ""), nil)
                }
            } catch {
                result(NSLocalizedString("CatchError", comment: ""), nil)
            }
        })
        task.resume()
    }
    
    static func getDeadline(result: @escaping (_ error: String?, _ deadline: Int?) -> ()) {
        if FirebaseManager.shared.getUid() == nil {
            result(NSLocalizedString("UidError", comment: ""), nil)
            return
        }
        
        var dic: [String: Any] = [:]
        dic.updateValue(FirebaseManager.shared.getUid()!, forKey: "uid")
        
        let urlString = "\(url)/getDeadline.php"
        let encodedURL = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: encodedURL!)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        
        do {
            let json_data = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            request.httpBody = json_data
        } catch  {
            result("data error", nil)
            return
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
            data, response, error in
            if let error = error {
                result(error.localizedDescription, nil)
                return
            }
                
            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                   
                if let resultData = jsonData as? NSDictionary {
                    if let query = resultData["query"] as? String {
                        print(query)
                    }
                    
                    if let error = resultData["error"] as? Int {
                        result(ErrorMessage.shared.translateErrorMessage(code: error), nil)
                        return
                    }
                    
                    if let deadline = Int(resultData["deadline"] as? String ?? "0") {
                        result(nil, deadline)
                    } else {
                        result("?", nil)
                    }
                }
            } catch {
                result(NSLocalizedString("CatchError", comment: ""), nil)
            }
        })
        task.resume()
    }
    
    static func getTotalAvg(result: @escaping (_ error: String?, _ avg: TotalAvg?) -> ()) {
        if FirebaseManager.shared.getUid() == nil {
            result(NSLocalizedString("UidError", comment: ""), nil)
            return
        }
        
        var dic: [String: Any] = [:]
        dic.updateValue(FirebaseManager.shared.getUid()!, forKey: "uid")
        
        let urlString = "\(url)/getStatisticsTotalAVG.php"
        let encodedURL = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: encodedURL!)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        
        do {
            let json_data = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            request.httpBody = json_data
        } catch  {
            result("data error", nil)
            return
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
            data, response, error in
            if let error = error {
                result(error.localizedDescription, nil)
                return
            }
                
            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                   
                if let resultData = jsonData as? NSDictionary {
                    if let error = resultData["error"] as? Int {
                        result(ErrorMessage.shared.translateErrorMessage(code: error), nil)
                        return
                    }
                    
                    if let query = resultData["query"] as? String {
                        result(query, nil)
                        return
                    }
                    
                    var data = TotalAvg()
                    
                    if let _ = resultData["no_count"] as? Bool {
                        result(nil, data)
                        return
                    }
                    
                    data.taskCount = resultData["task_count"] as? Int ?? 0
                    data.avgSuccess = Int(resultData["avg_success"] as? Double ?? 0)
                    data.avgMark = resultData["avg_mark"] as? String ?? "F"
                    
                    result(nil, data)
                }
            } catch {
                result(NSLocalizedString("CatchError", comment: ""), nil)
            }
        })
        task.resume()
    }
    
    static func getStatistics(result: @escaping (_ error: String?, _ list: [Work]) -> ()) {
        if FirebaseManager.shared.getUid() == nil {
            result(NSLocalizedString("UidError", comment: ""), [])
            return
        }
        
        var dic: [String: Any] = [:]
        dic.updateValue(FirebaseManager.shared.getUid()!, forKey: "uid")
        
        let urlString = "\(url)/getStatistics.php"
        let encodedURL = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: encodedURL!)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        
        do {
            let json_data = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            request.httpBody = json_data
        } catch  {
            result("data error", [])
            return
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
            data, response, error in
            if let error = error {
                result(error.localizedDescription, [])
                return
            }
                
            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                   
                if let resultData = jsonData as? NSDictionary {
                    if let error = resultData["error"] as? Int {
                        result(ErrorMessage.shared.translateErrorMessage(code: error), [])
                        return
                    }
                    
                    if let query = resultData["query"] as? String {
                        result(query, [])
                        return
                    }
                    
                    if let _ = resultData["no_count"] as? Bool {
                        result(nil, [])
                        return
                    }
                }
                
                if let arr = jsonData as? Array<NSDictionary> {
                    var workList: [Work] = []
                    for value in arr {
                        var work = Work()
                        work.uid = value["uid"] as? String ?? ""
                        work.title = value["title"] as? String ?? ""
                        work.rDate = value["date"] as? String ?? ""
                        work.marks = value["marks"] as? String ?? ""
                        work.totalCount = Int(value["totalCount"] as? String ?? "0") ?? 0
                        work.successCount = Int(value["successCount"] as? String ?? "0") ?? 0
                        work.failedCount = Int(value["failedCount"] as? String ?? "0") ?? 0
                        work.comboCount = Int(value["comboCount"] as? String ?? "0") ?? 0
                        work.maxComboCount = Int(value["maxComboCount"] as? String ?? "0") ?? 0
                        work.successPercent = value["percent"] as? Int ?? 0
                        
                        var status: WorkStatus {
                            if Int(value["status"] as? String ?? "") == 0 {
                                return .ready
                            } else if Int(value["status"] as? String ?? "") == 1 {
                                return .success
                            } else {
                                return .ready
                            }
                        }
                        
                        work.status = status
                        workList.append(work)
                    }
                    result(nil, workList)
                } else {
                    print("??????")
                }
            } catch {
                result(NSLocalizedString("CatchError", comment: ""), [])
            }
        })
        task.resume()
    }
    
    static func updateStatus(title: String, newStatus: WorkStatus, result: @escaping (_ error: String?) -> ()) {
        if FirebaseManager.shared.getUid() == nil {
            result(NSLocalizedString("UidError", comment: ""))
            return
        }
        
        var dic: [String: Any] = [:]
        dic.updateValue(FirebaseManager.shared.getUid()!, forKey: "uid")
        dic.updateValue(title, forKey: "title")
        
        switch newStatus {
        case .ready:
            dic.updateValue(0, forKey: "new_status")
        case .success:
            dic.updateValue(1, forKey: "new_status")
        }
        
        let urlString = "\(url)/toDoUpdateStatus.php"
        let encodedURL = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: encodedURL!)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        
        do {
            let json_data = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            request.httpBody = json_data
        } catch  {
            result("data error")
            return
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
            data, response, error in
            if let error = error {
                result(error.localizedDescription)
                return
            }
                   
            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                   
                if let resultData = jsonData as? NSDictionary {
                    if let error = resultData["error"] as? Int {
                        result(ErrorMessage.shared.translateErrorMessage(code: error))
                        return
                    }
                    
                    if let query = resultData["query"] as? String {
                        result(query)
                        return
                    }
                    
                    if let _ = resultData["success"] as? Bool {
                        result(nil)
                        return
                    }
                }
            } catch {
                result(NSLocalizedString("CatchError", comment: ""))
            }
        })
        task.resume()
    }
    
    static func renameWork(title: String, newTitle: String, result: @escaping (_ error: String?) -> ()) {
       if FirebaseManager.shared.getUid() == nil {
            result(NSLocalizedString("UidError", comment: ""))
            return
        }
        
        var dic: [String: Any] = [:]
        dic.updateValue(FirebaseManager.shared.getUid()!, forKey: "uid")
        dic.updateValue(title, forKey: "title")
        dic.updateValue(newTitle, forKey: "new_title")
        
        let urlString = "\(url)/toDoRename.php"
        let encodedURL = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: encodedURL!)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        
        do {
            let json_data = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            request.httpBody = json_data
        } catch  {
            result("data error")
            return
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
            data, response, error in
            if let error = error {
                result(error.localizedDescription)
                return
            }
                   
            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                   
                if let resultData = jsonData as? NSDictionary {
                    if let error = resultData["error"] as? Int {
                        result(ErrorMessage.shared.translateErrorMessage(code: error))
                        return
                    }
                    
                    if let query = resultData["query"] as? String {
                        result(query)
                        return
                    }
                    
                    if let _ = resultData["success"] as? Bool {
                        result(nil)
                        return
                    }
                }
            } catch {
                result(NSLocalizedString("CatchError", comment: ""))
            }
        })
        task.resume()
    }
    
    static func deleteWork(title: String, result: @escaping (_ error: String?) -> ()) {
        if FirebaseManager.shared.getUid() == nil {
            result(NSLocalizedString("UidError", comment: ""))
            return
        }
        
        var dic: [String: Any] = [:]
        dic.updateValue(FirebaseManager.shared.getUid()!, forKey: "uid")
        dic.updateValue(title, forKey: "title")
        
        let urlString = "\(url)/toDoDelete.php"
        let encodedURL = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: encodedURL!)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        
        do {
            let json_data = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            request.httpBody = json_data
        } catch  {
            result("data error")
            return
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
            data, response, error in
            if let error = error {
                result(error.localizedDescription)
                return
            }
                   
            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                   
                if let resultData = jsonData as? NSDictionary {
                    if let error = resultData["error"] as? Int {
                        result(ErrorMessage.shared.translateErrorMessage(code: error))
                        return
                    }
                    
                    if let query = resultData["query"] as? String {
                        result(query)
                        return
                    }
                    
                    if let _ = resultData["success"] as? Bool {
                        result(nil)
                        return
                    }
                }
            } catch {
                result(NSLocalizedString("CatchError", comment: ""))
            }
        })
        task.resume()
    }
    
    static func getAllWorks(result: @escaping (_ error: String?, _ list: [Work]) -> ()) {
        if FirebaseManager.shared.getUid() == nil {
            result(NSLocalizedString("UidError", comment: ""), [])
            return
        }
        
        var dic: [String: Any] = [:]
        dic.updateValue(FirebaseManager.shared.getUid()!, forKey: "uid")
        
        let urlString = "\(url)/getAllWorks.php"
        let encodedURL = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: encodedURL!)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        
        do {
            let json_data = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            request.httpBody = json_data
        } catch  {
            result("data error", [])
            return
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
            data, response, error in
            if let error = error {
                result(error.localizedDescription, [])
                return
            }
                   
            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                   
                if let resultData = jsonData as? NSDictionary {
                    if let query = resultData["query"] as? String {
                        print(query)
                    }
                    
                    if let error = resultData["error"] as? Int {
                        result(ErrorMessage.shared.translateErrorMessage(code: error), [])
                        return
                    }
                    
                    if let _ = resultData["no_count"] as? Bool {
                        result(nil, [])
                        return
                    }
                }
                
                if let arr = jsonData as? Array<NSDictionary> {
                    var workList: [Work] = []
                    for value in arr {
                        var work = Work()
                        work.uid = value["uid"] as? String ?? ""
                        work.title = value["title"] as? String ?? ""
                        work.rDate = value["date"] as? String ?? ""
                        work.monday = Int(value["monday"] as? String ?? "0") ?? 0
                        work.tuesday = Int(value["tuesday"] as? String ?? "0") ?? 0
                        work.wednesday = Int(value["wednesday"] as? String ?? "0") ?? 0
                        work.thursday = Int(value["thursday"] as? String ?? "0") ?? 0
                        work.friday = Int(value["friday"] as? String ?? "0") ?? 0
                        work.saturday = Int(value["saturday"] as? String ?? "0") ?? 0
                        work.sunday = Int(value["sunday"] as? String ?? "0") ?? 0
                        workList.append(work)
                    }
                    result(nil, workList)
                } else {
                    result("Json data error.", [])
                }
            } catch {
                result(NSLocalizedString("CatchError", comment: ""), [])
            }
        })
        task.resume()
    }
    
    static func getWorks(result: @escaping (_ error: String?, _ list: [Work]) -> ()) {
        if FirebaseManager.shared.getUid() == nil {
            result(NSLocalizedString("UidError", comment: ""), [])
            return
        }
        
        var dic: [String: Any] = [:]
        dic.updateValue(FirebaseManager.shared.getUid()!, forKey: "uid")
    
        let urlString = "\(url)/getWorks.php"
        let encodedURL = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: encodedURL!)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        
        do {
            let json_data = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            request.httpBody = json_data
        } catch  {
            result("data error", [])
            return
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
            data, response, error in
            if let error = error {
                result(error.localizedDescription, [])
                return
            }
                   
            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                   
                if let resultData = jsonData as? NSDictionary {
                    if let error = resultData["error"] as? Int {
                        result(ErrorMessage.shared.translateErrorMessage(code: error), [])
                        return
                    }
                    
                    if let query = resultData["query"] as? String {
                        result(query, [])
                        return
                    }
                    
                    if let _ = resultData["no_count"] as? Bool {
                        result(nil, [])
                        return
                    }
                }
                
                if let arr = jsonData as? Array<NSDictionary> {
                    var workList: [Work] = []
                    for value in arr {
                        var work = Work()
                        work.uid = value["uid"] as? String ?? ""
                        work.title = value["title"] as? String ?? ""
                        work.rDate = value["date"] as? String ?? ""
                       
                        var status: WorkStatus {
                            if Int(value["status"] as? String ?? "") == 0 {
                                return .ready
                            } else if Int(value["status"] as? String ?? "") == 1 {
                                return .success
                            } else {
                                return .ready
                            }
                        }
                        
                        work.status = status
                        workList.append(work)
                    }
                    result(nil, workList)
                } else {
                    result("Json data error.", [])
                }
            } catch {
                result(NSLocalizedString("CatchError", comment: ""), [])
            }
        })
        task.resume()
    }
    
    static func add(title: String, days: [SimpleSelectItem], result: @escaping (_ error: String?) -> ()) {
        if FirebaseManager.shared.getUid() == nil {
            result(NSLocalizedString("UidError", comment: ""))
            return
        }
        
        var dic: [String: Any] = [:]
        var daysDic: [String: Any] = [:]
        
        for value in days {
            if value.isCheck {
                daysDic.updateValue(1, forKey: value.key)
            }
        }
        
        dic.updateValue(FirebaseManager.shared.getUid()!, forKey: "uid")
        dic.updateValue(title, forKey: "title")
        dic.updateValue(daysDic, forKey: "days")
        
        let urlString = "\(url)/toDoAdd.php"
        let encodedURL = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: encodedURL!)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        
        do {
            let json_data = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            request.httpBody = json_data
        } catch  {
            result("data error")
            return
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
            data, response, error in
            if let error = error {
                result(error.localizedDescription)
                return
            }
                    
            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                    
                if let resultData = jsonData as? NSDictionary {
                    if let error = resultData["error"] as? Int {
                        result(ErrorMessage.shared.translateErrorMessage(code: error))
                        return
                    }
                    
                    if let query = resultData["query"] as? String {
                        result(query)
                        return
                    }
                    
                    if let _ = resultData["success"] as? Bool {
                        result(nil)
                        return
                    }
                }
            } catch {
                result(NSLocalizedString("CatchError", comment: ""))
            }
        })
        task.resume()
    }
}
