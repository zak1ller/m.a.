//
//  LoginManager.swift
//  unspecified
//
//  Created by Min-Su Kim on 2020/07/18.
//  Copyright © 2020 Min-Su Kim. All rights reserved.
//

import Foundation
import FirebaseAuth
import FBSDKLoginKit

class FirebaseManager {
    static let shared = FirebaseManager()
    var firebaseAuth = Auth.auth()
    
    struct User {
        var uid = ""
        var name = ""
        var email = ""
        var provider = ""
        var signUpDate = ""
        var lastSignInDate = ""
        var signInCount = 0
    }
    
    enum UserInformation {
        case uid
        case email
        case displayName
        case provider
    }
    
    init() {
        firebaseAuth.languageCode = Locale.current.languageCode
    }
    
    func getUid() -> String? {
        return getUserInformation(type: .uid)
    }
    
    func isSignedIn() -> Bool {
        if Auth.auth().currentUser != nil {
            return true
        } else {
            return false
        }
    }
    
    func signOut(result: @escaping (_ error: String?) -> ()) {
        do {
            try firebaseAuth.signOut()
            LoginManager().logOut()
            result(nil)
        } catch let signOutError as NSError {
            result(signOutError.localizedDescription)
        }
    }
    
    func getUserInformation(type: UserInformation) -> String? {
        let user = Auth.auth().currentUser
        if let user = user {
            let uid = user.uid
            let email = user.email
            var provider = user.providerID
            let displayName = user.displayName
            
            for value in user.providerData {
                provider = value.providerID
            }
            
            switch type {
            case .uid:
                return uid
            case .email:
                return email
            case .displayName:
                return displayName
            case .provider:
                return provider
            }
        } else {
            return nil
        }
    }
    
    func setDisplayName(newName: String, result: @escaping (_ error: String?) -> ()) {
        let changeRequest = firebaseAuth.currentUser?.createProfileChangeRequest()
        changeRequest?.displayName = newName
        changeRequest?.commitChanges { (error) in
            if let error = error {
                result(error.localizedDescription)
            } else {
                result(nil)
            }
        }
    }
    
    func updateEmail(newEmail: String, result: @escaping (_ error: String?) -> ()) {
        firebaseAuth.currentUser?.updateEmail(to: newEmail) { (error) in
          if let error = error {
              result(error.localizedDescription)
          } else {
              result(nil)
          }
        }
    }
    
    func sendEmailVerificationWithCompletion(result: @escaping (_ error: String?) -> ()) {
        firebaseAuth.currentUser?.sendEmailVerification { (error) in
            if let error = error {
                result(error.localizedDescription)
            } else {
                result(nil)
            }
        }
    }
    
    func sendPasswordResetWithEmail(email: String,result: @escaping (_ error: String?) -> ()) {
        firebaseAuth.sendPasswordReset(withEmail: email) { error in
            if let error = error {
                result(error.localizedDescription)
            } else {
                result(nil)
            }
        }
    }
    
    func deleteUser(password: String = "", result: @escaping (_ error: String?) -> ()) {
        if getUid() == nil {
            result(NSLocalizedString("UidError", comment: ""))
            return
        }
       
        if getUserInformation(type: .provider) == "password" {
            if let user = self.firebaseAuth.currentUser {
                let credential = EmailAuthProvider.credential(withEmail: user.email ?? "", password: password)
                user.reauthenticate(with: credential, completion: {
                    authResult, error in
                    if let error = error {
                        result(ErrorMessage.shared.firebaseEncodeErrorMessage(error: error as NSError))
                        return
                    }
                    
                    var dic: [String: Any] = [:]
                    dic.updateValue(self.getUid()!, forKey: "uid")
                    
                    let urlString = "https://suhz23.cafe24.com/todo/deleteAccount.php"
                    let encodedURL = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
                    let url = URL(string: encodedURL!)
                    let request = NSMutableURLRequest(url: url!)
                    request.httpMethod = "POST"
                    
                    do {
                        let json_data = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
                        request.httpBody = json_data
                    } catch  {
                        result("data error")
                        return
                    }
                    
                    let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
                        data, response, error in
                        if let error = error {
                            result(error.localizedDescription)
                            return
                        }
                            
                        do {
                            let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                               
                            if let resultData = jsonData as? NSDictionary {
                                if let query = resultData["query"] as? String {
                                    print("Query error: \(query)")
                                }
                                
                                if let error = resultData["error"] as? Int {
                                    result(ErrorMessage.shared.translateErrorMessage(code: error))
                                    return
                                }
                                
                                if let _ = resultData["success"] as? Bool {
                                    if self.getUserInformation(type: .provider) == "password" {
                                        user.delete(completion: {
                                            error in
                                            if let error = error {
                                                result(ErrorMessage.shared.firebaseEncodeErrorMessage(error: error as NSError))
                                            } else {
                                                self.signOut(result: {
                                                    error in
                                                    if let error = error {
                                                        result(error)
                                                    } else {
                                                        result(nil)
                                                    }
                                                })
                                            }
                                        })
                                    } else {
                                        result(nil)
                                    }
                                } else {
                                    result("NoSuccess")
                                    return
                                }
                            }
                        } catch {
                            result(NSLocalizedString("CatchError", comment: ""))
                        }
                    })
                    task.resume()
                })
            } else {
                result("Found not user")
                return
            }
            return
        }
        
        var dic: [String: Any] = [:]
        dic.updateValue(getUid()!, forKey: "uid")
        
        let urlString = "url/deleteAccount.php"
        let encodedURL = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: encodedURL!)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        
        do {
            let json_data = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            request.httpBody = json_data
        } catch  {
            result("data error")
            return
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
            data, response, error in
            if let error = error {
                result(error.localizedDescription)
                return
            }
                
            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                   
                if let resultData = jsonData as? NSDictionary {
                    if let query = resultData["query"] as? String {
                        print("Query error: \(query)")
                    }
                    
                    if let error = resultData["error"] as? Int {
                        result(ErrorMessage.shared.translateErrorMessage(code: error))
                        return
                    }
                    
                    if let _ = resultData["success"] as? Bool {
                        if self.getUserInformation(type: .provider) == "password" {
                            if let user = self.firebaseAuth.currentUser {
                                user.delete(completion: {
                                    error in
                                    if let error = error {
                                        result(ErrorMessage.shared.firebaseEncodeErrorMessage(error: error as NSError))
                                    } else {
                                        self.signOut(result: {
                                            error in
                                            if let error = error {
                                                result(error)
                                            } else {
                                                result(nil)
                                            }
                                        })
                                    }
                                })
                            } else {
                                result("Found not user")
                            }
                        } else {
                            result(nil)
                        }
                    } else {
                        result("NoSuccess")
                        return
                    }
                }
            } catch {
                result(NSLocalizedString("CatchError", comment: ""))
            }
        })
        task.resume()
    }
    
    func updateUser(result: @escaping (_ error: String?) -> ()) {
        if getUid() == nil {
            result(NSLocalizedString("UidError", comment: ""))
            return
        }
        
        var dic: [String: Any] = [:]
        dic.updateValue(getUid()!, forKey: "uid")
        dic.updateValue(getUserInformation(type: .displayName) ?? "", forKey: "name")
        dic.updateValue(getUserInformation(type: .email) ?? "", forKey: "email")
        dic.updateValue(getUserInformation(type: .provider) ?? "", forKey: "provider")
        dic.updateValue(TimeZone.current.identifier, forKey: "timezone")
        
        let urlString = "\(url)/updateUser.php"
        let encodedURL = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: encodedURL!)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        
        do {
            let json_data = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            request.httpBody = json_data
        } catch  {
            result("data error")
            return
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
            data, response, error in
            if let error = error {
                result(error.localizedDescription)
                return
            }
                
            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                   
                if let resultData = jsonData as? NSDictionary {
                    if let query = resultData["query"] as? String {
                        print("Query error: \(query)")
                    }
                    
                    if let error = resultData["error"] as? Int {
                        result(ErrorMessage.shared.translateErrorMessage(code: error))
                        return
                    }
                    
                    if let _ = resultData["success"] as? Bool {
                        result(nil)
                    }
                }
            } catch {
                print("catch error")
                result(NSLocalizedString("CatchError", comment: ""))
            }
        })
        task.resume()
    }
    
    func sendVerificEmail(result: @escaping (_ error: String?) -> ()) {
        if firebaseAuth.currentUser == nil {
            result(nil)
        } else {
            Auth.auth().currentUser?.sendEmailVerification(completion: {
                error in
                if let error = error {
                    result(ErrorMessage.shared.firebaseEncodeErrorMessage(error: error as NSError))
                } else {
                    result(nil)
                }
            })
        }
    }
    
    func sendResetPasswordEmail(email: String, result: @escaping (_ error: String?) -> ()) {
        firebaseAuth.sendPasswordReset(withEmail: email, completion: {
            error in
            if let error = error {
                result(ErrorMessage.shared.firebaseEncodeErrorMessage(error: error as NSError))
            } else {
                result(nil)
            }
        })
    }
    
    func signInWithEmail(email: String, password: String, result: @escaping (_ error: String?) -> ()) {
        firebaseAuth.signIn(withEmail: email, password: password) { (user, error) in
            if let error = error {
                result(ErrorMessage.shared.firebaseEncodeErrorMessage(error: error as NSError))
            } else {
                if !self.isVerifiedEmail() {
                    result(NSLocalizedString("PleaseDoneVerifyEmail", comment: ""))
                    return
                }
                
                DispatchQueue.global().async(execute: {
                    FirebaseManager.shared.updateUser(result: {
                        error in
                        if let error = error {
                            result(error)
                        } else {
                            result(nil)
                        }
                    })
                })
            }
        }
    }
    
    func signUpWithEmail(email: String, password: String, result: @escaping (_ error: String?, _ success: String?) -> ()) {
        firebaseAuth.createUser(withEmail: email, password: password, completion: {
            authResult, error in
            if let error = error {
                result(ErrorMessage.shared.firebaseEncodeErrorMessage(error: error as NSError), nil)
            } else {
                FirebaseManager.shared.sendVerificEmail(result: {
                    error in
                    if let error = error {
                        result(error, nil)
                    } else {
                        result(nil, NSLocalizedString("SentVerifyEmailMessage", comment: ""))
                    }
                })
            }
        })
    }
    
    func isVerifiedEmail() -> Bool {
        return Auth.auth().currentUser?.isEmailVerified ?? false
    }
    
    func randomNonceString(length: Int = 32) -> String {
      precondition(length > 0)
      let charset: Array<Character> =
          Array("0123456789ABCDEFGHIJKLMNOPQRSTUVXYZabcdefghijklmnopqrstuvwxyz-._")
      var result = ""
      var remainingLength = length

      while remainingLength > 0 {
        let randoms: [UInt8] = (0 ..< 16).map { _ in
          var random: UInt8 = 0
          let errorCode = SecRandomCopyBytes(kSecRandomDefault, 1, &random)
          if errorCode != errSecSuccess {
            fatalError("Unable to generate nonce. SecRandomCopyBytes failed with OSStatus \(errorCode)")
          }
          return random
        }

        randoms.forEach { random in
          if remainingLength == 0 {
            return
          }

          if random < charset.count {
            result.append(charset[Int(random)])
            remainingLength -= 1
          }
        }
      }

      return result
    }
}
