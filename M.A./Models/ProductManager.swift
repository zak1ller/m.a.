//
//  ProductManager.swift
//  M.A.
//
//  Created by Min-Su Kim on 2020/08/09.
//  Copyright © 2020 Min-Su Kim. All rights reserved.
//

import Foundation

class ProductManager {
    static func getIsLimit(result: @escaping (_ error: String?, _ isPurchase: Bool?) -> ()) {
        if FirebaseManager.shared.getUid() == nil {
            result(NSLocalizedString("UidError", comment: ""), nil)
            return
        }
        
        var dic: [String: Any] = [:]
        dic.updateValue(FirebaseManager.shared.getUid()!, forKey: "uid")
        
        let urlString = "\(url)/isUnlimited.php"
        let encodedURL = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: encodedURL!)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        
        do {
            let json_data = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            request.httpBody = json_data
        } catch  {
            result("data error", nil)
            return
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
            data, response, error in
            if let error = error {
                result(error.localizedDescription, nil)
                return
            }
                
            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                if let resultData = jsonData as? NSDictionary {
                    if let query = resultData["query"] as? String {
                        print(query)
                    }
                    
                    if let error = resultData["error"] as? Int {
                        result(ErrorMessage.shared.translateErrorMessage(code: error), nil)
                        return
                    }
                    
                    if let isUnlimitedUser = resultData["isLimit"] as? String {
                        if isUnlimitedUser == "0" {
                            result(nil, false)
                        } else {
                            result(nil, true)
                        }
                        return
                    }
                    
                    result("Unknown return data.", nil)
                }
            } catch {
                result(NSLocalizedString("CatchError", comment: ""), nil)
            }
        })
        task.resume()
    }
    
    static func deleverProduct(product: String, result: @escaping (_ error: String?) -> ()) {
        if FirebaseManager.shared.getUid() == nil {
            result(NSLocalizedString("UidError", comment: ""))
            return
        }
        
        var dic: [String: Any] = [:]
        dic.updateValue(FirebaseManager.shared.getUid()!, forKey: "uid")
        dic.updateValue(product, forKey: "product")
        
        let urlString = "\(url)/deleverProduct.php"
        let encodedURL = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: encodedURL!)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        
        do {
            let json_data = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            request.httpBody = json_data
        } catch  {
            result("data error")
            return
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
            data, response, error in
            if let error = error {
                result(error.localizedDescription)
                return
            }
                   
            if data == nil {
                result("no respond data.")
                return
            }
            
            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                   
                if let resultData = jsonData as? NSDictionary {
                    if let error = resultData["error"] as? Int {
                        result(ErrorMessage.shared.translateErrorMessage(code: error))
                        return
                    }
                    
                    if let query = resultData["query"] as? String {
                        result(query)
                        return
                    }
                    
                    if let _ = resultData["success"] as? Bool {
                        result(nil)
                        return
                    }
                }
            } catch {
                result(NSLocalizedString("CatchError", comment: ""))
            }
        })
        task.resume()
    }
}
