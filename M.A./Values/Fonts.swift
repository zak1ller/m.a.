//
//  Fonts.swift
//  unspecified
//
//  Created by Min-Su Kim on 2020/07/13.
//  Copyright © 2020 Min-Su Kim. All rights reserved.
//

import Foundation
import UIKit

class Fonts {
    static let type1 = UIFont.systemFont(ofSize: 15, weight: .medium)
    static let type2 = UIFont.systemFont(ofSize: 12, weight: .medium)
    static let type3 = UIFont.systemFont(ofSize: 17, weight: .regular)
    static let type4 = UIFont.systemFont(ofSize: 14, weight: .bold)
    static let type5 = UIFont.systemFont(ofSize: 17, weight: .medium)
    static let title: UIFont = UIFont.systemFont(ofSize: 17, weight: .bold)
}
