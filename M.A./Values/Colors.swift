//
//  Colors.swift
//  unspecified
//
//  Created by Min-Su Kim on 2020/07/13.
//  Copyright © 2020 Min-Su Kim. All rights reserved.
//

import Foundation
import UIKit

class Colors {
    static let shared = Colors()
    var vc: UIViewController?
    
    var facebook = UIColor(hexString: "#4267B2")
    var google = UIColor(hexString: "#DB4437")
    
    var background: UIColor {
        if isdarkmode(vc: vc!) {
            return UIColor(red: 22/255, green: 22/255, blue: 22/255, alpha: 1)
        } else {
            return UIColor(hexString: "#f2f2f6")
        }
    }
    
    var backgroundSub: UIColor {
        if isdarkmode(vc: vc!) {
            return UIColor(hexString: "#202124")
        } else {
            return UIColor.white
        }
    }
    
    var point: UIColor {
        return UIColor(hexString: "59adc4")
    }
    
    var black: UIColor {
        if isdarkmode(vc: vc!) {
            return UIColor.white
        } else {
            return UIColor.black
        }
    }
    
    var textSub: UIColor {
        if isdarkmode(vc: vc!) {
            return UIColor(hexString: "#939395")
        } else {
            return UIColor(hexString: "#939395")
        }
    }
    
    var buttonSub: UIColor {
        if isdarkmode(vc: vc!) {
            return UIColor(hexString: "#424242")
        } else {
            return UIColor(hexString: "#CDCDCD")
        }
    }
    
    var line: UIColor {
        if isdarkmode(vc: vc!) {
            return UIColor(hexString: "#424242")
        } else {
            return UIColor(hexString: "#E8E8E8")
        }
    }
    
    var system: UIColor {
        if isdarkmode(vc: vc!) {
            return UIColor(hexString: "#3C8FF7")
        } else {
            return UIColor(hexString: "#007AFF")
        }
    }
    
    var backgroundAlpha: UIColor {
        if isdarkmode(vc: vc!) {
            return UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.5)
        } else {
            return UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5)
        }
    }
   
    func getGradeColor(grade: Int) -> UIColor {
        if grade >= 4 {
            return point
        } else if grade >= 2 && grade < 4 {
            return black
        } else {
            return buttonSub
        }
    }
    
    func isdarkmode(vc: UIViewController) -> Bool {
        if #available(iOS 13, *) {
            if vc.traitCollection.userInterfaceStyle == .dark {
                return true
            } else {
                return false
            }
        } else {
            return false
        }
    }
}
