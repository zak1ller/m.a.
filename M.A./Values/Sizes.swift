//
//  Sizes.swift
//  unspecified
//
//  Created by Min-Su Kim on 2020/07/13.
//  Copyright © 2020 Min-Su Kim. All rights reserved.
//

import Foundation
import UIKit

class Sizes {
    static let margin1: CGFloat = 16.75
    static let margin2: CGFloat = 50
    static let margin3: CGFloat = 33
    static let marginTop: CGFloat = 46
    
    static let lineHeight: CGFloat = 1
    
    static let button: CGFloat = 57
    static let button2: CGFloat = 39
    static let button3: CGFloat = 25
    
    static let border: CGFloat = 1
    static let radius: CGFloat = 8
    
    static let topViewHeight: CGFloat = 70+Sizes.topSpace
    
    static var topSpace: CGFloat {
        var size: CGFloat = 0
        if isXseries() {
            if isiPad() {
                size = 0
            } else {
                size = 20
            }
        } else {
            size = 0
        }
        return size
    }
    
    static var topHeight: CGFloat {
        if isXseries() {
            if isiPad() {
                return 20
            } else {
                return 44
            }
        } else {
            return 20
        }
    }
    
    static var bottomSpace: CGFloat {
        if isXseries() {
            return 34
        } else {
            return 0
        }
    }
    
    static var tableHeaderHeight: CGFloat {
        if Sizes.isXseries() {
            if Sizes.isiPad() {
                return Sizes.topHeight+Sizes.marginTop+26
            } else {
                return Sizes.topHeight+Sizes.marginTop+2
            }
        } else {
            return Sizes.topHeight+Sizes.marginTop+30
        }
    }
    
    static func isXseries() -> Bool {
            if UIDevice.current.modelName.contains("X")
                || UIDevice.current.modelName.contains("11")
                || UIDevice.current.modelName.contains("12")
                || UIDevice.current.modelName.contains("iPad Pro")
                || UIDevice.current.modelName.contains("iPad air 4th Gen")
                || UIDevice.current.modelName == "Simulator"
            {
                return true
            } else {
                return false
            }
    }
    
    static func isiPad() -> Bool {
        if StringManager.findString(str: UIDevice.current.modelName, findStr: "iPad (12.9") {
            return true
        } else {
            return false
        }
//        if StringManager.findString(str: UIDevice.current.modelName, findStr: "Simulator") {
//            return true
//        } else {
//            return false
//        }
    }
}
