//
//  ErrorMessage.swift
//  unspecified
//
//  Created by Min-Su Kim on 2020/07/14.
//  Copyright © 2020 Min-Su Kim. All rights reserved.
//

import Foundation
import Firebase

class ErrorMessage {
    static let shared = ErrorMessage()
    
    let wrongUid = 0
    let noExistUser = 1
    let wrongAccess = 2
    let queryError = 3
    let workMinLength = 100
    let workMaxLength = 101
    let workNoExist = 102
    let workAlready = 103
    let workNoDays = 104
    let memosMinLength = 200
    let memosMaxLength = 201
    let memosNoExist = 202
    let memoGroupMinLength = 204
    let memoGroupMaxLength = 205
    let memoGroupAlready = 206
    let memoGroupBlockName = 207
    let memoGroupNoExist = 208
    let needSlot = 9999
    
    func firebaseEncodeErrorMessage(error: NSError) -> String {
        var message: String
        
        switch AuthErrorCode(rawValue: error.code) {
        case .networkError:
            message = NSLocalizedString("NetworkErrorMessage", comment: "")
        case .userNotFound:
            message = NSLocalizedString("DidNotMatchPasswordErrorMessage", comment: "")
        case .tooManyRequests:
            message = NSLocalizedString("TooManyRequestErrorMessage", comment: "")
        case .invalidEmail:
            message = NSLocalizedString("InvaildEmailErrorMessage", comment: "")
        case .wrongPassword:
            message = NSLocalizedString("DidNotMatchPasswordErrorMessage", comment: "")
        case .emailAlreadyInUse:
            message = NSLocalizedString("EmailAlreadyUseErrorMessage", comment: "")
        case .weakPassword:
            message = NSLocalizedString("WeakPasswordErrorMessage", comment: "")
        case .userDisabled:
            message = NSLocalizedString("DisabledUserMessage", comment: "")
        default:
            message = error.localizedDescription
        }
        return message
    }
    
    func translateErrorMessage(code: Int) -> String {
        if code == wrongUid {
            return NSLocalizedString("UidError", comment: "")
        } else if code == noExistUser {
            return NSLocalizedString("UidError", comment: "")
        } else if code == wrongAccess {
            return NSLocalizedString("WrongAccessError", comment: "")
        } else if code == queryError {
            return NSLocalizedString("QueryError", comment: "")
        } else if code == workMinLength {
            return NSLocalizedString("WorkMinLengthError", comment: "")
        } else if code == workMaxLength {
            return NSLocalizedString("WorkMaxLengthError", comment: "")
        } else if code == workNoExist {
            return NSLocalizedString("WorkMaxLengthError", comment: "")
        } else if code == workAlready {
            return NSLocalizedString("WorkAlreadyError", comment: "")
        } else if code == workNoDays {
            return NSLocalizedString("WorkNoDaysErrorMessage", comment: "")
        } else if code == memosMinLength {
            return NSLocalizedString("ContentsMinError", comment: "")
        } else if code == memosMaxLength {
            return NSLocalizedString("ContentsMaxError", comment: "")
        } else if code == memosNoExist {
            return NSLocalizedString("ContentsNoExistError", comment: "")
        } else if code == memoGroupMinLength {
            return NSLocalizedString("MemoGroupMinError", comment: "")
        } else if code == memoGroupMaxLength {
            return NSLocalizedString("MemoGroupMaxError", comment: "")
        } else if code == memoGroupAlready {
            return NSLocalizedString("MemoGroupAlreadyError", comment: "")
        } else if code == memoGroupBlockName {
            return NSLocalizedString("MemoGroupBlockNameError", comment: "")
        } else if code == memoGroupNoExist {
            return NSLocalizedString("MemoGroupNoExistError", comment: "")
        } else if code == needSlot {
            return NSLocalizedString("NoSlotMessage", comment: "")
        } else {
            return "Unknown error code.(\(code))"
        }
    }
}
