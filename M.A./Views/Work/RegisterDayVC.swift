//
//  RegisterDay.swift
//  M.A.
//
//  Created by Min-Su Kim on 2020/09/22.
//  Copyright © 2020 Min-Su Kim. All rights reserved.
//

import Foundation
import UIKit

protocol RegisterDayVcDelegate: class {
    func registerDayVcRegistered()
}

class RegisterDayVC: UIViewController, TopViewBackButtonDelegate, TopViewRightTextButtonDelegate, UITableViewDelegate, UITableViewDataSource {
    private var loadingAnim: LoadingAnim!
    private let topView = TopView()
    private let tableView = UITableView()
    
    weak var delegate: RegisterDayVcDelegate?
    private var list: [SimpleSelectItem] = []
    private var text = ""
    private var isEditMode = false
    
    deinit {
        print("Register Day VC Deinited.")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setLayout()
        
        loadingAnim = LoadingAnim(view: view)
        loadingAnim.setColor(color: Colors.shared.buttonSub)
        
        if !isEditMode {
            list.append(SimpleSelectItem(title: NSLocalizedString("OnMondays", comment: ""), isCheck: true, key: "monday"))
            list.append(SimpleSelectItem(title: NSLocalizedString("OnTuesdays", comment: ""), isCheck: true, key: "tuesday"))
            list.append(SimpleSelectItem(title: NSLocalizedString("OnWednesdays", comment: ""), isCheck: true, key: "wednesday"))
            list.append(SimpleSelectItem(title: NSLocalizedString("OnThursdays", comment: ""), isCheck: true, key: "thursday"))
            list.append(SimpleSelectItem(title: NSLocalizedString("OnFridays", comment: ""), isCheck: true, key: "friday"))
            list.append(SimpleSelectItem(title: NSLocalizedString("OnSaturdays", comment: ""), isCheck: true, key: "saturday"))
            list.append(SimpleSelectItem(title: NSLocalizedString("OnSundays", comment: ""), isCheck: true, key: "sunday"))
            tableView.reloadData()
        } else {
            loadingAnim.start()
            
            DispatchQueue.global().async(execute: {
                WorkManager.getWorkDays(title: self.text, result: {
                    error, returnList in
                    self.loadingAnim.stop()
                    
                    if let error = error {
                        let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                        DispatchQueue.main.async(execute: {
                            self.present(alert, animated: true, completion: nil)
                        })
                        return
                    }
                    
                    if let list = returnList {
                        self.list = list
                        DispatchQueue.main.async(execute: {
                            self.tableView.reloadData()
                        })
                    }
                })
            })
        }
    }
    
    func setText(text: String) {
        self.text = text
    }
    
    func setIsEditMode(isEditMode: Bool) {
        self.isEditMode = isEditMode
    }
    
    func pressBackButton() {
        dismiss(animated: true, completion: nil)
    }
    
    func pressRightTextButton() {
        if isEditMode {
            loadingAnim.start()
            
            DispatchQueue.global().async(execute: {
                WorkManager.updateWorkDays(title: self.text, days: self.list, result: {
                    error in
                    self.loadingAnim.stop()
                    
                    if let error = error {
                        let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                        DispatchQueue.main.async(execute: {
                            self.present(alert, animated: true, completion: nil)
                        })
                    } else {
                        DispatchQueue.main.async(execute: {
                            self.dismiss(animated: true, completion: {
                                self.delegate?.registerDayVcRegistered()
                            })
                        })
                    }
                })
            })
        } else {
            loadingAnim.start()
            
            DispatchQueue.global().async(execute: {
                WorkManager.add(title: self.text, days: self.list, result: {
                    error in
                    self.loadingAnim.stop()
                    
                    if let error = error {
                        let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                        DispatchQueue.main.async(execute: {
                            self.present(alert, animated: true, completion: nil)
                        })
                    } else {
                        DispatchQueue.main.async(execute: {
                            self.dismiss(animated: true, completion: {
                                self.delegate?.registerDayVcRegistered()
                            })
                        })
                    }
                })
            })
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SimpleSelectCell", for: indexPath) as! SimpleSelectCell
        cell.setContents(item: list[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // 최소 하나 이상의 항목을 선택해야 하기에 체크를 해제하기 전에 선택된 항목이 몇개인지 체크한다.
        var selectedCount = 0
        for value in list {
            if value.isCheck {
                selectedCount += 1
            }
        }
        
        if list[indexPath.row].isCheck {
            // 선택 되어 있는 항목이 1개 이하일 경우 선택 취소를 막는다.
            if selectedCount <= 1 {
                
            } else {
                list[indexPath.row].isCheck = false
            }
        } else {
            list[indexPath.row].isCheck = true
        }
        tableView.reloadData()
    }
    
    private func setLayout() {
        view.backgroundColor = Colors.shared.backgroundSub
        
        if #available(iOS 13, *) {
            topView.showOnSubView(toView: view)
        } else {
            topView.show(toView: view)
        }
        
        topView.showBackButton()
        topView.backButtonDelegate = self
        topView.showRightTextButton(title: NSLocalizedString("Done", comment: ""))
        topView.rightTextButtonDelegate = self
        topView.settitle(title: NSLocalizedString("Repeat", comment: ""), vc: self)
        
        let tableViewSet = {
            self.view.addSubview(self.tableView)
            self.tableView.translatesAutoresizingMaskIntoConstraints = false
            self.tableView.register(SimpleSelectCell.self, forCellReuseIdentifier: "SimpleSelectCell")
            self.tableView.delegate = self
            self.tableView.dataSource = self
            self.tableView.rowHeight = UITableView.automaticDimension
            self.tableView.backgroundColor = UIColor.clear
            self.tableView.tableFooterView = UIView()
            
            let top = NSLayoutConstraint(item: self.tableView, attribute: .top, relatedBy: .equal, toItem: self.topView, attribute: .bottom, multiplier: 1, constant: Sizes.marginTop)
            let bottom = NSLayoutConstraint(item: self.tableView, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: 0)
            let leading = NSLayoutConstraint(item: self.tableView, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.tableView, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([top, bottom, leading, trailing])
        }
        tableViewSet()
    }
}
