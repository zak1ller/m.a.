//
//  RegisterVC.swift
//  unspecified
//
//  Created by Min-Su Kim on 2020/07/13.
//  Copyright © 2020 Min-Su Kim. All rights reserved.
//

import Foundation
import UIKit

protocol RegisterVcDelegate: class {
    func registerVcDidRegister()
}

class RegisterVC: UIViewController, TopViewBackButtonDelegate, TopViewRightTextButtonDelegate, UITextFieldDelegate, RegisterDayVcDelegate {
    private var loadingAnim: LoadingAnim!
    private var topView = TopView()
    private var inputField = UITextField()
    
    weak var delegate: RegisterVcDelegate?
    private var isEditMode = false
    private var editModeData = WorkManager.Work()
    
    deinit {
        print("Register VC Deinited.")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLayout()
        
        loadingAnim = LoadingAnim(view: view)
        loadingAnim.setColor(color: Colors.shared.buttonSub)
        
        if isEditMode {
            inputField.text = editModeData.title
            inputField.placeholder = editModeData.title
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        inputField.becomeFirstResponder()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }

    func setEditModeData(editModeData: WorkManager.Work) {
        self.editModeData = editModeData
    }
    
    func setIsEditMode(isEditMode: Bool) {
        self.isEditMode = isEditMode
    }
    
    func registerDayVcRegistered() {
        dismiss(animated: true, completion: {
            self.delegate?.registerVcDidRegister()
        })
    }
    
    func pressBackButton() {
        dismiss(animated: true, completion: nil)
    }
    
    func pressRightTextButton() {
        let text = inputField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        
        if isEditMode {
            let before = inputField.placeholder ?? ""
            
            loadingAnim.start()
            
            DispatchQueue.global().async(execute: {
                WorkManager.renameWork(title: before, newTitle: text, result: {
                    error in
                    self.loadingAnim.stop()

                    if let error = error {
                        let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                        DispatchQueue.main.async(execute: {
                            self.present(alert, animated: true , completion: nil)
                        })
                    } else {
                        DispatchQueue.main.async(execute: {
                            self.dismiss(animated: true, completion: {
                                self.delegate?.registerVcDidRegister()
                            })
                        })
                    }
                })
            })
        } else {
            let vc = RegisterDayVC()
            vc.delegate = self
            vc.setText(text: text)
            present(vc, animated: true, completion: nil)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        pressRightTextButton()
        return false
    }
    
    private func setLayout() {
        view.backgroundColor = Colors.shared.backgroundSub
        
        if #available(iOS 13, *) {
            topView.showOnSubView(toView: view)
        } else {
            topView.show(toView: view)
        }
    
        topView.showBackButton()
        topView.showRightTextButton(title: NSLocalizedString("Next", comment: ""))
        topView.backButtonDelegate = self
        topView.rightTextButtonDelegate = self
        
        let inputFieldSet = {
            self.view.addSubview(self.inputField)
            self.inputField.translatesAutoresizingMaskIntoConstraints = false
            self.inputField.delegate = self
            self.inputField.textColor = Colors.shared.black
            self.inputField.font = UIFont.systemFont(ofSize: 25, weight: .bold)
            self.inputField.returnKeyType = .done
            self.inputField.tintColor = Colors.shared.point
            self.inputField.textAlignment = .left
            self.inputField.placeholder = NSLocalizedString("NewHabit", comment: "")
            
            let top = NSLayoutConstraint(item: self.inputField, attribute: .top, relatedBy: .equal, toItem: self.topView, attribute: .bottom, multiplier: 1, constant: Sizes.marginTop)
            let leading = NSLayoutConstraint(item: self.inputField, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: Sizes.margin3)
            let trailing = NSLayoutConstraint(item: self.inputField, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: -Sizes.margin3)
            NSLayoutConstraint.activate([top, leading, trailing])
        }
        inputFieldSet()
    }
}
