//
//  LastDayResultVC.swift
//  M.A.
//
//  Created by Min-Su Kim on 2020/08/01.
//  Copyright © 2020 Min-Su Kim. All rights reserved.
//

import Foundation
import UIKit

class LasyDayResultVC: UIViewController, UITableViewDelegate, UITableViewDataSource, TopViewBackButtonDelegate {
    private let topView = TopView()
    private let titLabel = UILabel()
    private let tableView = UITableView()
    
    private var list: [WorkManager.lastDayResultData] = []
    
    deinit {
        print("Lastday Result VC Deinited.")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLayout()
    }
    
    @objc private func pressFullBottomButton(sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    func pressBackButton() {
        dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LastDayResultCell", for: indexPath) as! LastDayResultCell
        cell.setData(data: list[indexPath.row])
        return cell
    }
    
    func setList(list: [WorkManager.lastDayResultData]) {
        self.list = list
    }
    
    private func setLayout() {
        view.backgroundColor = Colors.shared.background
        
        topView.show(toView: view)
        topView.setbackcolor(color: Colors.shared.background)
        topView.settitle(title: NSLocalizedString("LastDayResults", comment: ""), vc: self)
        topView.showCloseButton()
        topView.backButtonDelegate = self
        
        let tableViewSet = {
            self.view.addSubview(self.tableView)
            self.view.sendSubviewToBack(self.tableView)
            self.tableView.translatesAutoresizingMaskIntoConstraints = false
            self.tableView.register(LastDayResultCell.self, forCellReuseIdentifier: "LastDayResultCell")
            self.tableView.delegate = self
            self.tableView.dataSource = self
            self.tableView.rowHeight = UITableView.automaticDimension
            self.tableView.backgroundColor = Colors.shared.background
            self.tableView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: Sizes.tableHeaderHeight))
            self.tableView.tableFooterView = UIView()
            self.tableView.separatorStyle = .none
            
            let top = NSLayoutConstraint(item: self.tableView, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1, constant: 0)
            let bottom = NSLayoutConstraint(item: self.tableView, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: 0)
            let leading = NSLayoutConstraint(item: self.tableView, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.tableView, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([top, bottom, leading, trailing])
        }
        tableViewSet()
    }
}
