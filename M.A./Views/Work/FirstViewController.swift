//
//  FirstViewController.swift
//  unspecified
//
//  Created by Min-Su Kim on 2020/07/13.
//  Copyright © 2020 Min-Su Kim. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController, RegisterVcDelegate, UITableViewDataSource, UITableViewDelegate, WorkListCellLongTouchDelegate, SignInVcDelegate, RegisterDayVcDelegate, WorkListManagerVcDelegate {

    private var loadingAnim: LoadingAnim!
    private var topView: TopView!
    private let topTitleLabel = UILabel()
    private let topStackView = UIStackView()
    private let topListManagerButton = UIButton(type: .system)
    private let topAddButton = UIButton(type: .system)
    private let todayLabel = UILabel()
    private let refreshControl = UIRefreshControl()
    private let taskTableView = UITableView()
    private let deadlineLabel = UILabel()
    
    private var taskList: [WorkManager.Work] = []
    private var isFirst = true
  
    override func viewDidLoad() {
        super.viewDidLoad()
        Colors.shared.vc = self
        topView = TopView()
        
        DispatchQueue.main.async(execute: {
            self.setLayout()
            self.loadingAnim = LoadingAnim(view: self.view)
            self.loadingAnim.setColor(color: Colors.shared.buttonSub)
        })
    }

    override func viewWillAppear(_ animated: Bool) {
        if !FirebaseManager.shared.isSignedIn() {
            let vc = SignInVC()
            vc.modalPresentationStyle = .fullScreen
            vc.delegate = self
            DispatchQueue.main.async(execute: {
                self.present(vc, animated: true, completion: nil)
            })
        } else {
            // Provider가 이메일 로그인 일경우, 메일 인증이 되지 않은 유저는 로그인 창으로 보낸다.
            if FirebaseManager.shared.getUserInformation(type: .provider) == "password" {
                if !FirebaseManager.shared.isVerifiedEmail() {
                    let vc = SignInVC()
                    vc.modalPresentationStyle = .fullScreen
                    vc.delegate = self
                    DispatchQueue.main.async(execute: {
                        self.present(vc, animated: true, completion: nil)
                    })
                    return
                }
            }
            
            if UserDefaultManager.shared.isListInit ?? false || isFirst {
                if isFirst {
                    isFirst = false
                }
                
                if UserDefaultManager.shared.isListInit == true {
                    UserDefaultManager.shared.isListInit = nil
                }
                
                self.reloadWorks()
            }
        }
    }
    
    @objc func pressAddButton(sender: UIButton) {
        let vc = RegisterVC()
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
    
    @objc func pressListManageButton(sender: UIButton) {
        let vc = WorkListManagerVC()
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
    
    func signInVcSignedIn() {
        print("로그인 됨")
    }
    
    func workListManagerValueChanged() {
        reloadWorks()
    }
    
    func workListCellLongTouch(cell: WorkListCell, index: Int) {
        let title = cell.titleLabel.text ?? ""
        let task = taskList[index]
        
        let alert = UIAlertController(title: title, message: nil, preferredStyle: ViewManager.actionSheetForiPad())
        alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: NSLocalizedString("EditSubject", comment: ""), style: .default, handler: {
            (_) in
            let vc = RegisterVC()
            vc.delegate = self
            vc.setIsEditMode(isEditMode: true)
            vc.setEditModeData(editModeData: task)
            self.present(vc, animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: NSLocalizedString("EditDays", comment: ""), style: .default, handler: {
            (_) in
            let vc = RegisterDayVC()
            vc.delegate = self
            vc.setIsEditMode(isEditMode: true)
            vc.setText(text: title)
            self.present(vc, animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: NSLocalizedString("Delete", comment: ""), style: .destructive, handler: {
            (_) in
            let alert = UIAlertController(title: NSLocalizedString("DeleteMessage", comment: ""), message: nil, preferredStyle: ViewManager.actionSheetForiPad())
            alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil))
            alert.addAction(UIAlertAction(title: NSLocalizedString("Delete", comment: ""), style: .destructive, handler: {
                (_) in
                self.loadingAnim.start()
                
                DispatchQueue.global().async(execute: {
                    WorkManager.deleteWork(title: title, result: {
                        error in
                        self.loadingAnim.stop()
                        
                        if let error = error {
                            let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                            DispatchQueue.main.async(execute: {
                                self.present(alert, animated: true, completion: nil)
                            })
                        } else {
                            self.reloadWorks()
                        }
                    })
                })
            }))
            self.present(alert, animated: true, completion: nil)
        }))
        present(alert, animated: true, completion: nil)
    }
    
    func registerVcDidRegister() {
        reloadWorks()
    }
    
    func registerDayVcRegistered() {
        reloadWorks()
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if refreshControl.isRefreshing {
            reloadWorks()
            
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+0.5, execute: {
                self.refreshControl.endRefreshing()
            })
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return taskList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WorkListCell", for: indexPath) as! WorkListCell
        cell.tag = indexPath.row
        cell.longTouchDelegate = self
        cell.setTask(task: taskList[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        view.addSubview(todayLabel)
        view.backgroundColor = Colors.shared.backgroundSub
        view.alpha = 0.99
        todayLabel.translatesAutoresizingMaskIntoConstraints = false
        todayLabel.textColor = Colors.shared.buttonSub
        todayLabel.font = UIFont.systemFont(ofSize: 12, weight: .medium)
        
        let centerY = NSLayoutConstraint(item: todayLabel, attribute: .centerY, relatedBy: .equal, toItem: view, attribute: .centerY, multiplier: 1, constant: 0)
        let leading = NSLayoutConstraint(item: todayLabel, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leading, multiplier: 1, constant: Sizes.margin3)
        NSLayoutConstraint.activate([centerY, leading])
        
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 35
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let task = taskList[indexPath.row]
       
        let alert = UIAlertController(title: task.title, message: nil, preferredStyle: ViewManager.actionSheetForiPad())
        alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil))
        
        switch task.status {
        case .ready:
            alert.addAction(UIAlertAction(title: NSLocalizedString("Success", comment: ""), style: .default, handler: {
                (_) in
                self.updateStatus(title: task.title, newStatus: .success)
            }))
        case .success:
            alert.addAction(UIAlertAction(title: NSLocalizedString("Ready", comment: ""), style: .default, handler: {
                (_) in
                self.updateStatus(title: task.title, newStatus: .ready)
            }))
        }
        
        DispatchQueue.main.async(execute: {
            self.present(alert, animated: true, completion: nil)
        })
    }
    
    private func updateStatus(title: String, newStatus: WorkManager.WorkStatus) {
        DispatchQueue.global().async(execute: {
            WorkManager.updateStatus(title: title, newStatus: newStatus, result: {
                error in
                if let error = error {
                    let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                    DispatchQueue.main.async(execute: {
                        self.present(alert, animated: true, completion: nil)
                    })
                } else {
                    self.reloadWorks()
                }
            })
        })
    }
    
    @objc private func reloadToday() {
        DispatchQueue.global().async(execute: {
            WorkManager.getWorkServerToday(result: {
                error, today in
                if let error = error {
                    print(error)
                    return
                }
                
                if let today = today {
                    var successCount = 0
                    for value in self.taskList {
                        if value.status == WorkManager.WorkStatus.success {
                            successCount += 1
                        }
                    }
                    
                    if self.taskList.count != 0 {
                        DispatchQueue.main.async(execute: {
                            self.todayLabel.text = "\(today) (\(successCount)/\(self.taskList.count))"
                        })
                    } else {
                        DispatchQueue.main.async(execute: {
                            self.todayLabel.text = "\(today)"
                        })
                    }
                }
            })
        })
    }
    
    @objc private func reloadWorks() {
        taskList.removeAll()
        
        DispatchQueue.global().async(execute: {
            WorkManager.getWorks(result: {
                error, returnList in
                if let error = error {
                    let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                    DispatchQueue.main.async(execute: {
                        self.present(alert, animated: true, completion: nil)
                    })
                } else {
                    for value in returnList {
                        self.taskList.append(value)
                    }
                    
                    DispatchQueue.main.async(execute: {
                        self.taskTableView.reloadData()
                    })
                    
                    self.reloadToday()
                }
            })
        })
    }

    private func setLayout() {
        view.backgroundColor = Colors.shared.background
        
        topView.show(toView: view)
        
        let todoTableViewSet = {
            self.view.addSubview(self.taskTableView)
            self.taskTableView.translatesAutoresizingMaskIntoConstraints = false
            self.taskTableView.tag = 1
            self.taskTableView.register(WorkListCell.self, forCellReuseIdentifier: "WorkListCell")
            self.taskTableView.delegate = self
            self.taskTableView.dataSource = self
            self.taskTableView.refreshControl = self.refreshControl
            self.taskTableView.tableFooterView = UIView()
            self.taskTableView.separatorStyle = .none
            self.taskTableView.backgroundColor = Colors.shared.backgroundSub
            self.taskTableView.layer.cornerRadius = Sizes.radius
            self.taskTableView.rowHeight = UITableView.automaticDimension
            
            let top = NSLayoutConstraint(item: self.taskTableView, attribute: .top, relatedBy: .equal, toItem: self.topView, attribute: .bottom, multiplier: 1, constant: Sizes.marginTop)
            let bottom = NSLayoutConstraint(item: self.taskTableView, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: 0)
            let leading = NSLayoutConstraint(item: self.taskTableView, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.taskTableView, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([top, bottom, leading, trailing])
        }
        todoTableViewSet()
        
        let topStasckViewSet = {
            self.view.addSubview(self.topStackView)
            self.topStackView.translatesAutoresizingMaskIntoConstraints = false
            self.topStackView.addArrangedSubview(self.topListManagerButton)
            self.topStackView.addArrangedSubview(self.topAddButton)
            self.topStackView.axis = .horizontal
            self.topStackView.distribution = .fillEqually
            self.topStackView.spacing = Sizes.margin1*0.5
            
            let top = NSLayoutConstraint(item: self.topStackView, attribute: .top, relatedBy: .equal, toItem: self.topView, attribute: .bottom, multiplier: 1, constant: -Sizes.button3)
            let bottom = NSLayoutConstraint(item: self.topStackView, attribute: .bottom, relatedBy: .equal, toItem: self.taskTableView, attribute: .top, multiplier: 1, constant: -Sizes.margin1)
            let leading = NSLayoutConstraint(item: self.topStackView, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: Sizes.margin1)
            let trailing = NSLayoutConstraint(item: self.topStackView, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: -Sizes.margin1)
            NSLayoutConstraint.activate([top, bottom, leading, trailing])
        }
        topStasckViewSet()
        
        let topManagerButtonSet = {
            self.topListManagerButton.addTarget(self, action: #selector(self.pressListManageButton(sender:)), for: .touchUpInside)
            self.topListManagerButton.backgroundColor = Colors.shared.backgroundSub
            self.topListManagerButton.layer.cornerRadius = Sizes.radius
            self.topListManagerButton.setTitle(NSLocalizedString("List", comment: ""), for: .normal)
            self.topListManagerButton.setTitleColor(Colors.shared.black, for: .normal)
            
            if Sizes.isiPad() {
                self.topListManagerButton.titleLabel?.font = UIFont.systemFont(ofSize: 25, weight: .medium)
            } else {
                self.topListManagerButton.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: .medium)
            }
        }
        topManagerButtonSet()
        
        let topAddButtonSet = {
            self.topAddButton.addTarget(self, action: #selector(self.pressAddButton(sender:)), for: .touchUpInside)
            self.topAddButton.backgroundColor = Colors.shared.backgroundSub
            self.topAddButton.layer.cornerRadius = Sizes.radius
            self.topAddButton.setTitle(NSLocalizedString("Add", comment: ""), for: .normal)
            self.topAddButton.setTitleColor(Colors.shared.black, for: .normal)
            
            if Sizes.isiPad() {
                self.topAddButton.titleLabel?.font = UIFont.systemFont(ofSize: 25, weight: .medium)
            } else {
                self.topAddButton.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: .medium)
            }
            
        }
        topAddButtonSet()
    }
}

