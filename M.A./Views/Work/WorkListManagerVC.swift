//
//  WorkListManagerVC.swift
//  M.A.
//
//  Created by Min-Su Kim on 2020/09/23.
//  Copyright © 2020 Min-Su Kim. All rights reserved.
//

import Foundation
import UIKit

protocol WorkListManagerVcDelegate: class {
    func workListManagerValueChanged()
}

class WorkListManagerVC: UIViewController, TopViewBackButtonDelegate, UITableViewDelegate, UITableViewDataSource, WorkListAllCellLongTouchDelegate, RegisterVcDelegate, RegisterDayVcDelegate {
    
    private var loadingAnim: LoadingAnim!
    private let topView = TopView()
    private let tableView = UITableView()
    
    weak var delegate: WorkListManagerVcDelegate?
    
    private var list: [WorkManager.Work] = []
    
    deinit {
        print("Work List Manager VC Deinited.")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLayout()
        loadingAnim = LoadingAnim(view: view)
        loadingAnim.setColor(color: Colors.shared.buttonSub)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        reloadWorks()
    }
    
    func pressBackButton() {
        dismiss(animated: true, completion: nil)
    }
    
    func registerVcDidRegister() {
        delegate?.workListManagerValueChanged()
        reloadWorks()
    }
    
    func registerDayVcRegistered() {
        delegate?.workListManagerValueChanged()
        reloadWorks()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WorkListAllCell", for: indexPath) as! WorkListAllCell
        cell.tag = indexPath.row
        cell.longTouchDelegate = self
        cell.setdata(work: list[indexPath.row])
        return cell
    }
    
    func workListAllCellLongTouch(cell: WorkListAllCell, index: Int) {
        let item = list[index]
        
        let alert = UIAlertController(title: item.title, message: nil, preferredStyle: ViewManager.actionSheetForiPad())
        alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: NSLocalizedString("EditSubject", comment: ""), style: .default, handler: {
            (_) in
            let vc = RegisterVC()
            vc.delegate = self
            vc.setIsEditMode(isEditMode: true)
            vc.setEditModeData(editModeData: item)
            self.present(vc, animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: NSLocalizedString("EditDays", comment: ""), style: .default, handler: {
            (_) in
            let vc = RegisterDayVC()
            vc.delegate = self
            vc.setIsEditMode(isEditMode: true)
            vc.setText(text: item.title)
            self.present(vc, animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: NSLocalizedString("Delete", comment: ""), style: .destructive, handler: {
            (_) in
            let alert = UIAlertController(title: NSLocalizedString("DeleteMessage", comment: ""), message: nil, preferredStyle: ViewManager.actionSheetForiPad())
            alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil))
            alert.addAction(UIAlertAction(title: NSLocalizedString("Delete", comment: ""), style: .destructive, handler: {
                (_) in
                self.loadingAnim.start()
                
                DispatchQueue.global().async(execute: {
                    WorkManager.deleteWork(title: item.title, result: {
                        error in
                        self.loadingAnim.stop()
                        
                        if let error = error {
                            let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                            DispatchQueue.main.async(execute: {
                                self.present(alert, animated: true, completion: nil)
                            })
                        } else {
                            DispatchQueue.main.async(execute: {
                                self.delegate?.workListManagerValueChanged()
                            })
                            self.reloadWorks()
                        }
                    })
                })
            }))
            self.present(alert, animated: true, completion: nil)
        }))
        present(alert, animated: true, completion: nil)
    }
    
    private func reloadWorks() {
        loadingAnim.start()
        
        DispatchQueue.global().async(execute: {
            WorkManager.getAllWorks(result: {
                error, returnList in
                self.loadingAnim.stop()
                
                if let error = error {
                    let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("Confirrm", comment: ""), style: .cancel, handler: nil))
                    DispatchQueue.main.async(execute: {
                        self.present(alert, animated: true, completion: nil)
                    })
                } else {
                    self.list = returnList
                    DispatchQueue.main.async(execute: {
                        self.tableView.reloadData()
                    })
                }
            })
        })
    }
    
    private func setLayout() {
        view.backgroundColor = Colors.shared.backgroundSub
        
        if #available(iOS 13, *) {
            topView.showOnSubView(toView: view)
        } else {
            topView.showOnSubView(toView: view)
        }
        
        topView.settitle(title: NSLocalizedString("List", comment: ""), vc: self)
        topView.showBackButton()
        topView.backButtonDelegate = self
        
        let tableViewSet = {
            self.view.addSubview(self.tableView)
            self.tableView.translatesAutoresizingMaskIntoConstraints = false
            self.tableView.register(WorkListAllCell.self, forCellReuseIdentifier: "WorkListAllCell")
            self.tableView.delegate = self
            self.tableView.dataSource = self
            self.tableView.rowHeight = UITableView.automaticDimension
            self.tableView.tableFooterView = UIView()
//            self.tableView.separatorStyle = .none
            self.tableView.backgroundColor = Colors.shared.backgroundSub
            
            let top = NSLayoutConstraint(item: self.tableView, attribute: .top, relatedBy: .equal, toItem: self.topView, attribute: .bottom, multiplier: 1, constant: Sizes.marginTop)
            let bottom = NSLayoutConstraint(item: self.tableView, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: 0)
            let leading = NSLayoutConstraint(item: self.tableView, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.tableView, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([top, bottom, leading, trailing])
        }
        tableViewSet()
    }
}
