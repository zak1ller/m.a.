//
//  TopView.swift
//  Soksok
//
//  Created by fsociety on 01/12/2018.
//  Copyright © 2018 김민수. All rights reserved.
//

import Foundation
import UIKit

protocol TopViewBackButtonDelegate: class {
    func pressBackButton()
}

protocol TopViewRightTextButtonDelegate: class {
    func pressRightTextButton()
}

protocol TopViewRightImageButtonDelegate: class {
    func pressRightImageButton()
}

class TopView: UIView {
    private var backvi = UIView()
    private var titlelb = UILabel()
    private var backButton = UIButton(type: .system)
    private var backButtonArea = UIButton(type: .system)
    private var rightTextButton = UIButton(type: .system)
    private var rightImageButtonArea = UIButton(type: .system)
    private var rightImageButton = UIButton(type: .system)
    
    weak var backButtonDelegate: TopViewBackButtonDelegate?
    weak var rightTextButtonDelegate: TopViewRightTextButtonDelegate?
    weak var rightImageButtonDelegate: TopViewRightImageButtonDelegate?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initCommon()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initCommon()
    }
    
    private func initCommon() {
        translatesAutoresizingMaskIntoConstraints = false
        setLayout()
    }
    
    convenience init(backgroundColor: UIColor) {
        self.init()
        self.backgroundColor = backgroundColor
    }
    
    func show(toView: UIView) {
        toView.addSubview(self)
        let top = NSLayoutConstraint(item: self, attribute: .top, relatedBy: .equal, toItem: toView, attribute: .top, multiplier: 1, constant: 0)
        let leading = NSLayoutConstraint(item: self, attribute: .leading, relatedBy: .equal, toItem: toView, attribute: .leading, multiplier: 1, constant: 0)
        let trailing = NSLayoutConstraint(item: self, attribute: .trailing, relatedBy: .equal, toItem: toView, attribute: .trailing, multiplier: 1, constant: 0)
        let height  = NSLayoutConstraint(item: self, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Sizes.topViewHeight)
        NSLayoutConstraint.activate([top,leading,trailing,height])
    }
    
    func showOnSubView(toView: UIView) {
        toView.addSubview(self)
        let top = NSLayoutConstraint(item: self, attribute: .top, relatedBy: .equal, toItem: toView, attribute: .top, multiplier: 1, constant: 0)
        let leading = NSLayoutConstraint(item: self, attribute: .leading, relatedBy: .equal, toItem: toView, attribute: .leading, multiplier: 1, constant: 0)
        let trailing = NSLayoutConstraint(item: self, attribute: .trailing, relatedBy: .equal, toItem: toView, attribute: .trailing, multiplier: 1, constant: 0)
        let height  = NSLayoutConstraint(item: self, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Sizes.button2+Sizes.margin1)
        NSLayoutConstraint.activate([top,leading,trailing,height])
    }
    
    func showBackButton() {
        backButton.isHidden = false
        backButtonArea.isHidden = false
    }
    
    func showCloseButton() {
        backButton.isHidden = false
        backButtonArea.isHidden = false
        backButton.setImage(UIImage(named: "close.png"), for: .normal)
    }
    
    func showRightTextButton(title: String) {
        rightTextButton.setTitle(title, for: .normal)
        rightTextButton.isHidden = false
    }
    
    func hideRightTextButton() {
        rightTextButton.isHidden = true
    }
    
    func showRightImageButton(imagee: UIImage?) {
        rightImageButton.setImage(imagee, for: .normal)
        rightImageButton.isHidden = false
        rightImageButtonArea.isHidden = false
    }
    
    func hideRightImageButton() {
        rightImageButton.isHidden = true
        rightImageButtonArea.isHidden = true
    }
    
    func setbackcolor(color: UIColor) {
        backvi.backgroundColor = color
    }
    
    func settitle(title: String, vc: UIViewController) {
        titlelb.text = title
    }
    
    @objc private func pressBackButton(sender: UIButton) {
        backButtonDelegate?.pressBackButton()
    }
    
    @objc private func pressRightTextButton(sender: UIButton) {
        rightTextButtonDelegate?.pressRightTextButton()
    }
    
    @objc private func pressRightImageButton(sender: UIButton) {
        rightImageButtonDelegate?.pressRightImageButton()
    }
}

extension TopView {
    func setLayout() {
        backgroundColor = UIColor.clear
        
        let backviset = {
            self.addSubview(self.backvi)
            self.backvi.translatesAutoresizingMaskIntoConstraints = false
            self.backvi.alpha = 0.95
           
            let top = NSLayoutConstraint(item: self.backvi, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 0)
            let bottom = NSLayoutConstraint(item: self.backvi, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: 0)
            let leading = NSLayoutConstraint(item: self.backvi, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.backvi, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([top, bottom, leading, trailing])
        }
        backviset()
        
        let backButtonSet = {
            self.addSubview(self.backButton)
            self.backButton.translatesAutoresizingMaskIntoConstraints = false
            self.backButton.setImage(UIImage(named: "back.png"), for: .normal)
            self.backButton.isHidden = true
            self.backButton.tintColor = Colors.shared.black
            
            let width = NSLayoutConstraint(item: self.backButton, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Sizes.button2)
            let height = NSLayoutConstraint(item: self.backButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Sizes.button2)
            let bottom = NSLayoutConstraint(item: self.backButton, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: 0)
            let leading = NSLayoutConstraint(item: self.backButton, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: Sizes.margin1)
            NSLayoutConstraint.activate([width, height, bottom, leading])
        }
        backButtonSet()
        
        let backButtonAreaSet = {
            self.addSubview(self.backButtonArea)
            self.backButtonArea.translatesAutoresizingMaskIntoConstraints = false
            self.backButtonArea.addTarget(self, action: #selector(self.pressBackButton(sender:)), for: .touchUpInside)
            self.backButtonArea.isHidden = true
            
            let width = NSLayoutConstraint(item: self.backButtonArea, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Sizes.button)
            let height = NSLayoutConstraint(item: self.backButtonArea, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Sizes.button)
            let centerX = NSLayoutConstraint(item: self.backButtonArea, attribute: .centerX, relatedBy: .equal, toItem: self.backButton, attribute: .centerX, multiplier: 1, constant: 0)
            let centerY = NSLayoutConstraint(item: self.backButtonArea, attribute: .centerY, relatedBy: .equal, toItem: self.backButton, attribute: .centerY, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([width, height, centerX, centerY])
        }
        backButtonAreaSet()
        
        let rightImageButtonSet = {
            self.addSubview(self.rightImageButton)
            self.rightImageButton.translatesAutoresizingMaskIntoConstraints = false
            self.rightImageButton.isHidden = true
            self.rightImageButton.tintColor = Colors.shared.black
            
            let width = NSLayoutConstraint(item: self.rightImageButton, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Sizes.button2)
            let height = NSLayoutConstraint(item: self.rightImageButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Sizes.button2)
            let bottom = NSLayoutConstraint(item: self.rightImageButton, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.rightImageButton, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: -Sizes.margin1)
            NSLayoutConstraint.activate([width, height, bottom, trailing])
        }
        rightImageButtonSet()
        
        let rightImageButtonAreaSet = {
            self.addSubview(self.rightImageButtonArea)
            self.rightImageButtonArea.translatesAutoresizingMaskIntoConstraints = false
            self.rightImageButtonArea.addTarget(self, action: #selector(self.pressRightImageButton(sender:)), for: .touchUpInside)
            self.rightImageButtonArea.isHidden = true
            
            let width = NSLayoutConstraint(item: self.rightImageButtonArea, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Sizes.button)
            let height = NSLayoutConstraint(item: self.rightImageButtonArea, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Sizes.button)
            let centerX = NSLayoutConstraint(item: self.rightImageButtonArea, attribute: .centerX, relatedBy: .equal, toItem: self.rightImageButton, attribute: .centerX, multiplier: 1, constant: 0)
            let centerY = NSLayoutConstraint(item: self.rightImageButtonArea, attribute: .centerY, relatedBy: .equal, toItem: self.rightImageButton, attribute: .centerY, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([width, height, centerX, centerY])
        }
        rightImageButtonAreaSet()
        
        let rightTextButtonSet = {
            self.addSubview(self.rightTextButton)
            self.rightTextButton.translatesAutoresizingMaskIntoConstraints = false
            self.rightTextButton.addTarget(self, action: #selector(self.pressRightTextButton(sender:)), for: .touchUpInside)
            self.rightTextButton.setTitleColor(Colors.shared.point, for: .normal)
            
            if Sizes.isiPad() {
                self.rightTextButton.titleLabel?.font = UIFont.systemFont(ofSize: 25, weight: .bold)
            } else {
                self.rightTextButton.titleLabel?.font = Fonts.title
            }
            
            self.rightTextButton.isHidden = true
            
            let height = NSLayoutConstraint(item: self.rightTextButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Sizes.button2)
            let bottom = NSLayoutConstraint(item: self.rightTextButton, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.rightTextButton, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: -Sizes.margin1)
            NSLayoutConstraint.activate([height, bottom, trailing])
        }
        rightTextButtonSet()
        
        let titlelbset = {
            self.addSubview(self.titlelb)
            self.titlelb.translatesAutoresizingMaskIntoConstraints = false
            self.titlelb.textAlignment = .center
            
            if Sizes.isiPad() {
                self.titlelb.font = UIFont.systemFont(ofSize: 25, weight: .bold)
            } else {
                self.titlelb.font = Fonts.title
            }
            
            self.titlelb.adjustsFontSizeToFitWidth = true
            self.titlelb.numberOfLines = 2
            self.titlelb.textColor = Colors.shared.black
        
            let bottom = NSLayoutConstraint(item: self.titlelb, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: 0)
            let height = NSLayoutConstraint(item: self.titlelb, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Sizes.button2)
            let leading = NSLayoutConstraint(item: self.titlelb, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: Sizes.margin2+Sizes.margin1)
            let trailing = NSLayoutConstraint(item: self.titlelb, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: -(Sizes.margin2+Sizes.margin1))
            NSLayoutConstraint.activate([bottom, height, leading, trailing])
        }
        titlelbset()
    }
}
