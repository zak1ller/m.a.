//
//  MenuVC.swift
//  M.A.
//
//  Created by Min-Su Kim on 2020/07/25.
//  Copyright © 2020 Min-Su Kim. All rights reserved.
//

import Foundation
import UIKit
import MessageUI

class MenuVC: UIViewController, MFMailComposeViewControllerDelegate {
    private var loadingAnim: LoadingAnim!
    private var topView: TopView!
    private let topEmptyView = UIView()
    private let emailLabel = UILabel()
    private let menuStackView = UIStackView()
    private var unlimitedProductButton: MenuButton!
    private var settingButton: MenuButton!
    private var accountButton: MenuButton!
    private var letterButton: MenuButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        unlimitedProductButton = MenuButton(id: "product")
        settingButton = MenuButton(id: "setting")
        accountButton = MenuButton(id: "account")
        letterButton = MenuButton(id: "letter")
        
        topView = TopView()
        setLayout()
        
        loadingAnim = LoadingAnim(view: view)
        loadingAnim.setColor(color: Colors.shared.buttonSub)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let username = FirebaseManager.shared.getUserInformation(type: .displayName) {
            topView.settitle(title: username, vc: self)
        } else {
            topView.settitle(title: NSLocalizedString("NoName", comment: ""), vc: self)
        }
        
        if let email = FirebaseManager.shared.getUserInformation(type: .email) {
            emailLabel.text = email
        } else {
            emailLabel.text = NSLocalizedString("NoData", comment: "")
        }
    }
    
    @objc private func pressUnlimitedProductButton(sender: UIButton) {
        loadingAnim.start()
        
        DispatchQueue.global().async(execute: {
            ProductManager.getIsLimit(result: {
                error, res in
                self.loadingAnim.stop()
                
                if let error = error {
                    let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                    DispatchQueue.main.async(execute: {
                        self.present(alert, animated: true, completion: nil)
                    })
                    return
                }
                
                if let res = res {
                    if res {
                        let alert = UIAlertController(title: NSLocalizedString("UnlimitedPassUser", comment: ""), message: nil, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                        DispatchQueue.main.async(execute: {
                            self.present(alert, animated: true, completion: nil)
                        })
                    } else {
                        DispatchQueue.main.async(execute: {
                            let vc = PurchaseVC()
                            vc.modalPresentationStyle = .fullScreen
                            self.present(vc, animated: true, completion: nil)
                        })
                    }
                }
            })
        })
    }
    
    @objc private func pressAccountButton(sender: UIButton) {
        let vc = AccountVC()
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: true, completion: nil)
    }
    
    @objc private func pressSettingButton(sender: UIButton) {
        let vc = SettingVC()
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: true, completion: nil)
    }
    
    @objc private func pressContactButton(sender: UIButton) {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setMessageBody(
                """
                app: M.A.
                user: \(FirebaseManager.shared.getUserInformation(type: .uid) ?? "")
                user account: \(FirebaseManager.shared.getUserInformation(type: .email) ?? "No email.")
                os vesion: \(UIDevice.current.systemVersion)
                app version: \(String(describing: Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String))
                """, isHTML: false)
            mail.setToRecipients(["zak1ller.kr@gmail.com"])
            present(mail, animated: true, completion: nil)
        } else {
            let alert = UIAlertController(title: NSLocalizedString("FailedOpneMailAppMessage", comment: ""), message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
            alert.addAction(UIAlertAction(title: NSLocalizedString("CopyEmail", comment: ""), style: .default, handler: {
                (_) in
                UIPasteboard.general.string = "zak1ller.kr@gmail.com"
            }))
            present(alert, animated: true, completion: nil)
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        if result == .sent {
            controller.dismiss(animated: true, completion: {
                let alert = UIAlertController(title: NSLocalizedString("SentContact", comment: ""), message: nil, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            })
        } else {
            controller.dismiss(animated: true)
        }
    }
    
    private func setLayout() {
        view.backgroundColor = Colors.shared.background
        
        topView.show(toView: view)
        topView.setbackcolor(color: Colors.shared.background)
        
        let topEmptyViewSet = {
            self.view.addSubview(self.topEmptyView)
            self.topEmptyView.translatesAutoresizingMaskIntoConstraints = false
            self.topEmptyView.backgroundColor = Colors.shared.background
            self.topEmptyView.alpha = 0.97
        
            let top = NSLayoutConstraint(item: self.topEmptyView, attribute: .top, relatedBy: .equal, toItem: self.topView, attribute: .bottom, multiplier: 1, constant: 0)
            let height = NSLayoutConstraint(item: self.topEmptyView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Sizes.marginTop)
            let leading = NSLayoutConstraint(item: self.topEmptyView, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.topEmptyView, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([top, height, leading, trailing])
        }
        topEmptyViewSet()
        
        let emailLabelSet = {
            self.topEmptyView.addSubview(self.emailLabel)
            self.emailLabel.translatesAutoresizingMaskIntoConstraints = false
            self.emailLabel.textColor = Colors.shared.textSub
            
            if Sizes.isiPad() {
                self.emailLabel.font = UIFont.systemFont(ofSize: 17, weight: .medium)
            } else {
                self.emailLabel.font = Fonts.type2
            }
            
            self.emailLabel.textAlignment = .center
            
            let leading = NSLayoutConstraint(item: self.emailLabel, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: Sizes.margin3)
            let trailing = NSLayoutConstraint(item: self.emailLabel, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: -Sizes.margin3)
            let centerY = NSLayoutConstraint(item: self.emailLabel, attribute: .centerY, relatedBy: .equal, toItem: self.topEmptyView, attribute: .centerY, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([leading, trailing, centerY])
        }
        emailLabelSet()
        
        let menuStackViewSet = {
            self.view.addSubview(self.menuStackView)
            self.menuStackView.translatesAutoresizingMaskIntoConstraints = false
            self.menuStackView.addArrangedSubview(self.unlimitedProductButton)
            self.menuStackView.addArrangedSubview(self.settingButton)
            self.menuStackView.addArrangedSubview(self.accountButton)
            self.menuStackView.addArrangedSubview(self.letterButton)
            self.menuStackView.axis = .vertical
            self.menuStackView.distribution = .fill
            
            let top = NSLayoutConstraint(item: self.menuStackView, attribute: .top, relatedBy: .equal, toItem: self.topEmptyView, attribute: .bottom, multiplier: 1, constant: 0)
            let leading = NSLayoutConstraint(item: self.menuStackView, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.menuStackView, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([top, leading, trailing])
        }
        menuStackViewSet()
        
        let menuButtonsSet = {
            self.unlimitedProductButton.setIcon(icon: UIImage(named: "premium.png"))
            self.unlimitedProductButton.setTitle(title: NSLocalizedString("UnlimiProductMenuTtitle", comment: ""))
            self.unlimitedProductButton.addTarget(self, action: #selector(self.pressUnlimitedProductButton(sender:)), for: .touchUpInside)
            
            self.settingButton.setIcon(icon: UIImage(named: "setting.png"))
            self.settingButton.setTitle(title: NSLocalizedString("Settings", comment: ""))
            self.settingButton.addTarget(self, action: #selector(self.pressSettingButton(sender:)), for: .touchUpInside)
            
            self.accountButton.setIcon(icon: UIImage(named: "user.png"))
            self.accountButton.setTitle(title: NSLocalizedString("Account", comment: ""))
            self.accountButton.addTarget(self, action: #selector(self.pressAccountButton(sender:)), for: .touchUpInside)
            
            self.letterButton.setIcon(icon: UIImage(named: "letter.png"))
            self.letterButton.setTitle(title: NSLocalizedString("ContactUs", comment: ""))
            self.letterButton.addTarget(self, action: #selector(self.pressContactButton(sender:)), for: .touchUpInside)
            self.letterButton.hideArrowImageView()
        }
        menuButtonsSet()
    }
}
