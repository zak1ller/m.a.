//
//  SetTaskDeadlineVC.swift
//  M.A.
//
//  Created by Min-Su Kim on 2020/07/27.
//  Copyright © 2020 Min-Su Kim. All rights reserved.
//

import Foundation
import UIKit

class SetTaskDeadlineVC: UIViewController, TopViewBackButtonDelegate, UITableViewDelegate, UITableViewDataSource {
    private var loadinganim: LoadingAnim!
    private let topView = TopView()
    private let tableView = UITableView()
    
    private var deadline = 0
    private var times: [Int] = []
    
    deinit {
        print("Set Task Deadline VC Deinited.")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLayout()
        loadinganim = LoadingAnim(view: view)
        loadinganim.setColor(color: Colors.shared.buttonSub)
        
        var value = 0
        while(times.count < 24) {
            times.append(value)
            value += 1
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        reloadDeadline()
    }
    
    func pressBackButton() {
        dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return times.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Hour24SelectCell", for: indexPath) as! Hour24SelectCell
        
        if times[indexPath.row] <= 12 {
            cell.valueLabel.text = "\(times[indexPath.row])\(NSLocalizedString("marginTimeAm", comment: ""))"
        } else {
            cell.valueLabel.text = "\(times[indexPath.row])\(NSLocalizedString("marginTimePm", comment: ""))"
        }
        
        if times[indexPath.row] == deadline {
            cell.setType(isSelected: true)
        } else {
            cell.setType(isSelected: false)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        updateDeadline(deadline: times[indexPath.row])
    }
    
    private func reloadDeadline() {
        DispatchQueue.global().async(execute: {
            WorkManager.getDeadline(result: {
                error, deadline in
                if let error = error {
                    let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                    DispatchQueue.main.async(execute: {
                        self.present(alert, animated: true, completion: nil)
                    })
                }
                
                if let deadline = deadline {
                    self.deadline = deadline
                    DispatchQueue.main.async(execute: {
                        self.tableView.reloadData()
                    })
                }
            })
        })
    }
    
    private func updateDeadline(deadline: Int) {
        DispatchQueue.global().async(execute: {
            WorkManager.updateDeadline(deadline: deadline, result: {
                error in
                if let error = error {
                    let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                    DispatchQueue.main.async(execute: {
                        self.present(alert, animated: true, completion: nil)
                    })
                } else {
                    self.deadline = deadline
                    DispatchQueue.main.async(execute: {
                        self.tableView.reloadData()
                    })
                }
            })
        })
    }
    
    private func setLayout() {
        view.backgroundColor = Colors.shared.background
        topView.show(toView: view)
        topView.showBackButton()
        topView.backButtonDelegate = self
        topView.settitle(title: NSLocalizedString("TaskMarginTime", comment: ""), vc: self)
        topView.setbackcolor(color: Colors.shared.background)
        
        let tableViewSet = {
            self.view.addSubview(self.tableView)
            self.view.sendSubviewToBack(self.tableView)
            self.tableView.translatesAutoresizingMaskIntoConstraints = false
            self.tableView.register(Hour24SelectCell.self, forCellReuseIdentifier: "Hour24SelectCell")
            self.tableView.delegate = self
            self.tableView.dataSource = self
            self.tableView.separatorStyle = .none
            self.tableView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: Sizes.tableHeaderHeight))
            self.tableView.tableFooterView = UIView()
            self.tableView.backgroundColor = Colors.shared.background
            
            let top = NSLayoutConstraint(item: self.tableView, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1, constant: 0)
            let bottom = NSLayoutConstraint(item: self.tableView, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: 0)
            let leading = NSLayoutConstraint(item: self.tableView, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.tableView, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([top, bottom, leading, trailing])
        }
        tableViewSet()
    }
}
