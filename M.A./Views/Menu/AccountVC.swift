//
//  AccountV.swift
//  M.A.
//
//  Created by Min-Su Kim on 2020/07/26.
//  Copyright © 2020 Min-Su Kim. All rights reserved.
//

import Foundation
import UIKit

class AccountVC: UIViewController, TopViewBackButtonDelegate, OnelineEditVCDelegate {
    private var loadingAnim: LoadingAnim!
    private let topView = TopView()
    private let userStackView = UIStackView()
    private let usernameButton = MenuButton(id: "username")
    private let emailButton = MenuButton(id: "email")
    private let menuStackView = UIStackView()
    private let signOutButton = MenuButton(id: "sign_out")
    private let deleteAccountButton = MenuButton(id: "delete_account")
    private let privacyPolicyButton = MenuButton(id: "privacy_policy")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLayout()
        loadingAnim = LoadingAnim(view: view)
        loadingAnim.setColor(color: Colors.shared.buttonSub)
    }
    
    @objc private func pressUserButton(sender: UIButton) {
        let vc = OnelineEditVC()
        vc.tag = 1
        vc.delegate = self
        vc.isbecome = true
        vc.maxtext = 20
        vc.settitle(title: NSLocalizedString("UpdateUsername", comment: ""))
        vc.setplaceholder(title: FirebaseManager.shared.getUserInformation(type: .displayName) ?? NSLocalizedString("NoData", comment: ""))
        vc.setFieldText(text: FirebaseManager.shared.getUserInformation(type: .displayName) ?? NSLocalizedString("NoData", comment: ""))
        DispatchQueue.main.async(execute: {
            self.present(vc, animated: true, completion: nil)
        })
    }
    
    @objc private func pressSignOutButton(sender: UIButton) {
        let alert = UIAlertController(title: NSLocalizedString("SignOutMessage", comment: ""), message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("SignOut", comment: ""), style: .destructive, handler: {
            (_) in
            self.loadingAnim.start()
            
            FirebaseManager.shared.signOut(result: {
                error in
                self.loadingAnim.stop()
                
                if let error = error {
                    let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                } else {
                    let vc = SignInVC()
                    vc.modalPresentationStyle = .fullScreen
                    self.present(vc, animated: true, completion: nil)
                }
            })
        }))
        alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    @objc private func pressDeleteAccountButton(sender: UIButton) {
        let alert = UIAlertController(title: NSLocalizedString("DeleteAccountMessage", comment: ""), message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: NSLocalizedString("DeleteAccount", comment: ""), style: .destructive, handler: {
            (_) in
            if FirebaseManager.shared.getUserInformation(type: .provider) == "password" {
                let vc = OnelineEditVC()
                vc.delegate = self
                vc.tag = 2
                vc.isbecome = true
                vc.ispassword = true
                vc.settitle(title: NSLocalizedString("DeleteAccount", comment: ""))
                vc.setplaceholder(title: NSLocalizedString("RequestEnterPasswordMessage", comment: ""))
                self.present(vc, animated: true, completion: nil)
            } else {
                self.loadingAnim.start()
                
                FirebaseManager.shared.deleteUser(result: {
                    error in
                    self.loadingAnim.stop()
                    
                    if let error = error {
                        let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                        DispatchQueue.main.async(execute: {
                            self.present(alert, animated: true, completion: nil)
                        })
                    } else {
                        DispatchQueue.main.async(execute: {
                            let vc = SignInVC()
                            vc.modalPresentationStyle = .fullScreen
                            self.present(vc, animated: true, completion: nil)
                        })
                    }
                })
            }
        }))
        DispatchQueue.main.async(execute: {
            self.present(alert, animated: true, completion: nil)
        })
    }
    
    @objc private func pressPrivacyPolicyButton(sender: UIButton) {
        let url = URL(string: "http://monkeyinbrain.com/other/privacyPolicy.html")
                UIApplication.shared.open(url!, options: [:], completionHandler: nil)
    }
    
    func pressBackButton() {
        dismiss(animated: true, completion: nil)
    }
    
    func onelineEditDidDone(_ oneLineEditVC: OnelineEditVC, value: String, tag: Int, index: Int) {
        if oneLineEditVC.tag == 1 {
            loadingAnim.start()
            
            FirebaseManager.shared.setDisplayName(newName: value, result: {
                error in
                self.loadingAnim.stop()
                
                if let error = error {
                    let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                } else {
                    self.usernameButton.setTitle(title: FirebaseManager.shared.getUserInformation(type: .displayName) ?? NSLocalizedString("NoData", comment: ""))
                }
            })
        } else if oneLineEditVC.tag == 2 {
            self.loadingAnim.start()
            
            FirebaseManager.shared.deleteUser(password: value, result: {
                error in
                self.loadingAnim.stop()
                
                if let error = error {
                    let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                    DispatchQueue.main.async(execute: {
                        self.present(alert, animated: true, completion: nil)
                    })
                } else {
                    DispatchQueue.main.async(execute: {
                        let vc = SignInVC()
                        vc.modalPresentationStyle = .fullScreen
                        self.present(vc, animated: true, completion: nil)
                    })
                }
            })
        }
    }
    
    private func setLayout() {
        view.backgroundColor = Colors.shared.background
        
        topView.show(toView: view)
        topView.settitle(title: NSLocalizedString("Account", comment: ""), vc: self)
        topView.showBackButton()
        topView.backButtonDelegate = self
        
        let userStackViewSet = {
            self.view.addSubview(self.userStackView)
            self.userStackView.translatesAutoresizingMaskIntoConstraints = false
            self.userStackView.addArrangedSubview(self.usernameButton)
            self.userStackView.addArrangedSubview(self.emailButton)
            self.userStackView.axis = .vertical
            self.userStackView.distribution = .fill
            
            let top = NSLayoutConstraint(item: self.userStackView, attribute: .top, relatedBy: .equal, toItem: self.topView, attribute: .bottom, multiplier: 1, constant: Sizes.marginTop)
            let leading = NSLayoutConstraint(item: self.userStackView, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.userStackView, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([top, leading, trailing])
        }
        userStackViewSet()
        
        let menuStackViewSet = {
            self.view.addSubview(self.menuStackView)
            self.menuStackView.translatesAutoresizingMaskIntoConstraints = false
            self.menuStackView.addArrangedSubview(self.signOutButton)
            self.menuStackView.addArrangedSubview(self.privacyPolicyButton)
            self.menuStackView.axis = .vertical
            self.menuStackView.distribution = .fill
            ViewManager.appendLine(lineType: .top, view: self.menuStackView, spacing: Sizes.margin1)
            
            let top = NSLayoutConstraint(item: self.menuStackView, attribute: .top, relatedBy: .equal, toItem: self.userStackView, attribute: .bottom, multiplier: 1, constant: Sizes.marginTop)
            let leading = NSLayoutConstraint(item: self.menuStackView, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.menuStackView, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([top, leading, trailing])
        }
        menuStackViewSet()
        
        let deleteAccountButtonSet = {
            self.view.addSubview(self.deleteAccountButton)
            self.deleteAccountButton.translatesAutoresizingMaskIntoConstraints = false
            ViewManager.appendLine(lineType: .top, view: self.deleteAccountButton, spacing: Sizes.margin1)
            
            let top = NSLayoutConstraint(item: self.deleteAccountButton, attribute: .top, relatedBy: .equal, toItem: self.menuStackView, attribute: .bottom, multiplier: 1, constant: Sizes.marginTop)
            let leading = NSLayoutConstraint(item: self.deleteAccountButton, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.deleteAccountButton, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([top, leading, trailing])
        }
        deleteAccountButtonSet()
        
        let menuButtonsSet = {
            self.usernameButton.addTarget(self, action: #selector(self.pressUserButton(sender:)), for: .touchUpInside)
            self.usernameButton.setTitle(title: FirebaseManager.shared.getUserInformation(type: .displayName) ??  NSLocalizedString("NoData", comment: ""))
            self.usernameButton.hideArrowImageView()
            
            self.emailButton.setTitle(title: FirebaseManager.shared.getUserInformation(type: .email) ?? NSLocalizedString("NoData", comment: ""))
            self.emailButton.hideArrowImageView()
            self.emailButton.setTitleColor(color: Colors.shared.buttonSub)
            
            self.signOutButton.addTarget(self, action: #selector(self.pressSignOutButton(sender:)), for: .touchUpInside)
            self.signOutButton.setTitle(title: NSLocalizedString("SignOut", comment: ""))
            self.signOutButton.hideArrowImageView()
            
            self.privacyPolicyButton.addTarget(self, action: #selector(self.pressPrivacyPolicyButton(sender:)), for: .touchUpInside)
            self.privacyPolicyButton.setTitle(title: NSLocalizedString("PrivacyPolicy", comment: ""))
            self.privacyPolicyButton.hideArrowImageView()
            
            self.deleteAccountButton.addTarget(self, action: #selector(self.pressDeleteAccountButton(sender:)), for: .touchUpInside)
            self.deleteAccountButton.setTitle(title: NSLocalizedString("DeleteAccount", comment: ""))
            self.deleteAccountButton.hideArrowImageView()
        }
        menuButtonsSet()
    }
}
