//
//  SettingVC.swift
//  M.A.
//
//  Created by Min-Su Kim on 2020/07/27.
//  Copyright © 2020 Min-Su Kim. All rights reserved.
//

import Foundation
import UIKit

class SettingVC: UIViewController, TopViewBackButtonDelegate {
    private let topView = TopView()
    private let taskStackView = UIStackView()
    private let taskMarginTimeButton = MenuButton(id: "margin_time")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLayout()
    }
    
    @objc private func pressTaskMarginButton(sender: UIButton) {
        let vc = SetTaskDeadlineVC()
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: true , completion: nil)
    }
    
    func pressBackButton() {
        dismiss(animated: true, completion: nil)
    }
    
    private func setLayout() {
        view.backgroundColor = Colors.shared.background
        topView.show(toView: view)
        topView.showBackButton()
        topView.settitle(title: NSLocalizedString("Settings", comment: ""), vc: self)
        topView.backButtonDelegate = self
        
        let taskStackViewSet = {
            self.view.addSubview(self.taskStackView)
            self.taskStackView.translatesAutoresizingMaskIntoConstraints = false
            self.taskStackView.addArrangedSubview(self.taskMarginTimeButton)
            self.taskStackView.axis = .vertical
            self.taskStackView.distribution = .fill
            
            let top = NSLayoutConstraint(item: self.taskStackView, attribute: .top, relatedBy: .equal, toItem: self.topView, attribute: .bottom, multiplier: 1, constant: Sizes.marginTop)
            let leading = NSLayoutConstraint(item: self.taskStackView, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.taskStackView, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([top, leading, trailing])
        }
        taskStackViewSet()
        
        let menuButtonsSet = {
            self.taskMarginTimeButton.setTitle(title: NSLocalizedString("TaskMarginTime", comment: ""))
            self.taskMarginTimeButton.addTarget(self, action: #selector(self.pressTaskMarginButton(sender:)), for: .touchUpInside)
        }
        menuButtonsSet()
    }
}
