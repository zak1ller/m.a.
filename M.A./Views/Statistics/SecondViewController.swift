//
//  SecondViewController.swift
//  unspecified
//
//  Created by Min-Su Kim on 2020/07/13.
//  Copyright © 2020 Min-Su Kim. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, StatisticsListCellLongTouchDelegate, OnelineEditVCDelegate {
    private var loadingAnim: LoadingAnim!
    private var topView: TopView!
    private let tableStackView = UIStackView()
    private let totalView = UIView()
    private let totalBackView = UIView()
    private let totalTitleLabel = UILabel()
    private let totalValueStackView = UIStackView()
    private let totalValueLabel = UILabel()
    private let totalValueSubLabel = UILabel()
    private let totalGradeStackView = UIStackView()
    private let totalGradeView1 = UIView()
    private let totalGradeView2 = UIView()
    private let totalGradeView3 = UIView()
    private let totalGradeView4 = UIView()
    private let totalGradeView5 = UIView()
    private let messageView = UIView()
    private let messageBackView = UIButton(type: .system)
    private let messageTextLabel = UILabel()
    private let taskTableView = UITableView()
    
    private var taskList: [WorkManager.Work] = []
    private var isFirst = true
    private var message: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        topView = TopView()
        setLayout()
        loadingAnim = LoadingAnim(view: view)
        loadingAnim.setColor(color: Colors.shared.buttonSub)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        reloadTotalAvg()
        reloadStatistics()
    }

    @objc private func pressMessageButton(sender: UIButton) {
        let vc = OnelineEditVC()
        vc.delegate = self
        vc.setplaceholder(title: NSLocalizedString("ForMyLifeHabitText", comment: ""))
        
        if let m = message {
            vc.setFieldText(text: m)
        }
        
        vc.mintext = 0
        vc.maxtext = 20
        vc.isbecome = true
        vc.isdismissanimation = true
        present(vc, animated: true, completion: nil)
    }
    
    func onelineEditDidDone(_ oneLineEditVC: OnelineEditVC, value: String, tag: Int, index: Int) {
        loadingAnim.start()
        
        DispatchQueue.global().async(execute: {
            WorkManager.setMessage(message: value, result: {
                error in
                self.loadingAnim.stop()
                
                if let error = error {
                    let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                    DispatchQueue.main.async(execute: {
                        self.present(alert, animated: true, completion: nil)
                    })
                } else {
                    self.reloadMessage()
                }
            })
        })
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return taskList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StatisticsListCell", for: indexPath) as! StatisticsListCell
        cell.tag = indexPath.row
        cell.longTouchDelegate = self
        cell.setValue(task: taskList[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = DetailStatisticsVC()
        vc.setTask(task: taskList[indexPath.row])
        DispatchQueue.main.async(execute: {
            self.present(vc, animated: true, completion: nil)
        })
    }
    
    func statisticsListCellLongTouch(cell: StatisticsListCell, index: Int) {
        let item = taskList[index]
        
        let alert = UIAlertController(title: item.title, message: nil, preferredStyle: ViewManager.actionSheetForiPad())
        alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: NSLocalizedString("StatisticsTask", comment: ""), style: .default, handler: {
            (_) in
            self.loadingAnim.start()
            
            DispatchQueue.global().async(execute: {
                WorkManager.resetWork(title: item.title, result: {
                    error in
                    self.loadingAnim.stop()
                    
                    if let error = error {
                        let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                        DispatchQueue.main.async(execute: {
                            self.present(alert, animated: true, completion: nil)
                        })
                    } else {
                        self.reloadTotalAvg()
                        self.reloadStatistics()
                    }
                })
            })
        }))
        present(alert, animated: true, completion: nil)
    }
    
    private func reloadTotalAvg() {
        DispatchQueue.global().async(execute: {
            WorkManager.getTotalAvg(result: {
                error, avg in
                if let error = error {
                    let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                    DispatchQueue.main.async(execute: {
                        self.present(alert, animated: true, completion: nil)
                    })
                } else {
                    if let avg = avg {
                        DispatchQueue.main.async(execute: {
                            let v = Int(Double(avg.avgSuccess)*0.05)
                            self.totalValueLabel.text = "\(v)"
                            
                            if v == 1 {
                                self.totalGradeView1.backgroundColor = Colors.shared.point
                                self.totalGradeView2.backgroundColor = Colors.shared.background
                                self.totalGradeView3.backgroundColor = Colors.shared.background
                                self.totalGradeView4.backgroundColor = Colors.shared.background
                                self.totalGradeView5.backgroundColor = Colors.shared.background
                            } else if v == 2 {
                                self.totalGradeView1.backgroundColor = Colors.shared.point
                                self.totalGradeView2.backgroundColor = Colors.shared.point
                                self.totalGradeView3.backgroundColor = Colors.shared.background
                                self.totalGradeView4.backgroundColor = Colors.shared.background
                                self.totalGradeView5.backgroundColor = Colors.shared.background
                            } else if v == 3 {
                                self.totalGradeView1.backgroundColor = Colors.shared.point
                                self.totalGradeView2.backgroundColor = Colors.shared.point
                                self.totalGradeView3.backgroundColor = Colors.shared.point
                                self.totalGradeView4.backgroundColor = Colors.shared.background
                                self.totalGradeView5.backgroundColor = Colors.shared.background
                            } else if v == 4 {
                                self.totalGradeView1.backgroundColor = Colors.shared.point
                                self.totalGradeView2.backgroundColor = Colors.shared.point
                                self.totalGradeView3.backgroundColor = Colors.shared.point
                                self.totalGradeView4.backgroundColor = Colors.shared.point
                                self.totalGradeView5.backgroundColor = Colors.shared.background
                            } else if v == 5 {
                                self.totalGradeView1.backgroundColor = Colors.shared.point
                                self.totalGradeView2.backgroundColor = Colors.shared.point
                                self.totalGradeView3.backgroundColor = Colors.shared.point
                                self.totalGradeView4.backgroundColor = Colors.shared.point
                                self.totalGradeView5.backgroundColor = Colors.shared.point
                            } else {
                                self.totalGradeView1.backgroundColor = Colors.shared.background
                                self.totalGradeView2.backgroundColor = Colors.shared.background
                                self.totalGradeView3.backgroundColor = Colors.shared.background
                                self.totalGradeView4.backgroundColor = Colors.shared.background
                                self.totalGradeView5.backgroundColor = Colors.shared.background
                            }
                        })
                    }
                }
            })
        })
    }
    
    private func reloadStatistics() {
        DispatchQueue.global().async(execute: {
            WorkManager.getStatistics(result: {
                error, data in
                if let error = error {
                    let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                    DispatchQueue.main.async(execute: {
                        self.present(alert, animated: true, completion: nil)
                    })
                } else {
                    self.taskList = data
                    DispatchQueue.main.async(execute: {
                        self.taskTableView.reloadData()
                    })
                    
                    self.reloadMessage()
                }
            })
        })
    }
    
    private func reloadMessage() {
        DispatchQueue.global().async(execute: {
            WorkManager.getMessage(result: {
                error, message in
                if let error = error {
                    print(error)
                }
                if let message = message {
                    if message.count == 0 {
                        DispatchQueue.main.async(execute: {
                            self.message = nil
                            self.messageTextLabel.text = "\(NSLocalizedString("ForMyLifeHabitText", comment: ""))\n\(self.taskList.count)\(NSLocalizedString("TasksText", comment: ""))"
                        })
                    } else {
                        DispatchQueue.main.async(execute: {
                            self.message = message
                            self.messageTextLabel.text = "\(message)\n\(self.taskList.count)\(NSLocalizedString("TasksText", comment: ""))"
                        })
                    }
                }
            })
        })
    }
    
    private func setLayout() {
        view.backgroundColor = Colors.shared.background
        
        topView.show(toView: view)
        
        let tableStackViewSet = {
            self.view.addSubview(self.tableStackView)
            self.tableStackView.translatesAutoresizingMaskIntoConstraints = false
            self.tableStackView.addArrangedSubview(self.totalView)
            self.tableStackView.addArrangedSubview(self.messageView)
            self.tableStackView.addArrangedSubview(self.taskTableView)
            self.tableStackView.axis = .vertical
            self.tableStackView.distribution = .fill
            self.tableStackView.spacing = Sizes.margin1*0.5
            
            let top = NSLayoutConstraint(item: self.tableStackView, attribute: .top, relatedBy: .equal, toItem: self.topView, attribute: .bottom, multiplier: 1, constant: 0)
            let bottom = NSLayoutConstraint(item: self.tableStackView, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: 0)
            let leading = NSLayoutConstraint(item: self.tableStackView, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.tableStackView, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([top, bottom, leading, trailing])
        }
        tableStackViewSet()
        
        let tableViewSet = {
            self.taskTableView.register(StatisticsListCell.self, forCellReuseIdentifier: "StatisticsListCell")
            self.taskTableView.delegate = self
            self.taskTableView.dataSource = self
            self.taskTableView.separatorStyle = .none
            self.taskTableView.tableFooterView = UIView()
            self.taskTableView.rowHeight = UITableView.automaticDimension
            self.taskTableView.backgroundColor = Colors.shared.background
        }
        tableViewSet()
        
        let totalBackViewSet = {
            self.totalView.addSubview(self.totalBackView)
            self.totalBackView.translatesAutoresizingMaskIntoConstraints = false
            self.totalBackView.backgroundColor = Colors.shared.backgroundSub
            self.totalBackView.layer.cornerRadius = Sizes.radius
            
            let top = NSLayoutConstraint(item: self.totalBackView, attribute: .top, relatedBy: .equal, toItem: self.totalView, attribute: .top, multiplier: 1, constant: 0)
            let bottom = NSLayoutConstraint(item: self.totalBackView, attribute: .bottom, relatedBy: .equal, toItem: self.totalView, attribute: .bottom, multiplier: 1, constant: 0)
            let leading = NSLayoutConstraint(item: self.totalBackView, attribute: .leading, relatedBy: .equal, toItem: self.totalView, attribute: .leading, multiplier: 1, constant: Sizes.margin1)
            let trailing = NSLayoutConstraint(item: self.totalBackView, attribute: .trailing, relatedBy: .equal, toItem: self.totalView, attribute: .trailing, multiplier: 1, constant: -Sizes.margin1)
            NSLayoutConstraint.activate([top, bottom, leading, trailing])
        }
        totalBackViewSet()
        
        let messageBackViewSet = {
            self.messageView.addSubview(self.messageBackView)
            self.messageBackView.translatesAutoresizingMaskIntoConstraints = false
            self.messageBackView.addTarget(self, action: #selector(self.pressMessageButton(sender:)), for: .touchUpInside)
            self.messageBackView.backgroundColor = Colors.shared.backgroundSub
            self.messageBackView.layer.cornerRadius = Sizes.radius
            
            let top = NSLayoutConstraint(item: self.messageBackView, attribute: .top, relatedBy: .equal, toItem: self.messageView, attribute: .top, multiplier: 1, constant: 0)
            let bottom = NSLayoutConstraint(item: self.messageBackView, attribute: .bottom, relatedBy: .equal, toItem: self.messageView, attribute: .bottom, multiplier: 1, constant: 0)
            let leading = NSLayoutConstraint(item: self.messageBackView, attribute: .leading, relatedBy: .equal, toItem: self.messageView, attribute: .leading, multiplier: 1, constant: Sizes.margin1)
            let trailing = NSLayoutConstraint(item: self.messageBackView, attribute: .trailing, relatedBy: .equal, toItem: self.messageView, attribute: .trailing, multiplier: 1, constant: -Sizes.margin1)
            NSLayoutConstraint.activate([top, bottom, leading, trailing])
        }
        messageBackViewSet()
        
        let totalTitleLabelSet = {
            self.totalView.addSubview(self.totalTitleLabel)
            self.totalTitleLabel.translatesAutoresizingMaskIntoConstraints = false
            self.totalTitleLabel.text = NSLocalizedString("TotalGradeTitle", comment: "")
            self.totalTitleLabel.textColor = Colors.shared.black
            self.totalTitleLabel.textAlignment = .center
            
            if Sizes.isiPad() {
                self.totalTitleLabel.font = UIFont.systemFont(ofSize: 25, weight: .medium)
            } else {
                self.totalTitleLabel.font = UIFont.systemFont(ofSize: 17, weight: .medium)
            }
            
            let top = NSLayoutConstraint(item: self.totalTitleLabel, attribute: .top, relatedBy: .equal, toItem: self.totalView, attribute: .top, multiplier: 1, constant: Sizes.margin1)
            let leading = NSLayoutConstraint(item: self.totalTitleLabel, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: Sizes.margin1)
            let trailing = NSLayoutConstraint(item: self.totalTitleLabel, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: -Sizes.margin1)
            NSLayoutConstraint.activate([top, leading, trailing])
        }
        totalTitleLabelSet()
        
        let totalValueStackViewSet = {
            self.totalView.addSubview(self.totalValueStackView)
            self.totalValueStackView.translatesAutoresizingMaskIntoConstraints = false
            self.totalValueStackView.addArrangedSubview(self.totalValueSubLabel)
            self.totalValueStackView.addArrangedSubview(self.totalValueLabel)
            self.totalValueStackView.axis = .horizontal
            self.totalValueStackView.distribution = .fill
            
            let top = NSLayoutConstraint(item: self.totalValueStackView, attribute: .top, relatedBy: .equal, toItem: self.totalTitleLabel, attribute: .bottom, multiplier: 1, constant: 8)
            let centerX = NSLayoutConstraint(item: self.totalValueStackView, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([top, centerX])
            
            let valueFontSet = {
                self.totalValueLabel.textColor = Colors.shared.point
                self.totalValueSubLabel.text = "Lv."
                self.totalValueSubLabel.textColor = Colors.shared.point
                
                if Sizes.isiPad() {
                    self.totalValueLabel.font = UIFont.systemFont(ofSize: 25, weight: .bold)
                    self.totalValueSubLabel.font = UIFont.systemFont(ofSize: 25, weight: .bold)
                } else {
                    self.totalValueLabel.font = UIFont.systemFont(ofSize: 17, weight: .bold)
                    self.totalValueSubLabel.font = UIFont.systemFont(ofSize: 17, weight: .bold)
                }
            }
            valueFontSet()
        }
        totalValueStackViewSet()
        
        let totalGradeStackViewSet = {
            self.totalView.addSubview(self.totalGradeStackView)
            self.totalGradeStackView.translatesAutoresizingMaskIntoConstraints = false
            self.totalGradeStackView.addArrangedSubview(self.totalGradeView1)
            self.totalGradeStackView.addArrangedSubview(self.totalGradeView2)
            self.totalGradeStackView.addArrangedSubview(self.totalGradeView3)
            self.totalGradeStackView.addArrangedSubview(self.totalGradeView4)
            self.totalGradeStackView.addArrangedSubview(self.totalGradeView5)
            self.totalGradeStackView.axis = .horizontal
            self.totalGradeStackView.distribution = .fillEqually
            self.totalGradeStackView.spacing = 4
            
            let top = NSLayoutConstraint(item: self.totalGradeStackView, attribute: .top, relatedBy: .equal, toItem: self.totalValueStackView, attribute: .bottom, multiplier: 1, constant: Sizes.margin1*0.5)
            let bottom = NSLayoutConstraint(item: self.totalGradeStackView, attribute: .bottom, relatedBy: .equal, toItem: self.totalView, attribute: .bottom, multiplier: 1, constant: -Sizes.margin1)
            let centerX = NSLayoutConstraint(item: self.totalGradeStackView, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1, constant: 0)
            let width = NSLayoutConstraint(item: self.totalGradeStackView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 200)
            let height = NSLayoutConstraint(item: self.totalGradeStackView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 16)
            NSLayoutConstraint.activate([top, bottom, centerX, width, height])
        }
        totalGradeStackViewSet()
        
        let gradeViewsSet = {
            let radius: CGFloat = 4
            let backgroundColor = Colors.shared.background
            
            self.totalGradeView1.backgroundColor = backgroundColor
            self.totalGradeView1.layer.cornerRadius = radius
            self.totalGradeView2.backgroundColor = backgroundColor
            self.totalGradeView2.layer.cornerRadius = radius
            self.totalGradeView3.backgroundColor = backgroundColor
            self.totalGradeView3.layer.cornerRadius = radius
            self.totalGradeView4.backgroundColor = backgroundColor
            self.totalGradeView4.layer.cornerRadius = radius
            self.totalGradeView5.backgroundColor = backgroundColor
            self.totalGradeView5.layer.cornerRadius = radius
        }
        gradeViewsSet()
        
        let messageTextLabelSet = {
            self.messageView.addSubview(self.messageTextLabel)
            self.messageTextLabel.translatesAutoresizingMaskIntoConstraints = false
            self.messageTextLabel.textColor = Colors.shared.textSub
            self.messageTextLabel.textAlignment = .center
            self.messageTextLabel.numberOfLines = 0
            
            if Sizes.isiPad() {
                self.messageTextLabel.font = UIFont.systemFont(ofSize: 20, weight: .medium)
            } else {
                self.messageTextLabel.font = Fonts.type1
            }
            
            let top = NSLayoutConstraint(item: self.messageTextLabel, attribute: .top, relatedBy: .equal, toItem: self.messageView, attribute: .top, multiplier: 1, constant: Sizes.margin1)
            let bottom = NSLayoutConstraint(item: self.messageTextLabel, attribute: .bottom, relatedBy: .equal, toItem: self.messageView, attribute: .bottom, multiplier: 1, constant: -Sizes.margin1)
            let centerX = NSLayoutConstraint(item: self.messageTextLabel, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([top, bottom, centerX])
        }
        messageTextLabelSet()
    }
}

