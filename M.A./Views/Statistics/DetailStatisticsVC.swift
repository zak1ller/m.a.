//
//  DetailStatisticsVC.swift
//  M.A.
//
//  Created by Min-Su Kim on 2020/07/31.
//  Copyright © 2020 Min-Su Kim. All rights reserved.
//

import Foundation
import UIKit

class DetailStatisticsVC: UIViewController, TopViewBackButtonDelegate, TopViewRightImageButtonDelegate, UITableViewDelegate, UITableViewDataSource {
    private var loadingAnim: LoadingAnim!
    private let topView = TopView()
    private let tableView = UITableView()
    
    private var task = WorkManager.Work()
    private var list: [Detail] = []
    
    private struct Detail {
        var title: String?
        var value: String?
        var image: UIImage?
    }
    
    deinit {
        print("Detail Statistics VC Deinited.")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLayout()
        
        loadingAnim = LoadingAnim(view: view)
        loadingAnim.setColor(color: Colors.shared.buttonSub)
    }
    
    func pressBackButton() {
        dismiss(animated: true, completion: nil)
    }
    
    func pressRightImageButton() {
        let alert = UIAlertController(title: NSLocalizedString("TaskResetMessage", comment: ""), message: nil, preferredStyle: ViewManager.actionSheetForiPad())
        alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .default, handler: {
            (_) in
            self.loadingAnim.start()
            
            DispatchQueue.global().async(execute: {
                WorkManager.resetWork(title: self.task.title, result: {
                    error in
                    self.loadingAnim.stop()
                    
                    if let error = error {
                        let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                        DispatchQueue.main.async(execute: {
                            self.present(alert, animated: true, completion: nil)
                        })
                    } else {
                        UserDefaultManager.shared.isListInit = true
                        
                        self.task.comboCount = 0
                        self.task.failedCount = 0
                        self.task.maxComboCount = 0
                        self.task.marks = "?"
                        self.task.successCount = 0
                        self.task.failedCount = 0
                        self.task.totalCount = 0
                        self.task.successPercent = 0
                        self.setTask(task: self.task)
                        DispatchQueue.main.async(execute: {
                            self.tableView.reloadData()
                        })
                    }
                })
            })
        }))
        present(alert, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DetailStatisticsCell", for: indexPath) as! DetailStatisticsCell
        
        if list[indexPath.row].title == nil {
            cell.titleLabel.isHidden = true
        } else {
            cell.titleLabel.isHidden = false
            cell.titleLabel.text = list[indexPath.row].title
        }
        
        if list[indexPath.row].value == nil {
            cell.valueLabel.isHidden = true
        } else {
            cell.valueLabel.isHidden = false
            cell.valueLabel.text = list[indexPath.row].value
        }
        
        cell.iconImageView.image = list[indexPath.row].image
        cell.iconImageView.setImageColor(color: Colors.shared.black)
        return cell
    }
    
    func setTask(task: WorkManager.Work) {
        list.removeAll()
        
        self.task = task
        
        var data = Detail()
        data.title = NSLocalizedString("DetailTotalCount", comment: "")
        data.value = "\(task.totalCount)"
        data.image = UIImage(named: "total_count.png")
        list.append(data)
        
        data = Detail()
        data.title = NSLocalizedString("DetailSuccessCount", comment: "")
        data.value = "\(task.successCount)"
        data.image = UIImage(named: "success_count.png")
        list.append(data)
        
        data = Detail()
        data.title = NSLocalizedString("DetailFailedCount", comment: "")
        data.value = "\(task.failedCount)"
        data.image = UIImage(named: "failure_count.png")
        list.append(data)
        
        data = Detail()
        data.title = NSLocalizedString("DetailRate", comment: "")
        data.value = "\(task.successPercent)%"
        data.image = UIImage(named: "rate.png")
        list.append(data)
        
        data = Detail()
        data.title = NSLocalizedString("DetailGrade", comment: "")
        data.value = "Lv.\(task.marks)"
        data.image = UIImage(named: "grade.png")
        list.append(data)
        
        data = Detail()
        data.title = NSLocalizedString("DetailMaxCombo", comment: "")
        data.value = "\(task.maxComboCount)"
        data.image = UIImage(named: "max_combo.png")
        list.append(data)
    }
    
    private func setLayout() {
        view.backgroundColor = Colors.shared.backgroundSub
        
        if #available(iOS 13, *) {
            topView.showOnSubView(toView: view)
        } else {
            topView.show(toView: view)
        }
        
        topView.settitle(title: task.title, vc: self)
        topView.showBackButton()
        topView.backButtonDelegate = self
        topView.showRightImageButton(imagee: UIImage(named: "reset.png"))
        topView.rightImageButtonDelegate = self
        
        let tableViewSet = {
            self.view.addSubview(self.tableView)
            self.tableView.translatesAutoresizingMaskIntoConstraints = false
            self.tableView.register(DetailStatisticsCell.self, forCellReuseIdentifier: "DetailStatisticsCell")
            self.tableView.delegate = self
            self.tableView.dataSource = self
            self.tableView.rowHeight = UITableView.automaticDimension
            self.tableView.tableFooterView = UIView()
            self.tableView.backgroundColor = Colors.shared.backgroundSub
            
            let top = NSLayoutConstraint(item: self.tableView, attribute: .top, relatedBy: .equal, toItem: self.topView, attribute: .bottom, multiplier: 1, constant: Sizes.marginTop)
            let bottom = NSLayoutConstraint(item: self.tableView, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: 0)
            let leading = NSLayoutConstraint(item: self.tableView, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.tableView, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([top, bottom, leading, trailing])
        }
        tableViewSet()
    }
}
