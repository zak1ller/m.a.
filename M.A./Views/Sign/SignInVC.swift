//
//  SignInVC.swift
//  unspecified
//
//  Created by Min-Su Kim on 2020/07/21.
//  Copyright © 2020 Min-Su Kim. All rights reserved.
//

import Foundation
import UIKit
import GoogleSignIn
import FirebaseAuth
import FBSDKLoginKit
import CryptoKit
import AuthenticationServices

protocol SignInVcDelegate: class {
    func signInVcSignedIn()
}

class SignInVC: UIViewController, UITextFieldDelegate, GIDSignInDelegate, LoginButtonDelegate, OnelineEditVCDelegate {
    private var loadingAnim: LoadingAnim!
    private let contentView = UIStackView()
    private let signInFieldStackView = UIStackView()
    private let idFieldView = UIView()
    private let pwFieldView = UIView()
    private let signInButton = UIButton()
    private let idImageView = UIImageView()
    private let pwImageView = UIImageView()
    private let idField = UITextField()
    private let pwField = UITextField()
    private let forgotPasswordButton = UIButton(type: .system)
    private let signUpButton = UIButton(type: .system)
    private let signUpStackView = UIStackView()
    private let signUpFirstLabel = UILabel()
    private let signUpSecondLabel = UILabel()
    private let orLabel = UILabel()
    private let socialSignInView = UIView()
    private let socialSignInStackView = UIStackView()
    private let googleSignInStackView = UIStackView()
    private let googleSignInButton = UIButton(type: .system)
    private let facebookSignInStackView = UIStackView()
    private let facebookSignInButton = UIButton(type: .system)
    private let appleSignInStackView = UIStackView()
    private let appleSignInButton = UIButton(type: .system)
    
    weak var delegate: SignInVcDelegate?
    private var contentViewCenterYConstraint: NSLayoutConstraint!
    fileprivate var currentNonce: String?

    deinit {
        print("Sign In VC Deinited.")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLayout()
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: UIWindow.keyboardWillShowNotification,
            object: nil)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHide(_:)),
            name: UIWindow.keyboardWillHideNotification,
            object: nil)
        
        GIDSignIn.sharedInstance().presentingViewController = self
        
        loadingAnim = LoadingAnim(view: view)
        loadingAnim.setColor(color: Colors.shared.buttonSub)
    }
    
    @objc private func pressForgotPasswordButton(sender: UIButton) {
        let vc = OnelineEditVC()
        vc.delegate = self
        vc.maxtext = 50
        vc.isbecome = true
        vc.settitle(title: NSLocalizedString("ResetPasssword", comment: ""))
        vc.setplaceholder(title: NSLocalizedString("FieldTitleEmail", comment: ""))
        present(vc, animated: true, completion: nil)
    }
    
    @objc func keyboardWillShow(_ notificaiton: Notification) {
        if let keyboardFrame: NSValue = notificaiton.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardHeight = keyboardFrame.cgRectValue.height
//            signInButton.isHidden = false
            signInButton.alpha = 1
            orLabel.isHidden = true
            socialSignInView.isHidden = true
            googleSignInStackView.isHidden = true
            facebookSignInStackView.isHidden = true
            
            if #available(iOS 13, *) {
                appleSignInStackView.isHidden = true
            }
            
            contentViewCenterYConstraint.constant = -keyboardHeight/2
        }
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
//        signInButton.isHidden = true
        signInButton.alpha = 0
        orLabel.isHidden = false
        socialSignInView.isHidden = false
        googleSignInStackView.isHidden = false
        facebookSignInStackView.isHidden = false
        
        if #available(iOS 13, *) {
            appleSignInStackView.isHidden = false
        }
        
        contentViewCenterYConstraint.constant = -Sizes.bottomSpace+Sizes.button2
    }
    
    @objc private func pressBackView() {
        view.endEditing(true)
    }
    
    @objc private func pressContentview() {
    
    }
    
    @objc private func pressIdFieldView() {
        idField.becomeFirstResponder()
    }
    
    @objc private func pressPwFieldView() {
        pwField.becomeFirstResponder()
    }
    
    @objc private func pressSignUpButton(sender: UIButton) {
        let vc = SignUpVC()
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: true, completion: nil)
    }
    
    @objc private func pressSignInButton(sender: UIButton) {
        let email = idField.text ?? ""
        let password = pwField.text ?? ""
        
        self.loadingAnim.start()
        
        FirebaseManager.shared.signInWithEmail(email: email, password: password, result: {
            error in
            self.loadingAnim.stop()
            
            if let error = error {
                let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                
                if error == NSLocalizedString("PleaseDoneVerifyEmail", comment: "") {
                    alert.addAction(UIAlertAction(title: NSLocalizedString("ResentVerifyMail", comment: ""), style: .default, handler: {
                        (_) in
                        self.loadingAnim.start()
                        
                        FirebaseManager.shared.sendVerificEmail(result: {
                            error in
                            self.loadingAnim.stop()
                            
                            let alert = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                            
                            if let error = error {
                                alert.title = error
                            } else {
                                alert.title = NSLocalizedString("ResentVerifyEmail", comment: "")
                            }
                            DispatchQueue.main.async(execute: {
                                self.present(alert, animated: true, completion: nil)
                            })
                        })
                    }))
                }
                
                DispatchQueue.main.async(execute: {
                    self.present(alert, animated: true, completion: nil)
                })
            } else {
                DispatchQueue.main.async(execute: {
                    self.dismiss(animated: true, completion: {
                        self.delegate?.signInVcSignedIn()
                    })
                })
            }
        })
    }
    
    @objc private func pressGoogleSignInButton(sedner: UIButton) {
        GIDSignIn.sharedInstance()?.signIn()
        GIDSignIn.sharedInstance()?.delegate = self
    }
    
    @objc private func pressFacebookSignInButton(sedner: UIButton) {
        let bt = FBLoginButton()
        bt.delegate = self
        bt.permissions = ["public_profile", "email"]
        bt.sendActions(for: .touchUpInside)
    }
    
    @available(iOS 13, *)
    @objc private func pressAppleSignInButton(sender: UIButton) {
        startSignInWithAppleFlow()
    }
    
    @available(iOS 13, *)
    func startSignInWithAppleFlow() {
        let nonce = FirebaseManager.shared.randomNonceString()
        currentNonce = nonce
        
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]
        request.nonce = sha256(nonce)

        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        authorizationController.presentationContextProvider = self
        authorizationController.performRequests()
    }

    @available(iOS 13, *)
    private func sha256(_ input: String) -> String {
        let inputData = Data(input.utf8)
        let hashedData = SHA256.hash(data: inputData)
        let hashString = hashedData.compactMap {
            return String(format: "%02x", $0)
        }.joined()

        return hashString
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.tag == 1 {
            pwField.becomeFirstResponder()
        } else if textField.tag == 2 {
            pressSignInButton(sender: signInButton)
        }
        return true
    }
    
    func onelineEditDidDone(_ oneLineEditVC: OnelineEditVC, value: String, tag: Int, index: Int) {
        loadingAnim.start()
        
        FirebaseManager.shared.sendResetPasswordEmail(email: value, result: {
            error in
            self.loadingAnim.stop()
            
            let alert = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
            
            if let error = error {
                alert.title = error
            } else {
                alert.title = NSLocalizedString("SentResetPassWordEmailMessage", comment: "")
            }
            
            self.present(alert, animated: true, completion: nil)
        })
    }
    
    func loginButtonDidLogOut(_ loginButton: FBLoginButton) {
        
    }
    
    func loginButton(_ loginButton: FBLoginButton, didCompleteWith result: LoginManagerLoginResult?, error: Error?) {
        if let error = error {
            let alert = UIAlertController(title: error.localizedDescription, message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("Confirmation", comment: ""), style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
            return
        }
        
        if AccessToken.current == nil {
            return
        }
        
        self.loadingAnim.start()
        
        let credential = FacebookAuthProvider.credential(withAccessToken: AccessToken.current!.tokenString)
        Auth.auth().signIn(with: credential) { (authResult, error) in
            if let error = error {
                self.loadingAnim.stop()
                
                let alert = UIAlertController(title: error.localizedDescription, message: nil, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            } else {
                DispatchQueue.global().async(execute: {
                    FirebaseManager.shared.updateUser(result: {
                        error in
                        self.loadingAnim.stop()
                        
                        if let error = error {
                            let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                            DispatchQueue.main.async(execute: {
                                self.present(alert, animated: true, completion: {
                                    FirebaseManager.shared.signOut(result: {
                                        error in
                                        if let error = error {
                                            let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                                            alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                                            DispatchQueue.main.async(execute: {
                                                self.present(alert, animated: true, completion: nil)
                                            })
                                        }
                                    })
                                })
                            })
                        } else {
                            DispatchQueue.main.async(execute: {
                                self.dismiss(animated: true, completion: {
                                    self.delegate?.signInVcSignedIn()
                                })
                            })
                        }
                    })
                })
            }
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            let alert = UIAlertController(title: error.localizedDescription, message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
            return
        }
        
        guard let authentication = user.authentication else { return }
        
        self.loadingAnim.start()
        
        let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken,
                                                          accessToken: authentication.accessToken)
        Auth.auth().signIn(with: credential, completion: {
            result, error in
            if let error = error {
                self.loadingAnim.stop()
                
                let alert = UIAlertController(title: error.localizedDescription, message: nil, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            } else {
                DispatchQueue.global().async(execute: {
                    FirebaseManager.shared.updateUser(result: {
                        error in
                        self.loadingAnim.stop()
                        
                        if let error = error {
                            let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                            DispatchQueue.main.async(execute: {
                                self.present(alert, animated: true, completion: {
                                    FirebaseManager.shared.signOut(result: {
                                        error in
                                        if let error = error {
                                            let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                                            alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                                            DispatchQueue.main.async(execute: {
                                                self.present(alert, animated: true, completion: nil)
                                            })
                                        }
                                    })
                                })
                            })
                        } else {
                            DispatchQueue.main.async(execute: {
                                self.dismiss(animated: true, completion: {
                                    self.delegate?.signInVcSignedIn()
                                })
                            })
                        }
                    })
                })
            }
        })
    }
    
    private func setLayout() {
        view.backgroundColor = Colors.shared.background
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(pressBackView)))
        
        let signUpButtonSet = {
            self.view.addSubview(self.signUpButton)
            self.signUpButton.translatesAutoresizingMaskIntoConstraints = false
            self.signUpButton.addTarget(self, action: #selector(self.pressSignUpButton(sender:)), for: .touchUpInside)
            
            let height = NSLayoutConstraint(item: self.signUpButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Sizes.button2)
            let centerX = NSLayoutConstraint(item: self.signUpButton, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1, constant: 0)
            let bottom = NSLayoutConstraint(item: self.signUpButton, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: -Sizes.bottomSpace)
            NSLayoutConstraint.activate([height, centerX, bottom])
        }
        signUpButtonSet()
        
        let signUpStackViewSet = {
            self.signUpButton.addSubview(self.signUpStackView)
            self.signUpStackView.translatesAutoresizingMaskIntoConstraints = false
            self.signUpStackView.addArrangedSubview(self.signUpFirstLabel)
            self.signUpStackView.addArrangedSubview(self.signUpSecondLabel)
            self.signUpStackView.axis = .horizontal
            self.signUpStackView.distribution = .fill
            self.signUpStackView.isUserInteractionEnabled = false
            
            let top = NSLayoutConstraint(item: self.signUpStackView, attribute: .top, relatedBy: .equal, toItem: self.signUpButton, attribute: .top, multiplier: 1, constant: 0)
            let bottom = NSLayoutConstraint(item: self.signUpStackView, attribute: .bottom, relatedBy: .equal, toItem: self.signUpButton, attribute: .bottom, multiplier: 1, constant: 0)
            let leading = NSLayoutConstraint(item: self.signUpStackView, attribute: .leading, relatedBy: .equal, toItem: self.signUpButton, attribute: .leading, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.signUpStackView, attribute: .trailing, relatedBy: .equal, toItem: self.signUpButton, attribute: .trailing, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([top, bottom, leading, trailing])
        }
        signUpStackViewSet()
        
        let signUpFontSet = {
            self.signUpFirstLabel.text = NSLocalizedString("DonotHaveAccount", comment: "")
            self.signUpFirstLabel.textColor = Colors.shared.textSub
            self.signUpSecondLabel.text = " "+NSLocalizedString("Signup", comment: "")
            self.signUpSecondLabel.textColor = Colors.shared.system
            
            if Sizes.isiPad() {
                self.signUpFirstLabel.font = UIFont.systemFont(ofSize: 17, weight: .medium)
                self.signUpSecondLabel.font = UIFont.systemFont(ofSize: 17, weight: .bold)
            } else {
                self.signUpFirstLabel.font = Fonts.type2
                self.signUpSecondLabel.font = UIFont.systemFont(ofSize: 12, weight: .bold)
            }
        }
        signUpFontSet()
        
        let contentViewSet = {
            self.view.addSubview(self.contentView)
            self.contentView.translatesAutoresizingMaskIntoConstraints = false
            self.contentView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.pressContentview)))
            self.contentView.addArrangedSubview(self.signInFieldStackView)
            self.contentView.addArrangedSubview(self.orLabel)
            self.contentView.addArrangedSubview(self.socialSignInView)
            self.contentView.axis = .vertical
            
            self.contentViewCenterYConstraint = NSLayoutConstraint(item: self.contentView, attribute: .centerY, relatedBy: .equal, toItem: self.view, attribute: .centerY, multiplier: 1, constant: 0)
            let leading = NSLayoutConstraint(item: self.contentView, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: Sizes.margin3)
            let trailing = NSLayoutConstraint(item: self.contentView, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: -Sizes.margin3)
            NSLayoutConstraint.activate([self.contentViewCenterYConstraint, leading, trailing])
        }
        contentViewSet()
        
        let signInFieldStackViewSet = {
            self.signInFieldStackView.addArrangedSubview(self.idFieldView)
            self.signInFieldStackView.addArrangedSubview(self.pwFieldView)
            self.signInFieldStackView.addArrangedSubview(self.signInButton)
            self.signInFieldStackView.axis = .vertical
            self.signInFieldStackView.distribution = .fill
            self.signInFieldStackView.spacing = Sizes.margin1*0.5
        }
        signInFieldStackViewSet()
        
        let signInButtonSet = {
            self.signInButton.translatesAutoresizingMaskIntoConstraints = false
            self.signInButton.addTarget(self, action: #selector(self.pressSignInButton(sender:)), for: .touchUpInside)
            self.signInButton.setTitle(NSLocalizedString("SignIn", comment: ""), for: .normal)
            self.signInButton.setTitleColor(Colors.shared.background, for: .normal)
            self.signInButton.backgroundColor = Colors.shared.point
            self.signInButton.layer.cornerRadius = Sizes.radius
            self.signInButton.alpha = 0
            
            if Sizes.isiPad() {
                self.signInButton.titleLabel?.font = UIFont.systemFont(ofSize: 25, weight: .bold)
            } else {
                self.signInButton.titleLabel?.font = Fonts.title
            }
            
            let height = NSLayoutConstraint(item: self.signInButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Sizes.button)
            NSLayoutConstraint.activate([height])
        }
        signInButtonSet()
        
        let idFieldViewSet = {
            self.idFieldView.translatesAutoresizingMaskIntoConstraints = false
            self.idFieldView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.pressIdFieldView)))
            self.idFieldView.backgroundColor = Colors.shared.backgroundSub
            self.idFieldView.layer.cornerRadius = Sizes.radius
            self.idFieldView.isUserInteractionEnabled = true
            
            let height = NSLayoutConstraint(item: self.idFieldView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Sizes.button)
            NSLayoutConstraint.activate([height])
        }
        idFieldViewSet()
        
        let pwFieldViewSet = {
            self.pwFieldView.translatesAutoresizingMaskIntoConstraints = false
            self.pwFieldView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.pressPwFieldView)))
            self.pwFieldView.backgroundColor = Colors.shared.backgroundSub
            self.pwFieldView.layer.cornerRadius = Sizes.radius
            self.pwFieldView.isUserInteractionEnabled = true
            
            let height = NSLayoutConstraint(item: self.pwFieldView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Sizes.button)
            NSLayoutConstraint.activate([height])
        }
        pwFieldViewSet()
        
        let idImageViewSet = {
            self.idFieldView.addSubview(self.idImageView)
            self.idImageView.translatesAutoresizingMaskIntoConstraints = false
            self.idImageView.image = UIImage(named: "email.png")
            self.idImageView.setImageColor(color: Colors.shared.textSub)
            
            let width = NSLayoutConstraint(item: self.idImageView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Sizes.button3)
            let height = NSLayoutConstraint(item: self.idImageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Sizes.button3)
            let leading = NSLayoutConstraint(item: self.idImageView, attribute: .leading, relatedBy: .equal, toItem: self.idFieldView, attribute: .leading, multiplier: 1, constant: Sizes.margin1)
            let centerY = NSLayoutConstraint(item: self.idImageView, attribute: .centerY, relatedBy: .equal, toItem: self.idFieldView, attribute: .centerY, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([width, height, leading, centerY])
        }
        idImageViewSet()
        
        let pwImageViewSet = {
            self.pwFieldView.addSubview(self.pwImageView)
            self.pwImageView.translatesAutoresizingMaskIntoConstraints = false
            self.pwImageView.image = UIImage(named: "password.png")
            self.pwImageView.setImageColor(color: Colors.shared.textSub)
            
            let width = NSLayoutConstraint(item: self.pwImageView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Sizes.button3)
            let height = NSLayoutConstraint(item: self.pwImageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Sizes.button3)
            let leading = NSLayoutConstraint(item: self.pwImageView, attribute: .leading, relatedBy: .equal, toItem: self.pwFieldView, attribute: .leading, multiplier: 1, constant: Sizes.margin1)
            let centerY = NSLayoutConstraint(item: self.pwImageView, attribute: .centerY, relatedBy: .equal, toItem: self.pwFieldView, attribute: .centerY, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([width, height, leading, centerY])
        }
        pwImageViewSet()
        
        let idFieldSet = {
            self.idFieldView.addSubview(self.idField)
            self.idField.translatesAutoresizingMaskIntoConstraints = false
            self.idField.tag = 1
            self.idField.delegate = self
            self.idField.tintColor = Colors.shared.point
            self.idField.textColor = Colors.shared.black
            self.idField.placeholder = NSLocalizedString("Email", comment: "")
            self.idField.keyboardType = .emailAddress
            self.idField.returnKeyType = .next
            
            if Sizes.isiPad() {
                self.idField.font = UIFont.systemFont(ofSize: 20, weight: .medium)
            } else {
                self.idField.font = Fonts.type1
            }
            
            let centerY = NSLayoutConstraint(item: self.idField, attribute: .centerY, relatedBy: .equal, toItem: self.idFieldView, attribute: .centerY, multiplier: 1, constant: 0)
            let leading = NSLayoutConstraint(item: self.idField, attribute: .leading, relatedBy: .equal, toItem: self.idImageView, attribute: .trailing, multiplier: 1, constant: Sizes.margin1*0.5)
            let trailing = NSLayoutConstraint(item: self.idField, attribute: .trailing, relatedBy: .equal, toItem: self.idFieldView, attribute: .trailing, multiplier: 1, constant: -Sizes.margin1)
            NSLayoutConstraint.activate([centerY, leading, trailing])
        }
        idFieldSet()
        
        let pwFieldSet = {
            self.pwFieldView.addSubview(self.pwField)
            self.pwField.translatesAutoresizingMaskIntoConstraints = false
            self.pwField.tag = 2
            self.pwField.delegate = self
            self.pwField.tintColor = Colors.shared.point
            self.pwField.textColor = Colors.shared.black
            self.pwField.placeholder = NSLocalizedString("Password", comment: "")
            self.pwField.keyboardType = .emailAddress
            self.pwField.isSecureTextEntry = true
            self.pwField.returnKeyType = .join
            
            if Sizes.isiPad() {
                self.pwField.font = UIFont.systemFont(ofSize: 20, weight: .medium)
            } else {
                self.pwField.font = Fonts.type1
            }
            
            let centerY = NSLayoutConstraint(item: self.pwField, attribute: .centerY, relatedBy: .equal, toItem: self.pwFieldView, attribute: .centerY, multiplier: 1, constant: 0)
            let leading = NSLayoutConstraint(item: self.pwField, attribute: .leading, relatedBy: .equal, toItem: self.pwImageView, attribute: .trailing, multiplier: 1, constant: Sizes.margin1*0.5)
            let trailing = NSLayoutConstraint(item: self.pwField, attribute: .trailing, relatedBy: .equal, toItem: self.pwFieldView, attribute: .trailing, multiplier: 1, constant: -Sizes.margin1)
            NSLayoutConstraint.activate([centerY, leading, trailing])
        }
        pwFieldSet()
        
        let forgotButtonSet = {
            self.pwFieldView.addSubview(self.forgotPasswordButton)
            self.forgotPasswordButton.translatesAutoresizingMaskIntoConstraints = false
            self.forgotPasswordButton.addTarget(self, action: #selector(self.pressForgotPasswordButton(sender:)), for: .touchUpInside)
            self.forgotPasswordButton.setTitle(NSLocalizedString("ForgotPassword", comment: ""), for: .normal)
            self.forgotPasswordButton.setTitleColor(Colors.shared.buttonSub, for: .normal)
            self.forgotPasswordButton.titleLabel?.font = UIFont.systemFont(ofSize: 10, weight: .bold)
            
            if Sizes.isiPad() {
                self.forgotPasswordButton.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: .bold)
            } else {
                self.forgotPasswordButton.titleLabel?.font = UIFont.systemFont(ofSize: 10, weight: .bold)
            }
            
            let centerY = NSLayoutConstraint(item: self.forgotPasswordButton, attribute: .centerY, relatedBy: .equal, toItem: self.pwFieldView, attribute: .centerY, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.forgotPasswordButton, attribute: .trailing, relatedBy: .equal, toItem: self.pwFieldView, attribute: .trailing, multiplier: 1, constant: -Sizes.margin1)
            NSLayoutConstraint.activate([centerY, trailing])
        }
        forgotButtonSet()
        
        let orLabelSet = {
            self.orLabel.translatesAutoresizingMaskIntoConstraints = false
            self.orLabel.text = "⎯⎯⎯⎯⎯⎯⎯⎯ \(NSLocalizedString("Or", comment: "")) ⎯⎯⎯⎯⎯⎯⎯⎯"
            self.orLabel.textAlignment = .center
            self.orLabel.textColor = Colors.shared.buttonSub
            
            if Sizes.isiPad() {
                self.orLabel.font = UIFont.systemFont(ofSize: 17, weight: .medium)
            } else {
                self.orLabel.font = Fonts.type2
            }
            
            let height = NSLayoutConstraint(item: self.orLabel, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Sizes.button)
            NSLayoutConstraint.activate([height])
        }
        orLabelSet()
        
        let socialSignInStackView = {
            self.view.addSubview(self.socialSignInStackView)
            self.socialSignInStackView.translatesAutoresizingMaskIntoConstraints = false
            self.socialSignInStackView.addArrangedSubview(self.googleSignInStackView)
            self.socialSignInStackView.addArrangedSubview(self.facebookSignInStackView)
            
            if #available(iOS 13, *) {
                self.socialSignInStackView.addArrangedSubview(self.appleSignInStackView)
            }
           
            self.socialSignInStackView.axis = .horizontal
            self.socialSignInStackView.distribution = .fillEqually
            self.socialSignInStackView.spacing = Sizes.button2
            
            let top = NSLayoutConstraint(item: self.socialSignInStackView, attribute: .top, relatedBy: .equal, toItem: self.socialSignInView, attribute: .top, multiplier: 1, constant: 0)
            let bottom = NSLayoutConstraint(item: self.socialSignInStackView, attribute: .bottom, relatedBy: .equal, toItem: self.socialSignInStackView, attribute: .bottom, multiplier: 1, constant: 0)
            let centerX = NSLayoutConstraint(item: self.socialSignInStackView, attribute: .centerX, relatedBy: .equal, toItem: self.socialSignInView, attribute: .centerX, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([top, bottom, centerX])
        }
        socialSignInStackView()
        
        let googleSignInStackViewSet = {
            self.googleSignInStackView.addArrangedSubview(self.googleSignInButton)
            
            if Sizes.isiPad() {
                self.googleSignInStackView.spacing = Sizes.margin1
            } else {
                self.googleSignInStackView.spacing = Sizes.margin1*0.25
            }
            
            self.googleSignInStackView.axis = .vertical
            self.googleSignInStackView.distribution = .fill
        }
        googleSignInStackViewSet()
        
        let facebookSignInStackViewSet = {
            self.facebookSignInStackView.addArrangedSubview(self.facebookSignInButton)
            
            if Sizes.isiPad() {
                self.facebookSignInStackView.spacing = Sizes.margin1
            } else {
                self.facebookSignInStackView.spacing = Sizes.margin1*0.25
            }
            
            self.facebookSignInStackView.axis = .vertical
            self.facebookSignInStackView.distribution = .fill
        }
        facebookSignInStackViewSet()
        
        let googleSignInButtonSet = {
            self.googleSignInButton.translatesAutoresizingMaskIntoConstraints = false
            self.googleSignInButton.addTarget(self, action: #selector(self.pressGoogleSignInButton(sedner:)), for: .touchUpInside)
            self.googleSignInButton.setImage(UIImage(named: "google.png"), for: .normal)
            self.googleSignInButton.tintColor = Colors.shared.google
            self.googleSignInButton.isUserInteractionEnabled = true
            
            let width: NSLayoutConstraint
            let height: NSLayoutConstraint
            
            if Sizes.isiPad() {
                width = NSLayoutConstraint(item: self.googleSignInButton, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Sizes.button*2)
                height = NSLayoutConstraint(item: self.googleSignInButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Sizes.button*2)
            } else {
                width = NSLayoutConstraint(item: self.googleSignInButton, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Sizes.button)
                height = NSLayoutConstraint(item: self.googleSignInButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Sizes.button)
            }
            NSLayoutConstraint.activate([width, height])
        }
        googleSignInButtonSet()
        
        let facebookSignInButtonSet = {
            self.facebookSignInButton.translatesAutoresizingMaskIntoConstraints = false
            self.facebookSignInButton.addTarget(self, action: #selector(self.pressFacebookSignInButton(sedner:)), for: .touchUpInside)
            self.facebookSignInButton.setImage(UIImage(named: "facebook.png"), for: .normal)
            self.facebookSignInButton.tintColor = Colors.shared.facebook
            
            let width: NSLayoutConstraint
            let height: NSLayoutConstraint
            
            if Sizes.isiPad() {
                width = NSLayoutConstraint(item: self.facebookSignInButton, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Sizes.button*2)
                height = NSLayoutConstraint(item: self.facebookSignInButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Sizes.button*2)
            } else {
                width = NSLayoutConstraint(item: self.facebookSignInButton, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Sizes.button)
                height = NSLayoutConstraint(item: self.facebookSignInButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Sizes.button)
            }
            NSLayoutConstraint.activate([width, height])
        }
        facebookSignInButtonSet()
        
        if #available(iOS 13, *) {
            let appleSignInStackViewSet = {
                self.appleSignInStackView.addArrangedSubview(self.appleSignInButton)
                
                if Sizes.isiPad() {
                    self.appleSignInStackView.spacing = Sizes.margin1
                } else {
                    self.appleSignInStackView.spacing = Sizes.margin1*0.25
                }
                
                self.appleSignInStackView.axis = .vertical
                self.appleSignInStackView.distribution = .fill
            }
            appleSignInStackViewSet()
            
            let appleSignInButtonSet = {
                self.appleSignInButton.translatesAutoresizingMaskIntoConstraints = false
                self.appleSignInButton.addTarget(self, action: #selector(self.pressAppleSignInButton(sender:)), for: .touchUpInside)
                self.appleSignInButton.setImage(UIImage(named: "apple.png"), for: .normal)
                self.appleSignInButton.tintColor = Colors.shared.black
                let width: NSLayoutConstraint
                let height: NSLayoutConstraint
                
                if Sizes.isiPad() {
                    width = NSLayoutConstraint(item: self.appleSignInButton, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Sizes.button*2)
                    height = NSLayoutConstraint(item: self.appleSignInButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Sizes.button*2)
                } else {
                    width = NSLayoutConstraint(item: self.appleSignInButton,
                                               attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Sizes.button)
                    height = NSLayoutConstraint(item: self.appleSignInButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Sizes.button)
                }
                
                NSLayoutConstraint.activate([width, height])
            }
            appleSignInButtonSet()
        }
    }
}

@available(iOS 13.0, *)
extension SignInVC: ASAuthorizationControllerDelegate {
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
            guard let nonce = currentNonce else {
            fatalError("Invalid state: A login callback was received, but no login request was sent.")
        }
        
        guard let appleIDToken = appleIDCredential.identityToken else {
            print("Unable to fetch identity token")
            return
        }
            
        guard let idTokenString = String(data: appleIDToken, encoding: .utf8) else {
            print("Unable to serialize token string from data: \(appleIDToken.debugDescription)")
            return
        }
        
        // Initialize a Firebase credential.
        let credential = OAuthProvider.credential(withProviderID: "apple.com",
                                                idToken: idTokenString,
                                                rawNonce: nonce)
            
        self.loadingAnim.start()
            
        Auth.auth().signIn(with: credential) { (authResult, error) in
            if let error = error {
                self.loadingAnim.stop()
                
                let alert = UIAlertController(title: error.localizedDescription, message: nil, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            } else {
                DispatchQueue.global().async(execute: {
                    FirebaseManager.shared.updateUser(result: {
                        error in
                        self.loadingAnim.stop()
                            
                            if let error = error {
                                let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                                alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                                DispatchQueue.main.async(execute: {
                                    self.present(alert, animated: true, completion: {
                                        FirebaseManager.shared.signOut(result: {
                                        error in
                                            if let error = error {
                                                let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                                                alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                                                DispatchQueue.main.async(execute: {
                                                    self.present(alert, animated: true, completion: nil)
                                                })
                                            }
                                        })
                                    })
                                })
                            } else {
                                DispatchQueue.main.async(execute: {
                                    self.dismiss(animated: true, completion: {
                                        self.delegate?.signInVcSignedIn()
                                    })
                                })
                            }
                        })
                    })
                }
            }
        }
    }

    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        let alert = UIAlertController(title: error.localizedDescription, message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
}

extension SignInVC : ASAuthorizationControllerPresentationContextProviding {
    @available(iOS 13.0, *)
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
}
