//
//  SignUpVC.swift
//  M.A.
//
//  Created by Min-Su Kim on 2020/07/24.
//  Copyright © 2020 Min-Su Kim. All rights reserved.
//

import Foundation
import UIKit
import Firebase

class SignUpVC: UIViewController, TopViewBackButtonDelegate, UITextFieldDelegate, TopViewRightTextButtonDelegate {
    private var loadingAnim: LoadingAnim!
    private let topView = TopView()
    private let fieldStackView = UIStackView()
    private let emailContentView = UIView()
    private let passwordContentView = UIView()
    private let passwordCheckContentView = UIView()
    private let emailField = UITextField()
    private let passwordField = UITextField()
    private let passwordCheckField = UITextField()
    private let signUpButton = UIButton(type: .system)
    
    private var signupButtonBottomConstraint: NSLayoutConstraint!
    
    deinit {
        print("Sign Up VC Deinited.")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLayout()
        
        loadingAnim = LoadingAnim(view: view)
        loadingAnim.setColor(color: Colors.shared.buttonSub)
        emailField.becomeFirstResponder()
    }
    
    @objc private func pressEmailContentView() {
        emailField.becomeFirstResponder()
    }
    
    @objc private func pressPasswordContentView() {
        passwordField.becomeFirstResponder()
    }
    
    @objc private func pressPasswordCheckContentView() {
        passwordCheckField.becomeFirstResponder()
    }
    
    func pressBackButton() {
        dismiss(animated: true, completion: nil)
    }
    
    func pressRightTextButton() {
        let email = emailField.text ?? ""
        let password = passwordField.text ?? ""
        let password2 = passwordCheckField.text ?? ""
        
        if email.count == 0 {
            emailField.becomeFirstResponder()
            return
        }
        
        if password.count == 0 {
            passwordField.becomeFirstResponder()
            return
        }
        
        if password2.count == 0 {
            passwordCheckField.becomeFirstResponder()
            return
        }
        
        if password != password2 {
            let alert = UIAlertController(title: NSLocalizedString("SignupDoNotMatchPassword", comment: ""), message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
            return
        }
        
        self.loadingAnim.start()
        
        FirebaseManager.shared.signUpWithEmail(email: email, password: password, result: {
            error, message in
            self.loadingAnim.stop()
            
            if let error = error {
                let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            
            if let message = message {
                let alert = UIAlertController(title: message, message: nil, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: {
                    (_) in
                    self.dismiss(animated: true, completion: nil)
                }))
                self.present(alert, animated: true, completion: nil)
            }
        })
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.tag == 1 {
            passwordField.becomeFirstResponder()
        } else if textField.tag == 2 {
            passwordCheckField.becomeFirstResponder()
        } else if textField.tag == 3 {
            pressRightTextButton()
        }
        
        return true
    }
    
    private func setLayout() {
        view.backgroundColor = Colors.shared.background
        
        topView.show(toView: view)
        topView.settitle(title: NSLocalizedString("Signup", comment: ""), vc: self)
        topView.showBackButton()
        topView.backButtonDelegate = self
        topView.showRightTextButton(title: NSLocalizedString("Done", comment: ""))
        topView.rightTextButtonDelegate = self
        
        let fieldStackViewSet = {
            self.view.addSubview(self.fieldStackView)
            self.fieldStackView.translatesAutoresizingMaskIntoConstraints = false
            self.fieldStackView.addArrangedSubview(self.emailContentView)
            self.fieldStackView.addArrangedSubview(self.passwordContentView)
            self.fieldStackView.addArrangedSubview(self.passwordCheckContentView)
            self.fieldStackView.axis = .vertical
            self.fieldStackView.distribution = .fill
            self.fieldStackView.spacing = Sizes.margin1*0.5
            
            let top = NSLayoutConstraint(item: self.fieldStackView, attribute: .top, relatedBy: .equal, toItem: self.topView, attribute: .bottom, multiplier: 1, constant: Sizes.marginTop)
            let leading = NSLayoutConstraint(item: self.fieldStackView, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: Sizes.margin1)
            let trailing = NSLayoutConstraint(item: self.fieldStackView, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: -Sizes.margin1)
            NSLayoutConstraint.activate([top, leading, trailing])
        }
        fieldStackViewSet()
        
        let emailContentViewSet = {
            self.emailContentView.translatesAutoresizingMaskIntoConstraints = false
            self.emailContentView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.pressEmailContentView)))
            self.emailContentView.layer.cornerRadius = Sizes.radius
            ViewManager.appendLine(lineType: .bottom, view: self.emailContentView)
            NSLayoutConstraint.activate([NSLayoutConstraint(item: self.emailContentView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Sizes.button)])
        }
        emailContentViewSet()
        
        let passwordContentViewSet = {
            self.passwordContentView.translatesAutoresizingMaskIntoConstraints = false
            self.passwordContentView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.pressPasswordContentView)))
            self.passwordContentView.layer.cornerRadius = Sizes.radius
            ViewManager.appendLine(lineType: .bottom, view: self.passwordContentView)
            NSLayoutConstraint.activate([NSLayoutConstraint(item: self.passwordContentView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Sizes.button)])
        }
        passwordContentViewSet()
        
        let passwordCheckContentViewSet = {
            self.passwordCheckContentView.translatesAutoresizingMaskIntoConstraints = false
            self.passwordCheckContentView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.pressPasswordCheckContentView)))
            self.passwordCheckContentView.layer.cornerRadius = Sizes.radius
            ViewManager.appendLine(lineType: .bottom, view: self.passwordCheckContentView)
            NSLayoutConstraint.activate([NSLayoutConstraint(item: self.passwordCheckContentView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Sizes.button)])
        }
        passwordCheckContentViewSet()
        
        let emailFieldSet = {
            self.emailContentView.addSubview(self.emailField)
            self.emailField.translatesAutoresizingMaskIntoConstraints = false
            self.emailField.delegate = self
            self.emailField.tag = 1
            self.emailField.placeholder = NSLocalizedString("FieldTitleEmail", comment: "")
            self.emailField.textColor = Colors.shared.black
            self.emailField.tintColor = Colors.shared.point
            self.emailField.returnKeyType = .next
            self.emailField.keyboardType = .emailAddress
            self.emailField.clearButtonMode = .whileEditing
            
            if Sizes.isiPad() {
                self.emailField.font = UIFont.systemFont(ofSize: 20, weight: .medium)
            } else {
                self.emailField.font = Fonts.type1
            }
            
            let centerY = NSLayoutConstraint(item: self.emailField, attribute: .centerY, relatedBy: .equal, toItem: self.emailContentView, attribute: .centerY, multiplier: 1, constant: 0)
            let leading = NSLayoutConstraint(item: self.emailField, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: Sizes.margin3)
            let trailing = NSLayoutConstraint(item: self.emailField, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: -Sizes.margin3)
            NSLayoutConstraint.activate([centerY, leading, trailing])
        }
        emailFieldSet()
        
        let passwordFieldSet = {
            self.passwordContentView.addSubview(self.passwordField)
            self.passwordField.translatesAutoresizingMaskIntoConstraints = false
            self.passwordField.delegate = self
            self.passwordField.tag = 2
            self.passwordField.placeholder = NSLocalizedString("Password", comment: "")
            self.passwordField.textColor = Colors.shared.black
            self.passwordField.tintColor = Colors.shared.point
            self.passwordField.isSecureTextEntry = true
            self.passwordField.returnKeyType = .next
            self.passwordField.clearButtonMode = .whileEditing
            
            if Sizes.isiPad() {
                self.passwordField.font = UIFont.systemFont(ofSize: 20, weight: .medium)
            } else {
                self.passwordField.font = Fonts.type1
            }
            
            let centerY = NSLayoutConstraint(item: self.passwordField, attribute: .centerY, relatedBy: .equal, toItem: self.passwordContentView, attribute: .centerY, multiplier: 1, constant: 0)
            let leading = NSLayoutConstraint(item: self.passwordField, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: Sizes.margin3)
            let trailing = NSLayoutConstraint(item: self.passwordField, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: -Sizes.margin3)
            NSLayoutConstraint.activate([centerY, leading, trailing])
        }
        passwordFieldSet()
        
        let passwordCheckFieldSet = {
            self.passwordCheckContentView.addSubview(self.passwordCheckField)
            self.passwordCheckField.translatesAutoresizingMaskIntoConstraints = false
            self.passwordCheckField.delegate = self
            self.passwordCheckField.tag = 3
            self.passwordCheckField.placeholder = NSLocalizedString("FieldTItleRetypePassword", comment: "")
            self.passwordCheckField.textColor = Colors.shared.black
            self.passwordCheckField.tintColor = Colors.shared.point
            self.passwordCheckField.isSecureTextEntry = true
            self.passwordCheckField.returnKeyType = .done
            self.passwordCheckField.clearButtonMode = .whileEditing
            
            if Sizes.isiPad() {
                self.passwordCheckField.font = UIFont.systemFont(ofSize: 20, weight: .medium)
            } else {
                self.passwordCheckField.font = Fonts.type1
            }
            
            let centerY = NSLayoutConstraint(item: self.passwordCheckField, attribute: .centerY, relatedBy: .equal, toItem: self.passwordCheckContentView, attribute: .centerY, multiplier: 1, constant: 0)
            let leading = NSLayoutConstraint(item: self.passwordCheckField, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: Sizes.margin3)
            let trailing = NSLayoutConstraint(item: self.passwordCheckField, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: -Sizes.margin3)
            NSLayoutConstraint.activate([centerY, leading, trailing])
        }
        passwordCheckFieldSet()
    }
}
