//
//  MenuButton.swift
//  M.A.
//
//  Created by Min-Su Kim on 2020/07/25.
//  Copyright © 2020 Min-Su Kim. All rights reserved.
//

import Foundation
import UIKit

class MenuButton: UIButton {
    private let arrowImageView = UIImageView()
    private let titleStackView = UIStackView()
    private let titleIconImageView = UIImageView()
    private let titleContentLabel = UILabel()
    
    private var title = ""
    private var id = ""
    
    convenience init(id: String) {
        self.init()
        self.id = id
        setLayout()
    }
    
    func setTitle(title: String) {
        self.title = title
        titleContentLabel.text = title
    }
    
    func setTitleColor(color: UIColor) {
        titleContentLabel.textColor = color
    }
    
    func setIcon(icon: UIImage?, color: UIColor = Colors.shared.black) {
        if let icon = icon  {
            titleIconImageView.image = icon
            titleIconImageView.setImageColor(color: color)
            titleIconImageView.isHidden = false
        } else {
            titleIconImageView.isHidden = true
        }
    }
    
    func hideArrowImageView() {
        arrowImageView.isHidden = true
    }
    
    private func setLayout() {
        translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([NSLayoutConstraint(item: self, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Sizes.button)])
        ViewManager.appendLine(lineType: .bottom, view: self, spacing: Sizes.margin1)
        
        let arrowImageViewSet = {
            self.addSubview(self.arrowImageView)
            self.arrowImageView.translatesAutoresizingMaskIntoConstraints = false
            self.arrowImageView.image = UIImage(named: "arrow_right.png")
            self.arrowImageView.setImageColor(color: Colors.shared.black)
            
            let width: NSLayoutConstraint!
            let height: NSLayoutConstraint!
            
            if Sizes.isiPad() {
                width = NSLayoutConstraint(item: self.arrowImageView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 30)
                height = NSLayoutConstraint(item: self.arrowImageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 30)
            } else {
                width = NSLayoutConstraint(item: self.arrowImageView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 20)
                height = NSLayoutConstraint(item: self.arrowImageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 20)
            }
            
            let trailing = NSLayoutConstraint(item: self.arrowImageView, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: -Sizes.margin3)
            let centerY = NSLayoutConstraint(item: self.arrowImageView, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([width, height, trailing, centerY])
        }
        arrowImageViewSet()
        
        let titleStackViewSet = {
            self.addSubview(self.titleStackView)
            self.titleStackView.translatesAutoresizingMaskIntoConstraints = false
            self.titleStackView.addArrangedSubview(self.titleIconImageView)
            self.titleStackView.addArrangedSubview(self.titleContentLabel)
            self.titleStackView.axis = .horizontal
            self.titleStackView.distribution = .fill
            self.titleStackView.spacing = Sizes.margin1*0.75
            self.titleStackView.isUserInteractionEnabled = false
            
            let height = NSLayoutConstraint(item: self.titleStackView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 20)
            let centerY = NSLayoutConstraint(item: self.titleStackView, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0)
            let leading = NSLayoutConstraint(item: self.titleStackView, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: Sizes.margin3)
            let trailing = NSLayoutConstraint(item: self.titleStackView, attribute: .trailing, relatedBy: .equal, toItem: self.arrowImageView, attribute: .leading, multiplier: 1, constant: -Sizes.margin1)
            NSLayoutConstraint.activate([height, centerY, leading, trailing])
        }
        titleStackViewSet()
        
        let titleIconImageViewSet = {
            self.titleIconImageView.translatesAutoresizingMaskIntoConstraints = false
            self.titleIconImageView.isHidden = true
            
            let width = NSLayoutConstraint(item: self.titleIconImageView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 20)
            let height = NSLayoutConstraint(item: self.titleIconImageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 20)
            NSLayoutConstraint.activate([width, height])
        }
        titleIconImageViewSet()
        
        let titleContentLabelSet = {
            self.titleContentLabel.textColor = Colors.shared.black
            
            if Sizes.isiPad() {
                self.titleContentLabel.font = UIFont.systemFont(ofSize: 20, weight: .medium)
            } else {
                self.titleContentLabel.font = Fonts.type1
            }
            
        }
        titleContentLabelSet()
    }
}
