//
//  OnelineEditVC.swift
//  Soksok
//
//  Created by Min-Su Kim on 4/15/20.
//  Copyright © 2020 김민수. All rights reserved.
//

import Foundation
import UIKit

protocol OnelineEditVCDelegate: class {
    func onelineEditDidDone(_ oneLineEditVC: OnelineEditVC, value: String, tag: Int, index: Int)
}

class OnelineEditVC: UIViewController, UITextFieldDelegate, TopViewBackButtonDelegate, TopViewRightTextButtonDelegate {
    private var topvi = TopView()
    private var enterbackvi = UIView()
    private var enterfi = UITextField()
    private var enterfiline = UIView()
    private var countlb = UILabel()
    
    weak var delegate: OnelineEditVCDelegate?
    var tag = 0
    var index = 0
    var mintext = 1
    var maxtext = 20
    var ispassword = false
    var isbecome = false
    var isnumberonly = false
    var isdismissanimation = true
    var donetext: String?
    
    deinit {
        print("oneline Edit View Controller Deinited.")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 13, *) {
            topvi.showOnSubView(toView: view)
        } else {
            topvi.show(toView: view)
        }
        
        
        setlayout()
        updatecount()
        
        if ispassword {
            enterfi.isSecureTextEntry = true
        }
        
        if isnumberonly {
            enterfi.keyboardType = .numberPad
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if isbecome {
            enterfi.becomeFirstResponder()
        }
    }
    
    @objc func textfielddidchange(_ sender: UITextField) {
        updatecount()
    }
    
    func setFieldText(text: String) {
        enterfi.text = text
    }
    
    func pressBackButton() {
        dismiss()
    }
    
    func pressRightTextButton() {
        let fieldcount = enterfi.text!.count
        if fieldcount > maxtext {
            return
        } else if fieldcount < mintext {
            return
        } else {
            dismiss()
            delegate?.onelineEditDidDone(self, value: enterfi.text ?? "", tag: tag, index: index)
        }
    }
    
    func getPlaceHolder() -> String {
        return enterfi.placeholder ?? ""
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        pressRightTextButton()
        return true
    }
    
    func settitle(title: String) {
        topvi.settitle(title: title, vc: self)
    }
    
    func setplaceholder(title: String) {
        enterfi.placeholder = title
    }

    private func updatecount() {
        let fieldcount = enterfi.text!.count
        countlb.text = "\(maxtext - fieldcount)"
    }
    
    private func setlayout() {
        view.backgroundColor = Colors.shared.backgroundSub
        topvi.showBackButton()
        topvi.backButtonDelegate = self
        topvi.rightTextButtonDelegate = self
        
        if let text = donetext {
            topvi.showRightTextButton(title: text)
        } else {
            topvi.showRightTextButton(title: NSLocalizedString("Done", comment: ""))
        }
        
        let enterbackviset = {
            self.view.addSubview(self.enterbackvi)
            self.enterbackvi.translatesAutoresizingMaskIntoConstraints = false
           
            let height = NSLayoutConstraint(item: self.enterbackvi, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Sizes.button2)
            let top = NSLayoutConstraint(item: self.enterbackvi, attribute: .top, relatedBy: .equal, toItem: self.topvi, attribute: .bottom, multiplier: 1, constant: Sizes.marginTop)
            let leading = NSLayoutConstraint(item: self.enterbackvi, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: Sizes.margin3)
            let trailing = NSLayoutConstraint(item: self.enterbackvi, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: -Sizes.margin3)
            NSLayoutConstraint.activate([height, top, leading, trailing])
        }
        enterbackviset()
        
        let countlbset = {
            self.enterbackvi.addSubview(self.countlb)
            self.countlb.translatesAutoresizingMaskIntoConstraints = false
            self.countlb.textColor = Colors.shared.textSub
            
            if Sizes.isiPad() {
                self.countlb.font = UIFont.systemFont(ofSize: 17, weight: .medium)
            } else {
                self.countlb.font = Fonts.type2
            }
            
            let centerY = NSLayoutConstraint(item: self.countlb, attribute: .centerY, relatedBy: .equal, toItem: self.enterbackvi, attribute: .centerY, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.countlb, attribute: .trailing, relatedBy: .equal, toItem: self.enterbackvi, attribute: .trailing, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([centerY, trailing])
        }
        countlbset()
        
        let enterfieldset = {
            self.enterbackvi.addSubview(self.enterfi)
            self.enterfi.translatesAutoresizingMaskIntoConstraints = false
            self.enterfi.addTarget(self, action: #selector(self.textfielddidchange(_:)), for: .editingChanged)
            self.enterfi.delegate = self
            self.enterfi.textColor = Colors.shared.black
            self.enterfi.tintColor = Colors.shared.point
            self.enterfi.returnKeyType = .done
        
            if Sizes.isiPad() {
                self.enterfi.font = UIFont.systemFont(ofSize: 20, weight: .medium)
            } else {
                self.enterfi.font = Fonts.type1
            }
            
            let centerY = NSLayoutConstraint(item: self.enterfi, attribute: .centerY, relatedBy: .equal, toItem: self.enterbackvi, attribute: .centerY, multiplier: 1, constant: 0)
            let leading = NSLayoutConstraint(item: self.enterfi, attribute: .leading, relatedBy: .equal, toItem: self.enterbackvi, attribute: .leading, multiplier: 1, constant: 24.5)
            let trailing = NSLayoutConstraint(item: self.enterfi, attribute: .trailing, relatedBy: .equal, toItem: self.enterbackvi, attribute: .trailing, multiplier: 1, constant: -24.5)
            NSLayoutConstraint.activate([centerY, leading, trailing])
        }
        enterfieldset()
        
        let enterfieldlineset = {
            self.view.addSubview(self.enterfiline)
            self.enterfiline.translatesAutoresizingMaskIntoConstraints = false
            self.enterfiline.backgroundColor = Colors.shared.line
            
            let height = NSLayoutConstraint(item: self.enterfiline, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Sizes.lineHeight)
            let leading = NSLayoutConstraint(item: self.enterfiline, attribute: .leading, relatedBy: .equal, toItem: self.enterbackvi, attribute: .leading, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.enterfiline, attribute: .trailing, relatedBy: .equal, toItem: self.enterbackvi, attribute: .trailing, multiplier: 1, constant: 0)
            let bottom = NSLayoutConstraint(item: self.enterfiline, attribute: .bottom, relatedBy: .equal, toItem: self.enterbackvi, attribute: .bottom, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([height, leading, trailing, bottom])
        }
        enterfieldlineset()
    }
    
    private func dismiss() {
        dismiss(animated: isdismissanimation, completion: nil)
    }
}
