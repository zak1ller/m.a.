//
//  TabBarController.swift
//  M.A.
//
//  Created by Min-Su Kim on 2020/08/04.
//  Copyright © 2020 Min-Su Kim. All rights reserved.
//

import Foundation
import UIKit

class TabBarController: UITabBarController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        tabBar.tintColor = Colors.shared.point
        tabBar.unselectedItemTintColor = Colors.shared.textSub
        tabBar.barTintColor = Colors.shared.backgroundSub
        
        // 네비게이션 아이템 이미지 중앙 정렬
        if !Sizes.isXseries() {
            if let items = tabBar.items {
                for item in items {
                    item.imageInsets.top = -6
                }
            }
        } else {
            if let items = tabBar.items {
                for item in items {
                    item.imageInsets.top = 0
                }
            }
        }
    }
}
