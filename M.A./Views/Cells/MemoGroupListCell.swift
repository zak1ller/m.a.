//
//  MemoGroupListCell.swift
//  M.A.
//
//  Created by Min-Su Kim on 2020/09/26.
//  Copyright © 2020 Min-Su Kim. All rights reserved.
//

import Foundation
import UIKit

protocol MemoGroupListCellLongPressDelegate: class {
    func memoGroupListCellLongPressed(index: Int)
}

class MemoGroupListCell: UITableViewCell {
    private let backView = UIView()
    private let arrowRightImageView = UIImageView()
    private let countLabel = UILabel()
    private let titleStackView = UIStackView()
    private let titleLabel = UILabel()
    
    weak var delegate: MemoGroupListCellLongPressDelegate?
    
    required init?(coder aDecoder: NSCoder) {super.init(coder: aDecoder)}
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        contentView.backgroundColor = Colors.shared.backgroundSub
        
        let backViewSet = {
            self.contentView.addSubview(self.backView)
            self.backView.translatesAutoresizingMaskIntoConstraints = false
            self.backView.addGestureRecognizer(UILongPressGestureRecognizer(target: self , action: #selector(self.longPress)))
            
            let top = NSLayoutConstraint(item: self.backView, attribute: .top, relatedBy: .equal, toItem: self.contentView, attribute: .top, multiplier: 1, constant: 0)
            let bottom = NSLayoutConstraint(item: self.backView, attribute: .bottom, relatedBy: .equal, toItem: self.contentView, attribute: .bottom, multiplier: 1, constant: 0)
            let leading = NSLayoutConstraint(item: self.backView, attribute: .leading, relatedBy: .equal, toItem: self.contentView, attribute: .leading, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.backView, attribute: .trailing, relatedBy: .equal, toItem: self.contentView, attribute: .trailing, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([top, bottom, leading, trailing])
        }
        backViewSet()
        
        let arrowRightImageViewSet = {
            self.backView.addSubview(self.arrowRightImageView)
            self.arrowRightImageView.translatesAutoresizingMaskIntoConstraints = false
            self.arrowRightImageView.image = UIImage(named: "arrow_right.png")
            self.arrowRightImageView.setImageColor(color: Colors.shared.textSub)
            
            let width: NSLayoutConstraint
            let height: NSLayoutConstraint
            
            if Sizes.isiPad() {
                width = NSLayoutConstraint(item: self.arrowRightImageView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 25)
                height = NSLayoutConstraint(item: self.arrowRightImageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 25)
            } else {
                width = NSLayoutConstraint(item: self.arrowRightImageView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 15)
                height = NSLayoutConstraint(item: self.arrowRightImageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 15)
            }
            
            let trailing = NSLayoutConstraint(item: self.arrowRightImageView, attribute: .trailing, relatedBy: .equal, toItem: self.contentView, attribute: .trailing, multiplier: 1, constant: -Sizes.margin1)
            let centerY = NSLayoutConstraint(item: self.arrowRightImageView, attribute: .centerY, relatedBy: .equal, toItem: self.backView, attribute: .centerY, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([width, height, trailing, centerY])
        }
        arrowRightImageViewSet()
        
        let countLabelSet = {
            self.backView.addSubview(self.countLabel)
            self.countLabel.translatesAutoresizingMaskIntoConstraints = false
            self.countLabel.textColor = Colors.shared.textSub
            
            if Sizes.isiPad() {
                self.countLabel.font = UIFont.systemFont(ofSize: 20, weight: .medium)
            } else {
                self.countLabel.font = Fonts.type1
            }
            
            let trailing = NSLayoutConstraint(item: self.countLabel, attribute: .trailing, relatedBy: .equal, toItem: self.arrowRightImageView, attribute: .leading, multiplier: 1, constant: -6)
            let centerY = NSLayoutConstraint(item: self.countLabel, attribute: .centerY, relatedBy: .equal, toItem: self.arrowRightImageView, attribute: .centerY, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([trailing, centerY])
        }
        countLabelSet()
        
        let titleStackViewSet = {
            self.backView.addSubview(self.titleStackView)
            self.titleStackView.translatesAutoresizingMaskIntoConstraints = false
            self.titleStackView.addArrangedSubview(self.titleLabel)
            self.titleStackView.axis = .vertical
            self.titleStackView.distribution = .fill
            self.titleStackView.spacing = 0
            
            let top = NSLayoutConstraint(item: self.titleStackView, attribute: .top, relatedBy: .equal, toItem: self.backView, attribute: .top, multiplier: 1, constant: Sizes.margin1)
            let bottom = NSLayoutConstraint(item: self.titleStackView, attribute: .bottom, relatedBy: .equal, toItem: self.backView, attribute: .bottom, multiplier: 1, constant: -Sizes.margin1)
            let leading = NSLayoutConstraint(item: self.titleStackView, attribute: .leading, relatedBy: .equal, toItem: self.contentView, attribute: .leading, multiplier: 1, constant: Sizes.margin3)
            let trailing = NSLayoutConstraint(item: self.titleStackView, attribute: .trailing, relatedBy: .lessThanOrEqual, toItem: self.countLabel, attribute: .leading, multiplier: 1, constant: -8)
            NSLayoutConstraint.activate([top, bottom, leading, trailing])
        }
        titleStackViewSet()
        
        let titleLabelSet = {
            self.titleLabel.textColor = Colors.shared.black
            
            if Sizes.isiPad() {
                self.titleLabel.font = UIFont.systemFont(ofSize: 20, weight: .medium)
            } else {
                self.titleLabel.font = Fonts.type1
            }
            
            self.titleLabel.numberOfLines = 0
        }
        titleLabelSet()
    }
    
    @objc private func longPress() {
        EffectManager.vibrate()
        delegate?.memoGroupListCellLongPressed(index: tag)
    }
    
    func setData(data: MemosManager.MemoGroup) {
        titleLabel.text = data.title
        countLabel.text = "\(data.count)"
    }
}
