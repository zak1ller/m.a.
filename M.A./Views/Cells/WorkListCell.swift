//
//  workListCell.swift
//  unspecified
//
//  Created by Min-Su Kim on 2020/07/14.
//  Copyright © 2020 Min-Su Kim. All rights reserved.
//

import Foundation
import UIKit

protocol WorkListCellLongTouchDelegate: class {
    func workListCellLongTouch(cell: WorkListCell, index: Int)
}

class WorkListCell: UITableViewCell {
    var backView = UIView()
    var workStatusImageView = UIImageView()
    var titleLabel = UILabel()
    var titleCenterYLine = UIView()
    
    weak var longTouchDelegate: WorkListCellLongTouchDelegate?
    
    required init?(coder aDecoder: NSCoder) {super.init(coder: aDecoder)}
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        contentView.backgroundColor = Colors.shared.backgroundSub
        
        let backViewSet = {
            self.contentView.addSubview(self.backView)
            self.backView.translatesAutoresizingMaskIntoConstraints = false
            self.backView.addGestureRecognizer(UILongPressGestureRecognizer(target: self, action: #selector(self.backViewLongTouch)))
            self.backView.layer.cornerRadius = Sizes.radius
            self.backView.backgroundColor = Colors.shared.backgroundSub
            
            let top = NSLayoutConstraint(item: self.backView, attribute: .top, relatedBy: .equal, toItem: self.contentView, attribute: .top, multiplier: 1, constant: 0)
            let bottom = NSLayoutConstraint(item: self.backView, attribute: .bottom, relatedBy: .equal, toItem: self.contentView, attribute: .bottom, multiplier: 1, constant: -Sizes.margin1*0.5)
            let leading = NSLayoutConstraint(item: self.backView, attribute: .leading, relatedBy: .equal, toItem: self.contentView, attribute: .leading, multiplier: 1, constant: Sizes.margin1)
            let trailing = NSLayoutConstraint(item: self.backView, attribute: .trailing, relatedBy: .equal, toItem: self.contentView, attribute: .trailing, multiplier: 1, constant: -Sizes.margin1)
            NSLayoutConstraint.activate([top, bottom, leading, trailing])
        }
        backViewSet()
        
        let workStatusImageViewSet = {
            self.backView.addSubview(self.workStatusImageView)
            self.workStatusImageView.translatesAutoresizingMaskIntoConstraints = false
            self.workStatusImageView.layer.cornerRadius = 17*0.25
            
            let width: NSLayoutConstraint
            let height: NSLayoutConstraint
            
            if Sizes.isiPad() {
                width = NSLayoutConstraint(item: self.workStatusImageView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 25)
                height = NSLayoutConstraint(item: self.workStatusImageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 25)
            } else {
                width = NSLayoutConstraint(item: self.workStatusImageView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 17)
                height = NSLayoutConstraint(item: self.workStatusImageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 17)
            }
            
            let top = NSLayoutConstraint(item: self.workStatusImageView, attribute: .top, relatedBy: .equal, toItem: self.backView, attribute: .top, multiplier: 1, constant: Sizes.margin1)
            let bottom = NSLayoutConstraint(item: self.workStatusImageView, attribute: .bottom, relatedBy: .equal, toItem: self.backView, attribute: .bottom, multiplier: 1, constant: -Sizes.margin1)
            let leading = NSLayoutConstraint(item: self.workStatusImageView, attribute: .leading, relatedBy: .equal, toItem: self.contentView, attribute: .leading, multiplier: 1, constant: Sizes.margin3)
            NSLayoutConstraint.activate([width, height, top, bottom, leading])
        }
        workStatusImageViewSet()
        
        let titleLabelSet = {
            self.backView.addSubview(self.titleLabel)
            self.titleLabel.translatesAutoresizingMaskIntoConstraints = false
            
            if Sizes.isiPad() {
                self.titleLabel.font = UIFont.systemFont(ofSize: 20, weight: .medium)
            } else {
                self.titleLabel.font = UIFont.systemFont(ofSize: 15, weight: .medium)
            }
            
            self.titleLabel.numberOfLines = 2
            self.titleLabel.adjustsFontSizeToFitWidth = true
            self.titleLabel.minimumScaleFactor = 0.5
            
            let top = NSLayoutConstraint(item: self.titleLabel, attribute: .top, relatedBy: .equal, toItem: self.workStatusImageView, attribute: .top, multiplier: 1, constant: 0)
            let leading = NSLayoutConstraint(item: self.titleLabel, attribute: .leading, relatedBy: .equal, toItem: self.workStatusImageView, attribute: .trailing, multiplier: 1, constant: Sizes.margin1)
            let trailing = NSLayoutConstraint(item: self.titleLabel, attribute: .trailing, relatedBy: .equal, toItem: self.contentView, attribute: .trailing, multiplier: 1, constant: -Sizes.margin3)
            NSLayoutConstraint.activate([top, leading, trailing])
        }
        titleLabelSet()
        
        let titleCenteYLineSet = {
            self.titleLabel.addSubview(self.titleCenterYLine)
            self.titleCenterYLine.translatesAutoresizingMaskIntoConstraints = false
            self.titleCenterYLine.isHidden = true
            self.titleCenterYLine.backgroundColor = Colors.shared.buttonSub
            
            let height = NSLayoutConstraint(item: self.titleCenterYLine, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 0.75)
            let centerY = NSLayoutConstraint(item: self.titleCenterYLine, attribute: .centerY, relatedBy: .equal, toItem: self.titleLabel, attribute: .centerY, multiplier: 1, constant: 0)
            let leading = NSLayoutConstraint(item: self.titleCenterYLine, attribute: .leading, relatedBy: .equal, toItem: self.titleLabel, attribute: .leading, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.titleCenterYLine, attribute: .trailing, relatedBy: .equal, toItem: self.titleLabel, attribute: .trailing, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([height, centerY, leading, trailing])
        }
        titleCenteYLineSet()
    }
    
    @objc private func backViewLongTouch() {
        EffectManager.vibrate()
        longTouchDelegate?.workListCellLongTouch(cell: self, index: tag)
    }
    
    func setTask(task: WorkManager.Work) {
        titleLabel.text = task.title
        
        switch task.status {
        case .ready:
            titleCenterYLine.isHidden = true
            workStatusImageView.backgroundColor = Colors.shared.buttonSub
            titleLabel.textColor = Colors.shared.black
        case .success:
            titleCenterYLine.isHidden = false
            workStatusImageView.backgroundColor = Colors.shared.point
            titleLabel.textColor = Colors.shared.buttonSub
        }
    }
}
