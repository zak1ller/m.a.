//
//  SimpleSelectCell.swift
//  M.A.
//
//  Created by Min-Su Kim on 2020/09/22.
//  Copyright © 2020 Min-Su Kim. All rights reserved.
//

import Foundation
import UIKit

struct SimpleSelectItem {
    var title = ""
    var isCheck = false
    var key = ""
    
    init(title: String, isCheck: Bool, key: String) {
        self.title = title
        self.isCheck = isCheck
        self.key = key
    }
}

class SimpleSelectCell: UITableViewCell {
    private let backView = UIView()
    private let checkImageView = UIImageView()
    private let titleLabel = UILabel()
    
    required init?(coder aDecoder: NSCoder) {super.init(coder: aDecoder)}
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        contentView.backgroundColor = Colors.shared.backgroundSub
        
        let backViewSet = {
            self.contentView.addSubview(self.backView)
            self.backView.translatesAutoresizingMaskIntoConstraints = false
            self.backView.backgroundColor = Colors.shared.backgroundSub
            
            let height = NSLayoutConstraint(item: self.backView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 45)
            let top = NSLayoutConstraint(item: self.backView, attribute: .top, relatedBy: .equal, toItem: self.contentView, attribute: .top, multiplier: 1, constant: 0)
            let bottom = NSLayoutConstraint(item: self.backView, attribute: .bottom, relatedBy: .equal, toItem: self.contentView, attribute: .bottom, multiplier: 1, constant: 0)
            let leading = NSLayoutConstraint(item: self.backView, attribute: .leading, relatedBy: .equal, toItem: self.contentView, attribute: .leading, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.backView, attribute: .trailing, relatedBy: .equal, toItem: self.contentView, attribute: .trailing, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([height, top, bottom, leading, trailing])
        }
        backViewSet()
        
        let checkImageViewSet = {
            self.backView.addSubview(self.checkImageView)
            self.checkImageView.translatesAutoresizingMaskIntoConstraints = false
            self.checkImageView.image = UIImage(named: "check.png")
            
            let width: NSLayoutConstraint
            let height: NSLayoutConstraint
            
            if Sizes.isiPad() {
                width = NSLayoutConstraint(item: self.checkImageView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 40)
                height = NSLayoutConstraint(item: self.checkImageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 40)
            } else {
                width = NSLayoutConstraint(item: self.checkImageView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 25)
                height = NSLayoutConstraint(item: self.checkImageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 25)
            }
            
            let trailing = NSLayoutConstraint(item: self.checkImageView, attribute: .trailing, relatedBy: .equal, toItem: self.backView, attribute: .trailing, multiplier: 1, constant: -Sizes.margin1)
            let centerY = NSLayoutConstraint(item: self.checkImageView, attribute: .centerY, relatedBy: .equal, toItem: self.backView, attribute: .centerY, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([width, height, trailing, centerY])
        }
        checkImageViewSet()
        
        let titleLabelSet = {
            self.backView.addSubview(self.titleLabel)
            self.titleLabel.translatesAutoresizingMaskIntoConstraints = false
            self.titleLabel.textColor = Colors.shared.black
            
            if Sizes.isiPad() {
                self.titleLabel.font = UIFont.systemFont(ofSize: 20, weight: .medium)
            } else {
                self.titleLabel.font = Fonts.type1
            }
            
            let centerY = NSLayoutConstraint(item: self.titleLabel, attribute: .centerY, relatedBy: .equal, toItem: self.backView, attribute: .centerY, multiplier: 1, constant: 0)
            let leading = NSLayoutConstraint(item: self.titleLabel, attribute: .leading, relatedBy: .equal, toItem: self.backView, attribute: .leading, multiplier: 1, constant: Sizes.margin3)
            NSLayoutConstraint.activate([centerY, leading])
        }
        titleLabelSet()
    }
    
    func setContents(item: SimpleSelectItem) {
        titleLabel.text = item.title
        
        if item.isCheck {
            checkImageView.setImageColor(color: Colors.shared.point)
        } else {
            checkImageView.setImageColor(color: UIColor.clear)
        }
    }
}
