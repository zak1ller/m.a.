//
//  StatisticsListCell.swift
//  unspecified
//
//  Created by Min-Su Kim on 2020/07/18.
//  Copyright © 2020 Min-Su Kim. All rights reserved.
//

import Foundation
import UIKit

protocol StatisticsListCellLongTouchDelegate: class {
    func statisticsListCellLongTouch(cell: StatisticsListCell, index: Int)
}

class StatisticsListCell: UITableViewCell {
    private let backView = UIView()
    var gradeValueLabel = UILabel()
    var taskNameLabel = UILabel()
    var taskDayLabel = UILabel()

    weak var longTouchDelegate: StatisticsListCellLongTouchDelegate?
    
    required init?(coder aDecoder: NSCoder) {super.init(coder: aDecoder)}
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        
        contentView.backgroundColor = Colors.shared.background
        contentView.addGestureRecognizer(UILongPressGestureRecognizer(target: self, action: #selector(backViewLongTouch)))
        
        let backViewSet = {
            self.contentView.addSubview(self.backView)
            self.backView.translatesAutoresizingMaskIntoConstraints = false
            self.backView.backgroundColor = Colors.shared.backgroundSub
            self.backView.layer.cornerRadius = Sizes.radius
            
            let top = NSLayoutConstraint(item: self.backView, attribute: .top, relatedBy: .equal, toItem: self.contentView, attribute: .top, multiplier: 1, constant: 0)
            let bottom = NSLayoutConstraint(item: self.backView, attribute: .bottom, relatedBy: .equal, toItem: self.contentView, attribute: .bottom, multiplier: 1, constant: -Sizes.margin1*0.5)
            let leading = NSLayoutConstraint(item: self.backView, attribute: .leading, relatedBy: .equal, toItem: self.contentView, attribute: .leading, multiplier: 1, constant: Sizes.margin1)
            let trailing = NSLayoutConstraint(item: self.backView, attribute: .trailing, relatedBy: .equal, toItem: self.contentView, attribute: .trailing, multiplier: 1, constant: -Sizes.margin1)
            NSLayoutConstraint.activate([top, bottom, leading, trailing])
        }
        backViewSet()
        
        let gradeValueLabelSet = {
            self.backView.addSubview(self.gradeValueLabel)
            self.gradeValueLabel.translatesAutoresizingMaskIntoConstraints = false
            self.gradeValueLabel.textAlignment = .center
            
            if Sizes.isiPad() {
                self.gradeValueLabel.font = UIFont.systemFont(ofSize: 18, weight: .bold)
            } else {
                self.gradeValueLabel.font = UIFont.systemFont(ofSize: 14, weight: .bold)
            }
            
            let width = NSLayoutConstraint(item: self.gradeValueLabel, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 50)
            let centerY = NSLayoutConstraint(item: self.gradeValueLabel, attribute: .centerY, relatedBy: .equal, toItem: self.backView, attribute: .centerY, multiplier: 1, constant: 0)
            let leading = NSLayoutConstraint(item: self.gradeValueLabel, attribute: .leading, relatedBy: .equal, toItem: self.contentView, attribute: .leading, multiplier: 1, constant: Sizes.margin1)
            NSLayoutConstraint.activate([width, centerY, leading])
        }
        gradeValueLabelSet()
        
        let taskDayLabelSet = {
            self.backView.addSubview(self.taskDayLabel)
            self.taskDayLabel.translatesAutoresizingMaskIntoConstraints = false
            
            if Sizes.isiPad() {
                self.taskDayLabel.font = UIFont.systemFont(ofSize: 17, weight: .medium)
            } else {
                self.taskDayLabel.font = UIFont.systemFont(ofSize: 12, weight: .medium)
            }
            
            self.taskDayLabel.textColor = Colors.shared.textSub
            
            let centerY = NSLayoutConstraint(item: self.taskDayLabel, attribute: .centerY, relatedBy: .equal, toItem: self.backView, attribute: .centerY, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.taskDayLabel, attribute: .trailing, relatedBy: .equal, toItem: self.contentView, attribute: .trailing, multiplier: 1, constant: -Sizes.margin3)
            NSLayoutConstraint.activate([centerY, trailing])
        }
        taskDayLabelSet()
        
        let taskNameLabelSet = {
            self.backView.addSubview(self.taskNameLabel)
            self.taskNameLabel.translatesAutoresizingMaskIntoConstraints = false
            
            if Sizes.isiPad() {
                self.taskNameLabel.font = UIFont.systemFont(ofSize: 20, weight: .medium)
            } else {
                self.taskNameLabel.font = Fonts.type1
            }
            
            self.taskNameLabel.textColor = Colors.shared.black
            self.taskNameLabel.numberOfLines = 0
            
            let top = NSLayoutConstraint(item: self.taskNameLabel, attribute: .top, relatedBy: .equal, toItem: self.backView, attribute: .top, multiplier: 1, constant: Sizes.margin1)
            let bottom = NSLayoutConstraint(item: self.taskNameLabel, attribute: .bottom, relatedBy: .equal, toItem: self.backView, attribute: .bottom, multiplier: 1, constant: -Sizes.margin1)
            let leading = NSLayoutConstraint(item: self.taskNameLabel, attribute: .leading, relatedBy: .equal, toItem: self.gradeValueLabel, attribute: .trailing, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.taskNameLabel, attribute: .trailing, relatedBy: .equal, toItem: self.taskDayLabel, attribute: .leading, multiplier: 1, constant: -Sizes.margin1)
            NSLayoutConstraint.activate([top, bottom, leading, trailing])
        }
        taskNameLabelSet()
    }
    
    @objc private func backViewLongTouch() {
        EffectManager.vibrate()
        longTouchDelegate?.statisticsListCellLongTouch(cell: self, index: tag)
    }
    
    func setValue(task: WorkManager.Work) {
        taskNameLabel.text = task.title
        
        if task.totalCount == 0 {
            gradeValueLabel.text = "?"
            gradeValueLabel.textColor = Colors.shared.textSub
        } else {
            gradeValueLabel.text = task.marks
            gradeValueLabel.textColor = Colors.shared.point
        }
        
        if task.totalCount == 0 {
            taskDayLabel.text = NSLocalizedString("TodayRegistered", comment: "")
        } else {
            taskDayLabel.text = "\(task.totalCount)\(NSLocalizedString("StatisticsDays", comment: ""))"
        }
    }
}
