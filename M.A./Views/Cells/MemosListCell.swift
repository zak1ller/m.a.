//
//  MemosListCell.swift
//  M.A.
//
//  Created by Min-Su Kim on 2020/08/04.
//  Copyright © 2020 Min-Su Kim. All rights reserved.
//

import Foundation
import UIKit

protocol MemosListCellLongPressDelegate: class {
    func memoListCellDidLongPress(cell: MemosListCell, index: Int)
}

class MemosListCell: UITableViewCell {
    var backView = UIView()
    var stackView = UIStackView()
    var contentLabel = UILabel()
    var dateLabel = UILabel()

    weak var longPressDelegate: MemosListCellLongPressDelegate?
    
    required init?(coder aDecoder: NSCoder) {super.init(coder: aDecoder)}
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        contentView.backgroundColor = Colors.shared.background
        
        let backViewSet = {
            self.contentView.addSubview(self.backView)
            self.backView.translatesAutoresizingMaskIntoConstraints = false
            self.backView.addGestureRecognizer(UILongPressGestureRecognizer(target: self, action: #selector(self.backViewLongPresse)))
            self.backView.backgroundColor = Colors.shared.backgroundSub
            self.backView.layer.cornerRadius = Sizes.radius
            
            let top = NSLayoutConstraint(item: self.backView, attribute: .top, relatedBy: .equal, toItem: self.contentView, attribute: .top, multiplier: 1, constant: 0)
            let bottom = NSLayoutConstraint(item: self.backView, attribute: .bottom, relatedBy: .equal, toItem: self.contentView, attribute: .bottom, multiplier: 1, constant: -Sizes.margin1*0.5)
            let leading = NSLayoutConstraint(item: self.backView, attribute: .leading, relatedBy: .equal, toItem: self.contentView, attribute: .leading, multiplier: 1, constant: Sizes.margin1)
            let trailing = NSLayoutConstraint(item: self.backView, attribute: .trailing, relatedBy: .equal, toItem: self.contentView, attribute: .trailing, multiplier: 1, constant: -Sizes.margin1)
            NSLayoutConstraint.activate([top, bottom, leading, trailing])
        }
        backViewSet()
        
        let stackViewSet = {
            self.backView.addSubview(self.stackView)
            self.stackView.translatesAutoresizingMaskIntoConstraints = false
            self.stackView.addArrangedSubview(self.contentLabel)
            self.stackView.addArrangedSubview(self.dateLabel)
            self.stackView.axis = .vertical
            self.stackView.distribution = .fill
            
            let top = NSLayoutConstraint(item: self.stackView, attribute: .top, relatedBy: .equal, toItem: self.backView, attribute: .top, multiplier: 1, constant: Sizes.margin1*0.5)
            let bottom = NSLayoutConstraint(item: self.stackView, attribute: .bottom, relatedBy: .equal, toItem: self.backView, attribute: .bottom, multiplier: 1, constant: -Sizes.margin1*0.5)
            let leading = NSLayoutConstraint(item: self.stackView, attribute: .leading, relatedBy: .equal, toItem: self.contentView, attribute: .leading, multiplier: 1, constant: Sizes.margin3)
            let trailing = NSLayoutConstraint(item: self.stackView, attribute: .trailing, relatedBy: .equal, toItem: self.contentView, attribute: .trailing, multiplier: 1, constant: -Sizes.margin3)
            NSLayoutConstraint.activate([top, bottom, leading, trailing])
        }
        stackViewSet()
        
        let contentLabelSet = {
            self.contentLabel.textColor = Colors.shared.black
            
            if Sizes.isiPad() {
                self.contentLabel.font = UIFont.systemFont(ofSize: 20, weight: .medium)
            } else {
                self.contentLabel.font = Fonts.type1
            }
            
            self.contentLabel.numberOfLines = 1
        }
        contentLabelSet()
        
        let dateLabelSet = {
            self.dateLabel.textColor = Colors.shared.textSub
            
            if Sizes.isiPad() {
                self.dateLabel.font = UIFont.systemFont(ofSize: 20, weight: .medium)
            } else {
                self.dateLabel.font = Fonts.type1
            }
            
        }
        dateLabelSet()
    }
    
    @objc private func backViewLongPresse() {
        EffectManager.vibrate()
        longPressDelegate?.memoListCellDidLongPress(cell: self, index: tag)
    }
    
    func setData(data: MemosManager.Memos) {
        contentLabel.text = data.contents
        dateLabel.text = StringManager.convertMemoDate(s: data.rdate)
    }
}
