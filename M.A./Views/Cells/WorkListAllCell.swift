//
//  WorkListAllCell.swift
//  M.A.
//
//  Created by Min-Su Kim on 2020/09/24.
//  Copyright © 2020 Min-Su Kim. All rights reserved.
//

import Foundation
import UIKit

protocol WorkListAllCellLongTouchDelegate: class {
    func workListAllCellLongTouch(cell: WorkListAllCell, index: Int)
}

class WorkListAllCell: UITableViewCell {
    private let backView = UIView()
    private let titleStackView = UIStackView()
    private let titleLabel = UILabel()
    private let dayLabel = UILabel()
    
    weak var longTouchDelegate: WorkListAllCellLongTouchDelegate?
    
    required init?(coder aDecoder: NSCoder) {super.init(coder: aDecoder)}
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        contentView.backgroundColor = Colors.shared.backgroundSub
        
        let backViewSet = {
            self.contentView.addSubview(self.backView)
            self.backView.translatesAutoresizingMaskIntoConstraints = false
            self.backView.addGestureRecognizer(UILongPressGestureRecognizer(target: self, action: #selector(self.backViewLongTouch)))
            
            let top = NSLayoutConstraint(item: self.backView, attribute: .top, relatedBy: .equal, toItem: self.contentView, attribute: .top, multiplier: 1, constant: Sizes.margin1*0.25)
            let bottom = NSLayoutConstraint(item: self.backView, attribute: .bottom, relatedBy: .equal, toItem: self.contentView, attribute: .bottom, multiplier: 1, constant: -Sizes.margin1*0.25)
            let leading = NSLayoutConstraint(item: self.backView, attribute: .leading, relatedBy: .equal, toItem: self.contentView, attribute: .leading, multiplier: 1, constant: Sizes.margin1)
            let trailing = NSLayoutConstraint(item: self.backView, attribute: .trailing, relatedBy: .equal, toItem: self.contentView, attribute: .trailing, multiplier: 1, constant: -Sizes.margin1)
            NSLayoutConstraint.activate([top, bottom, leading, trailing])
        }
        backViewSet()
    
        let titleStackViewSet = {
            self.backView.addSubview(self.titleStackView)
            self.titleStackView.translatesAutoresizingMaskIntoConstraints = false
            self.titleStackView.addArrangedSubview(self.titleLabel)
            self.titleStackView.addArrangedSubview(self.dayLabel)
            self.titleStackView.axis = .vertical
            self.titleStackView.distribution = .fill
            self.titleStackView.spacing = 4
            
            let top = NSLayoutConstraint(item: self.titleStackView, attribute: .top, relatedBy: .equal, toItem: self.backView, attribute: .top, multiplier: 1, constant: 8)
            let bottom = NSLayoutConstraint(item: self.titleStackView, attribute: .bottom, relatedBy: .equal, toItem: self.backView, attribute: .bottom, multiplier: 1, constant: -8)
            let leading = NSLayoutConstraint(item: self.titleStackView, attribute: .leading, relatedBy: .equal, toItem: self.contentView, attribute: .leading, multiplier: 1, constant: Sizes.margin3)
            let trailing = NSLayoutConstraint(item: self.titleStackView, attribute: .trailing, relatedBy: .equal, toItem: self.contentView, attribute: .trailing, multiplier: 1, constant: -Sizes.margin3)
            NSLayoutConstraint.activate([top, bottom, leading, trailing])
        }
        titleStackViewSet()
        
        let fontSet = {
            self.titleLabel.textColor = Colors.shared.black
            self.dayLabel.textColor = Colors.shared.textSub
            
            if Sizes.isiPad() {
                self.titleLabel.font = UIFont.systemFont(ofSize: 20, weight: .medium)
                self.dayLabel.font = UIFont.systemFont(ofSize: 15, weight: .medium)
            } else {
                self.titleLabel.font = Fonts.type1
                self.dayLabel.font = UIFont.systemFont(ofSize: 12, weight: .medium)
            }
        }
        fontSet()
    }
    
    @objc private func backViewLongTouch() {
        EffectManager.vibrate()
        longTouchDelegate?.workListAllCellLongTouch(cell: self, index: tag)
    }
    
    func setdata(work: WorkManager.Work) {
        titleLabel.text = work.title
        dayLabel.text = StringManager.getDaysString(work: work)
    }
}
