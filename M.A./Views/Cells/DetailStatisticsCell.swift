//
//  DetailStatisticsCell.swift
//  M.A.
//
//  Created by Min-Su Kim on 2020/07/31.
//  Copyright © 2020 Min-Su Kim. All rights reserved.
//

import Foundation
import UIKit

class DetailStatisticsCell: UITableViewCell {
    let iconImageView = UIImageView()
    let stackView = UIStackView()
    let titleLabel = UILabel()
    let valueLabel = UILabel()
    
    required init?(coder aDecoder: NSCoder) {super.init(coder: aDecoder)}
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        contentView.backgroundColor = Colors.shared.backgroundSub
        
        let iconImageViewSet = {
            self.contentView.addSubview(self.iconImageView)
            self.iconImageView.translatesAutoresizingMaskIntoConstraints = false
            
            let width = NSLayoutConstraint(item: self.iconImageView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Sizes.button3)
            let height = NSLayoutConstraint(item: self.iconImageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Sizes.button3)
            let leading = NSLayoutConstraint(item: self.iconImageView, attribute: .leading, relatedBy: .equal, toItem: self.contentView, attribute: .leading, multiplier: 1, constant: Sizes.margin3)
            let centerY = NSLayoutConstraint(item: self.iconImageView, attribute: .centerY, relatedBy: .equal, toItem: self.contentView, attribute: .centerY, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([width, height, leading, centerY])
        }
        iconImageViewSet()
        
        let stackViewSet = {
            self.contentView.addSubview(self.stackView)
            self.stackView.translatesAutoresizingMaskIntoConstraints = false
            self.stackView.addArrangedSubview(self.titleLabel)
            self.stackView.addArrangedSubview(self.valueLabel)
            self.stackView.axis = .horizontal
            self.stackView.distribution = .fill
            self.stackView.spacing = Sizes.margin1*0.25
            
            let top = NSLayoutConstraint(item: self.stackView, attribute: .top, relatedBy: .equal, toItem: self.contentView, attribute: .top, multiplier: 1, constant: Sizes.margin1)
            let bottom = NSLayoutConstraint(item: self.stackView, attribute: .bottom, relatedBy: .equal, toItem: self.contentView, attribute: .bottom, multiplier: 1, constant: -Sizes.margin1)
            let leading = NSLayoutConstraint(item: self.stackView, attribute: .leading, relatedBy: .equal, toItem: self.iconImageView, attribute: .trailing, multiplier: 1, constant: Sizes.margin1)
            let trailing = NSLayoutConstraint(item: self.stackView, attribute: .trailing, relatedBy: .equal, toItem: self.contentView, attribute: .trailing, multiplier: 1, constant: -Sizes.margin3)
            NSLayoutConstraint.activate([top, bottom, leading, trailing])
        }
        stackViewSet()
        
        let valueLabelSet = {
            self.valueLabel.textColor = Colors.shared.point
            
            if Sizes.isiPad() {
                self.valueLabel.font = UIFont.systemFont(ofSize: 22, weight: .bold)
            } else {
                self.valueLabel.font = UIFont.systemFont(ofSize: 15, weight: .bold)
            }
            
        }
        valueLabelSet()
        
        let titleLabelSet = {
            self.titleLabel.textColor = Colors.shared.black
            
            if Sizes.isiPad() {
                self.titleLabel.font = UIFont.systemFont(ofSize: 20, weight: .medium)
            } else {
                self.titleLabel.font = Fonts.type1
            }
            
        }
        titleLabelSet()
    }
}
