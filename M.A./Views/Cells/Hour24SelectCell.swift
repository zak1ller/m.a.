//
//  Hour24SelectCell.swift
//  M.A.
//
//  Created by Min-Su Kim on 2020/07/27.
//  Copyright © 2020 Min-Su Kim. All rights reserved.
//

import Foundation
import UIKit

class Hour24SelectCell: UITableViewCell {
    var valueLabel = UILabel()
    var checkedImageView = UIImageView()
    
    required init?(coder aDecoder: NSCoder) {super.init(coder: aDecoder)}
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        contentView.backgroundColor = Colors.shared.background
        ViewManager.appendLine(lineType: .bottom, view: contentView, spacing: Sizes.margin1)
        
        let valueLabelSet = {
            self.contentView.addSubview(self.valueLabel)
            self.valueLabel.translatesAutoresizingMaskIntoConstraints = false
            self.valueLabel.font = Fonts.type1
            self.valueLabel.textColor = Colors.shared.black
            
            let top = NSLayoutConstraint(item: self.valueLabel, attribute: .top, relatedBy: .equal, toItem: self.contentView, attribute: .top, multiplier: 1, constant: Sizes.margin1*0.75)
            let bottom = NSLayoutConstraint(item: self.valueLabel, attribute: .bottom, relatedBy: .equal, toItem: self.contentView, attribute: .bottom, multiplier: 1, constant: -Sizes.margin1*0.75)
            let leading = NSLayoutConstraint(item: self.valueLabel, attribute: .leading, relatedBy: .equal, toItem: self.contentView, attribute: .leading, multiplier: 1, constant: Sizes.margin3)
            NSLayoutConstraint.activate([top, bottom, leading])
        }
        valueLabelSet()
        
        let checkedImageViewSet = {
            self.contentView.addSubview(self.checkedImageView)
            self.checkedImageView.translatesAutoresizingMaskIntoConstraints = false
            self.checkedImageView.image = UIImage(named: "check.png")
            self.checkedImageView.setImageColor(color: Colors.shared.black)
            
            let width = NSLayoutConstraint(item: self.checkedImageView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Sizes.button3)
            let height = NSLayoutConstraint(item: self.checkedImageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Sizes.button3)
            let trailing = NSLayoutConstraint(item: self.checkedImageView, attribute: .trailing, relatedBy: .equal, toItem: self.contentView, attribute: .trailing, multiplier: 1, constant: -Sizes.margin3)
            let centerY = NSLayoutConstraint(item: self.checkedImageView, attribute: .centerY, relatedBy: .equal, toItem: self.contentView, attribute: .centerY, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([width, height, trailing, centerY])
        }
        checkedImageViewSet()
    }
    
    func setType(isSelected: Bool) {
        if isSelected {
            checkedImageView.isHidden = false
        } else {
            checkedImageView.isHidden = true
        }
    }
}
