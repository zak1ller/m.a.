//
//  LastDayResultCell.swift
//  M.A.
//
//  Created by Min-Su Kim on 2020/08/01.
//  Copyright © 2020 Min-Su Kim. All rights reserved.
//

import Foundation
import UIKit

class LastDayResultCell: UITableViewCell {
    let rightStackView = UIStackView()
    let resultImageView = UIImageView()
    let titleLabel = UILabel()
    let gradeStackView = UIStackView()
    let gradeTitleLabel = UILabel()
    let gradeImageView = UIImageView()
    let gradeValueLabel = UILabel()
    let percentStackView = UIStackView()
    let percentTitleLabel = UILabel()
    let percentImageView = UIImageView()
    let percentValueLabel = UILabel()
   
    required init?(coder aDecoder: NSCoder) {super.init(coder: aDecoder)}
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        contentView.backgroundColor = Colors.shared.background
        
        let resultImageViewSet = {
            self.contentView.addSubview(self.resultImageView)
            self.resultImageView.translatesAutoresizingMaskIntoConstraints = false
            
            let top = NSLayoutConstraint(item: self.resultImageView, attribute: .top, relatedBy: .equal, toItem: self.contentView, attribute: .top, multiplier: 1, constant: Sizes.margin1)
            let leading = NSLayoutConstraint(item: self.resultImageView, attribute: .leading, relatedBy: .equal, toItem: self.contentView, attribute: .leading, multiplier: 1, constant: Sizes.margin3)
            let width = NSLayoutConstraint(item: self.resultImageView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 17)
            let height = NSLayoutConstraint(item: self.resultImageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 17)
            NSLayoutConstraint.activate([top, leading, width, height])
        }
        resultImageViewSet()
        
        let rightStackViewSet = {
            self.contentView.addSubview(self.rightStackView)
            self.rightStackView.translatesAutoresizingMaskIntoConstraints = false
            self.rightStackView.addArrangedSubview(self.titleLabel)
            self.rightStackView.addArrangedSubview(self.gradeStackView)
            self.rightStackView.addArrangedSubview(self.percentStackView)
            self.rightStackView.axis = .vertical
            self.rightStackView.distribution = .fill
            self.rightStackView.spacing = Sizes.margin1*0.25
            
            let top = NSLayoutConstraint(item: self.rightStackView, attribute: .top, relatedBy: .equal, toItem: self.contentView, attribute: .top, multiplier: 1, constant: Sizes.margin1)
            let bottom = NSLayoutConstraint(item: self.rightStackView, attribute: .bottom, relatedBy: .equal, toItem: self.contentView, attribute: .bottom, multiplier: 1, constant: -Sizes.margin1)
            let leading = NSLayoutConstraint(item: self.rightStackView, attribute: .leading, relatedBy: .equal, toItem: self.resultImageView, attribute: .trailing, multiplier: 1, constant: Sizes.margin1)
            let trailing = NSLayoutConstraint(item: self.rightStackView, attribute: .trailing, relatedBy: .equal, toItem: self.contentView, attribute: .trailing, multiplier: 1, constant: -Sizes.margin3)
            NSLayoutConstraint.activate([top, bottom, leading, trailing])
        }
        rightStackViewSet()
        
        let gradeStackViewSet = {
            self.gradeStackView.addArrangedSubview(self.gradeTitleLabel)
            self.gradeStackView.addArrangedSubview(self.gradeImageView)
            self.gradeStackView.addArrangedSubview(self.gradeValueLabel)
            self.gradeStackView.addArrangedSubview(UIView())
            self.gradeStackView.axis = .horizontal
            self.gradeStackView.distribution = .fill
            self.gradeStackView.spacing = Sizes.margin1*0.25
        }
        gradeStackViewSet()
        
        let percentStackViewSet = {
            self.percentStackView.addArrangedSubview(self.percentTitleLabel)
            self.percentStackView.addArrangedSubview(self.percentImageView)
            self.percentStackView.addArrangedSubview(self.percentValueLabel)
            self.percentStackView.addArrangedSubview(UIView())
            self.percentStackView.axis = .horizontal
            self.percentStackView.distribution = .fill
            self.percentStackView.spacing = Sizes.margin1*0.25
        }
        percentStackViewSet()
        
        let gradeImageViewSet = {
            self.gradeImageView.translatesAutoresizingMaskIntoConstraints = false
            
            let width = NSLayoutConstraint(item: self.gradeImageView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 15)
            let height = NSLayoutConstraint(item: self.gradeImageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 15)
            NSLayoutConstraint.activate([width, height])
        }
        gradeImageViewSet()
        
        let percentImageViewSet = {
            self.percentImageView.translatesAutoresizingMaskIntoConstraints = false
            
            let width = NSLayoutConstraint(item: self.percentImageView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 15)
            let height = NSLayoutConstraint(item: self.percentImageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 15)
            NSLayoutConstraint.activate([width, height])
        }
        percentImageViewSet()
        
        let fontSet = {
            self.titleLabel.textColor = Colors.shared.point
            self.titleLabel.font = Fonts.type1
            self.titleLabel.textAlignment = .left
            self.titleLabel.numberOfLines = 2
            
            self.gradeTitleLabel.text = NSLocalizedString("DetailGrade", comment: "")
            self.gradeTitleLabel.textColor = Colors.shared.buttonSub
            self.gradeTitleLabel.font = Fonts.type2
            self.percentTitleLabel.text = NSLocalizedString("DetailRate", comment: "")
            self.percentTitleLabel.textColor = Colors.shared.buttonSub
            self.percentTitleLabel.font = Fonts.type2
            
            self.gradeValueLabel.textColor = Colors.shared.textSub
            self.gradeValueLabel.font = Fonts.type2
            self.percentValueLabel.textColor = Colors.shared.textSub
            self.percentValueLabel.font = Fonts.type2
        }
        fontSet()
    }
    
    func setData(data: WorkManager.lastDayResultData) {
        gradeStackView.isHidden = true
        percentStackView.isHidden = true
        
        titleLabel.text = data.title
        gradeValueLabel.text = data.todayRank
        gradeValueLabel.textColor = Colors.shared.point
        percentValueLabel.textColor = Colors.shared.point
        
        if data.isCheck {
            resultImageView.image = UIImage(named: "success.png")
        } else {
            resultImageView.image = UIImage(named: "failure.png")
        }
        
        // 점수가 하락했을 경우
        if data.lastAvg > data.todayAvg {
            percentValueLabel.text = "-\(String(format: "%.1f", data.lastAvg-data.todayAvg))%"
            percentImageView.image = UIImage(named: "fall.png")
            percentImageView.setImageColor(color: UIColor.systemBlue)
            percentValueLabel.textColor = UIColor.systemBlue
           
            // 등급이 하락했을 경우
            if data.lastRank != data.todayRank {
                gradeImageView.image = UIImage(named: "fall.png")
                gradeImageView.setImageColor(color: UIColor.systemBlue)
                gradeValueLabel.textColor = UIColor.systemBlue
                
                gradeStackView.isHidden = false
            }
            percentStackView.isHidden = false
        }
        
        // 점수가 상승했을 경우
        if data.lastAvg < data.todayAvg {
            percentValueLabel.text = "\(String(format: "%.1f", data.todayAvg-data.lastAvg))%"
            percentImageView.image = UIImage(named: "rise.png")
            percentImageView.setImageColor(color: UIColor.systemRed)
            percentValueLabel.textColor = UIColor.systemRed
            
            // 등급이 상승했을 경우
            if data.lastRank != data.todayRank {
                gradeImageView.image = UIImage(named: "rise.png")
                gradeImageView.setImageColor(color: UIColor.systemRed)
                gradeValueLabel.textColor = UIColor.systemRed
                
                gradeStackView.isHidden = false
            }
            percentStackView.isHidden = false
        }
    }
}
