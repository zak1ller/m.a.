//
//  MemoGroupSelectVC.swift
//  M.A.
//
//  Created by Min-Su Kim on 2020/09/26.
//  Copyright © 2020 Min-Su Kim. All rights reserved.
//

import Foundation
import UIKit

protocol MemoGroupSelectVcDelegate: class {
    func memoGroupSelectDidChange()
    func memoGroupDidEdit()
}

class MemoGroupSelectVC: UIViewController, TopViewBackButtonDelegate, UITableViewDelegate, UITableViewDataSource, TopViewRightTextButtonDelegate, OnelineEditVCDelegate, MemoGroupListCellLongPressDelegate {
    private var loadingAnim: LoadingAnim!
    private let topView = TopView()
    private let tableView = UITableView()
    
    weak var delegate: MemoGroupSelectVcDelegate?
    
    private var list: [MemosManager.MemoGroup] = []
    private let newGroup = 1
    private let updateGroup = 2
    private let updateDefaultGroup = 3
    
    deinit {
        print("Memo Group Select VC Deinited.")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setLayout()
        
        loadingAnim = LoadingAnim(view: view)
        loadingAnim.setColor(color: Colors.shared.buttonSub)
        
        reloadGroups()
    }
    
    func pressBackButton() {
        dismiss(animated: true, completion: nil)
    }
    
    func pressRightTextButton() {
        let vc = OnelineEditVC()
        vc.tag = newGroup
        vc.isbecome = true
        vc.isdismissanimation = true
        vc.settitle(title: NSLocalizedString("NewGroup", comment: ""))
        vc.mintext = 1
        vc.maxtext = 20
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
    
    func onelineEditDidDone(_ oneLineEditVC: OnelineEditVC, value: String, tag: Int, index: Int) {
        if tag == newGroup {
            loadingAnim.start()
            
            DispatchQueue.global().async(execute: {
                MemosManager.newGroup(title: value, result: {
                    error in
                    self.loadingAnim.stop()
                    
                    if let error = error {
                        let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                        DispatchQueue.main.async(execute: {
                            self.present(alert, animated: true, completion: nil)
                        })
                    } else {
                        self.reloadGroups()
                    }
                })
            })
        } else if tag == updateGroup {
            let item = list[index]
            
            if item.title == value {
                return
            }
            
            loadingAnim.start()
            
            DispatchQueue.global().async(execute: {
                MemosManager.updateGroupName(title: item.title, newTitle: value, result: {
                    error in
                    self.loadingAnim.stop()
                    
                    if let error = error {
                        let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                        DispatchQueue.main.async(execute: {
                            self.present(alert, animated: true, completion: nil)
                        })
                    } else {
                        self.delegate?.memoGroupDidEdit()
                        self.reloadGroups()
                    }
                })
            })
        } else if tag == updateDefaultGroup {
            if value.count == 0 {
                UserDefaultManager.shared.memoBasicName = nil
            } else {
                UserDefaultManager.shared.memoBasicName = value.trimmingCharacters(in: .whitespacesAndNewlines)
            }
            
            delegate?.memoGroupDidEdit()
            reloadGroups()
        }
    }
    
    func memoGroupListCellLongPressed(index: Int) {
        if index == 0 {
            // 기본 그룹은 이름 변경만 가능하며 기본 그룹은 서버에 등록 되어 있지 않으므로 Default name 값만 설정한다.
            let vc = OnelineEditVC()
            vc.tag = self.updateDefaultGroup
            vc.index = index
            vc.delegate = self
            vc.isbecome = true
            vc.isdismissanimation = true
            vc.settitle(title: NSLocalizedString("NewMemoGroupName", comment: ""))
            vc.mintext = 0
            vc.maxtext = 20
            
            if let v = UserDefaultManager.shared.memoBasicName {
                vc.setFieldText(text: v)
            } else {
                vc.setplaceholder(title: NSLocalizedString("BasicMemoGroup", comment: ""))
            }
            
            DispatchQueue.main.async(execute: {
                self.present(vc, animated: true, completion: nil)
            })
        } else {
            let item = list[index]
            let alert = UIAlertController(title: item.title, message: nil, preferredStyle: ViewManager.actionSheetForiPad())
            alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil))
            alert.addAction(UIAlertAction(title: NSLocalizedString("Edit", comment: ""), style: .default, handler: {
                (_) in
                let vc = OnelineEditVC()
                vc.tag = self.updateGroup
                vc.index = index
                vc.delegate = self
                vc.isbecome = true
                vc.isdismissanimation = true
                vc.settitle(title: NSLocalizedString("NewMemoGroupName", comment: ""))
                vc.setFieldText(text: item.title)
                vc.mintext = 1
                vc.maxtext = 20
                DispatchQueue.main.async(execute: {
                    self.present(vc, animated: true, completion: nil)
                })
            }))
            alert.addAction(UIAlertAction(title: NSLocalizedString("Delete", comment: ""), style: .destructive, handler: {
                (_) in
                let alert = UIAlertController(title: NSLocalizedString("MemoGroupDeleteMessage", comment: ""), message: nil, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil))
                alert.addAction(UIAlertAction(title: NSLocalizedString("Delete", comment: ""), style: .destructive, handler: {
                    (_) in
                    self.loadingAnim.start()
                    
                    DispatchQueue.global().async(execute: {
                        MemosManager.deleteGroup(title: item.title, result: {
                            error in
                            self.loadingAnim.stop()
                            
                            if let error = error {
                                let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                                alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                                DispatchQueue.main.async(execute: {
                                    self.present(alert, animated: true, completion: nil)
                                })
                            } else {
                                self.delegate?.memoGroupDidEdit()
                                self.reloadGroups()
                            }
                        })
                    })
                }))
                DispatchQueue.main.async(execute: {
                    self.present(alert, animated: true, completion: nil)
                })
            }))
            DispatchQueue.main.async(execute: {
                self.present(alert, animated: true, completion: nil)
            })
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MemoGroupListCell", for: indexPath) as! MemoGroupListCell
        cell.tag = indexPath.row
        cell.delegate = self
        cell.setData(data: list[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            UserDefaultManager.shared.memoGroup = nil
        } else {
            UserDefaultManager.shared.memoGroup = list[indexPath.row].title
        }
       
        dismiss(animated: true, completion: {
            self.delegate?.memoGroupSelectDidChange()
        })
    }
    
    private func reloadGroups() {
        loadingAnim.start()
        
        DispatchQueue.global().async(execute: {
            MemosManager.getGroups(result: {
                error, list in
                self.loadingAnim.stop()
                
                if let error = error {
                    let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                    DispatchQueue.main.async(execute: {
                        self.present(alert, animated: true, completion: nil)
                    })
                } else {
                    self.list =  list
                    
                    DispatchQueue.main.async(execute: {
                        self.tableView.reloadData()
                    })
                }
            })
        })
    }
    
    private func setLayout() {
        view.backgroundColor = Colors.shared.backgroundSub
        
        if #available(iOS 13, *) {
            topView.showOnSubView(toView: view)
        } else {
            topView.show(toView: view)
        }
        
        topView.settitle(title: NSLocalizedString("MemoGroup", comment: ""), vc: self)
        topView.showBackButton()
        topView.backButtonDelegate = self
        topView.showRightTextButton(title: NSLocalizedString("NewGroup", comment: ""))
        topView.rightTextButtonDelegate = self
        
        let tableViewSet = {
            self.view.addSubview(self.tableView)
            self.tableView.translatesAutoresizingMaskIntoConstraints = false
            self.tableView.register(MemoGroupListCell.self, forCellReuseIdentifier: "MemoGroupListCell")
            self.tableView.delegate = self
            self.tableView.dataSource = self
            self.tableView.rowHeight = UITableView.automaticDimension
            self.tableView.tableFooterView = UIView()
            self.tableView.backgroundColor = Colors.shared.backgroundSub
            
            let top = NSLayoutConstraint(item: self.tableView, attribute: .top, relatedBy: .equal, toItem: self.topView, attribute: .bottom, multiplier: 1, constant: Sizes.marginTop)
            let bottom = NSLayoutConstraint(item: self.tableView, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: 0)
            let leading = NSLayoutConstraint(item: self.tableView, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.tableView, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([top, bottom, leading, trailing])
        }
        tableViewSet()
    }
}
