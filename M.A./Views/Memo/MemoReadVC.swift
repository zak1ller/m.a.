//
//  MemoReadVC.swift
//  M.A.
//
//  Created by Min-Su Kim on 2020/08/05.
//  Copyright © 2020 Min-Su Kim. All rights reserved.
//

import Foundation
import UIKit

protocol MemoReadVcDelegate: class {
    func memoReadDidEdit()
}

class MemoReadVC: UIViewController, TopViewBackButtonDelegate, TopViewRightTextButtonDelegate, UITextViewDelegate {
    private var loadingAnim: LoadingAnim!
    private let topView = TopView()
    private let contentsTextView = UITextView()
    
    weak var delegate: MemoReadVcDelegate?
    
    private var memo: MemosManager.Memos = MemosManager.Memos();
    private var contentsTextViewBottomConstraint: NSLayoutConstraint!
    
    deinit {
        print("Memo Read VC Deinited.")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: UIWindow.keyboardWillShowNotification,
            object: nil)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHide(_:)),
            name: UIWindow.keyboardWillHideNotification,
            object: nil)
        
        setLayout()
        
        loadingAnim = LoadingAnim(view: view)
        loadingAnim.setColor(color: Colors.shared.buttonSub)
    }

    @objc private func keyboardWillShow(_ notificaiton: Notification) {
        if let keyboardFrame: NSValue = notificaiton.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardHeight = keyboardFrame.cgRectValue.height
            contentsTextViewBottomConstraint.constant = -(keyboardHeight+Sizes.margin1)
        }
    }
    
    @objc private func keyboardWillHide(_ notification: Notification) {
        contentsTextViewBottomConstraint.constant = -Sizes.margin1
    }
    
    func pressBackButton() {
        dismiss(animated: true, completion: nil)
    }
    
    func pressRightTextButton() {
        contentsTextView.endEditing(true)
        
        let before = memo.contents
        let after = contentsTextView.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if after.count == 0 {
            // 삭제
            loadingAnim.start()
            
            DispatchQueue.global().async(execute: {
                MemosManager.deleteMemos(id: self.memo.id, result: {
                    error in
                    self.loadingAnim.stop()
                    
                    if let error = error {
                        let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                        DispatchQueue.main.async(execute: {
                            self.present(alert, animated: true, completion: nil)
                        })
                    } else {
                        DispatchQueue.main.async(execute: {
                            self.dismiss(animated: true, completion: {
                                self.delegate?.memoReadDidEdit()
                            })
                        })
                    }
                })
            })
        } else if before == after {
            // 작업 없음
        } else {
            loadingAnim.start()
            
            DispatchQueue.global().async(execute: {
                MemosManager.updateMemos(id: self.memo.id, contents: before, newContents: after, result: {
                    error in
                    self.loadingAnim.stop()
                    
                    if let error = error {
                        let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                        DispatchQueue.main.async(execute: {
                            self.present(alert, animated: true, completion: nil)
                        })
                    } else {
                        DispatchQueue.main.async(execute: {
                            self.dismiss(animated: true, completion: {
                                self.delegate?.memoReadDidEdit()
                            })
                        })
                    }
                })
            })
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        topView.showRightTextButton(title: NSLocalizedString("Done", comment: ""))
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        topView.hideRightTextButton()
    }
    
    func setContents(memo: MemosManager.Memos) {
        self.memo = memo
        self.contentsTextView.text = memo.contents
    }
    
    private func setLayout() {
        view.backgroundColor = Colors.shared.backgroundSub
        
        if #available(iOS 13, *) {
            topView.showOnSubView(toView: view)
        } else {
            topView.show(toView: view)
        }
        
        topView.showBackButton()
        topView.backButtonDelegate = self
        topView.rightTextButtonDelegate = self
        
        let contentsLabelSet = {
            self.view.addSubview(self.contentsTextView)
            self.contentsTextView.translatesAutoresizingMaskIntoConstraints = false
            self.contentsTextView.delegate = self
            
            if Sizes.isiPad() {
                self.contentsTextView.font = UIFont.systemFont(ofSize: 25, weight: .medium)
            } else {
                self.contentsTextView.font = UIFont.systemFont(ofSize: 18, weight: .medium)
            }
            
            self.contentsTextView.textColor = Colors.shared.black
            self.contentsTextView.tintColor = Colors.shared.point
            self.contentsTextView.backgroundColor = Colors.shared.backgroundSub
            
            let top = NSLayoutConstraint(item: self.contentsTextView, attribute: .top, relatedBy: .equal, toItem: self.topView, attribute: .bottom, multiplier: 1, constant: Sizes.marginTop)
            self.contentsTextViewBottomConstraint = NSLayoutConstraint(item: self.contentsTextView, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: -Sizes.bottomSpace)
            let leading = NSLayoutConstraint(item: self.contentsTextView, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: Sizes.margin3)
            let trailing = NSLayoutConstraint(item: self.contentsTextView, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: -Sizes.margin3)
            NSLayoutConstraint.activate([top, self.contentsTextViewBottomConstraint, leading, trailing])
        }
        contentsLabelSet()
    }
}
