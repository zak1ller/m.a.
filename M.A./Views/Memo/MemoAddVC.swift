//
//  MemoAddVC.swift
//  M.A.
//
//  Created by Min-Su Kim on 2020/08/04.
//  Copyright © 2020 Min-Su Kim. All rights reserved.
//

import Foundation
import UIKit

protocol MemoAddVcDelegate: class {
    func memoAddVcDidAdd()
}

class MemoAddVC: UIViewController, TopViewBackButtonDelegate, TopViewRightTextButtonDelegate, UITextViewDelegate {
    private var loadingAnim: LoadingAnim!
    private let topView = TopView()
    private let inputFieldBackView = UIView()
    private let inputField = UITextView()
    private let remainCountLabel = UILabel()
    
    weak var delegate: MemoAddVcDelegate?
    private var inputFieldBackViewBottomConstraint: NSLayoutConstraint!
    private let maxCount = 2000
   
    deinit {
        print("Memo Add VC Deinited.")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: UIWindow.keyboardWillShowNotification,
            object: nil)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHide(_:)),
            name: UIWindow.keyboardWillHideNotification,
            object: nil)
        
        setLayout()
        
        loadingAnim = LoadingAnim(view: view)
        loadingAnim.setColor(color: Colors.shared.buttonSub)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        inputField.becomeFirstResponder()
        updateRemainCount()
    }
    
    @objc private func keyboardWillShow(_ notificaiton: Notification) {
        if let keyboardFrame: NSValue = notificaiton.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardHeight = keyboardFrame.cgRectValue.height
            inputFieldBackViewBottomConstraint.constant = -(keyboardHeight+Sizes.margin1)
        }
    }
    
    @objc private func keyboardWillHide(_ notification: Notification) {
        inputFieldBackViewBottomConstraint.constant = -Sizes.margin1
    }
    
    func pressBackButton() {
        dismiss(animated: true, completion: nil)
    }
    
    func pressRightTextButton() {
        save()
    }
    
    func textViewDidChange(_ textView: UITextView) {
        updateRemainCount()
    }
    
    private func updateRemainCount() {
        let count = maxCount-inputField.text.count
        if count >= 0 {
            remainCountLabel.textColor = Colors.shared.textSub
        } else {
            remainCountLabel.textColor = UIColor.systemRed
        }
        remainCountLabel.text = "\(count)"
    }
    
    private func save() {
        let contents = inputField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        
        if contents.count == 0 {
            return
        }
        
        loadingAnim.start()
        
        let g: String
        if let group = UserDefaultManager.shared.memoGroup {
            g = group
        } else {
            g = ""
        }

        DispatchQueue.global().async(execute: {
            MemosManager.add(contents: contents, group: g, result: {
                error in
                self.loadingAnim.stop()
                
                if let error = error {
                    let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                    DispatchQueue.main.async(execute: {
                        self.present(alert, animated: true, completion: nil)
                    })
                } else {
                    DispatchQueue.main.async(execute: {
                        self.dismiss(animated: true, completion: {
                            self.delegate?.memoAddVcDidAdd()
                        })
                    })
                }
            })
        })
    }
    
    private func setLayout() {
        view.backgroundColor = Colors.shared.backgroundSub
        
        if #available(iOS 13, *) {
            topView.showOnSubView(toView: view)
        } else {
            topView.show(toView: view)
        }
        
        topView.showBackButton()
        topView.backButtonDelegate = self
        topView.showRightTextButton(title: NSLocalizedString("Done", comment: ""))
        topView.rightTextButtonDelegate = self
        
        let inputFieldSet = {
            self.view.addSubview(self.inputField)
            self.inputField.translatesAutoresizingMaskIntoConstraints = false
            self.inputField.delegate = self
            self.inputField.textColor = Colors.shared.black
            self.inputField.font = UIFont.systemFont(ofSize: 18, weight: .medium)
            self.inputField.tintColor = Colors.shared.point
            self.inputField.backgroundColor = Colors.shared.backgroundSub
            self.inputField.layer.cornerRadius = Sizes.radius
            self.inputField.returnKeyType = .done
            
            let top = NSLayoutConstraint(item: self.inputField, attribute: .top, relatedBy: .equal, toItem: self.topView, attribute: .bottom, multiplier: 1, constant: Sizes.marginTop)
            let leading = NSLayoutConstraint(item: self.inputField, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: Sizes.margin3)
            let trailing = NSLayoutConstraint(item: self.inputField, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: -Sizes.margin3)
            self.inputFieldBackViewBottomConstraint = NSLayoutConstraint(item: self.inputField, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: -Sizes.margin1)
            NSLayoutConstraint.activate([top, leading, trailing, self.inputFieldBackViewBottomConstraint])
        }
        inputFieldSet()
        
        let remainCountLabelSet = {
            self.view.addSubview(self.remainCountLabel)
            self.remainCountLabel.translatesAutoresizingMaskIntoConstraints = false
            self.remainCountLabel.textColor = Colors.shared.textSub
            self.remainCountLabel.font = Fonts.type1
            
            let bottom = NSLayoutConstraint(item: self.remainCountLabel, attribute: .bottom, relatedBy: .equal, toItem: self.inputField, attribute: .bottom, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.remainCountLabel, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: -Sizes.margin1)
            NSLayoutConstraint.activate([bottom, trailing])
        }
        remainCountLabelSet()
    }
}
