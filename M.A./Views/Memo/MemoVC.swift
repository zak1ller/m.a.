//
//  MemoVC.swift
//  M.A.
//
//  Created by Min-Su Kim on 2020/08/04.
//  Copyright © 2020 Min-Su Kim. All rights reserved.
//

import Foundation
import UIKit

class MemoVC: UIViewController, MemoAddVcDelegate, UITableViewDelegate, UITableViewDataSource, MemosListCellLongPressDelegate, TopViewRightTextButtonDelegate, MemoGroupSelectVcDelegate, MemoReadVcDelegate {
    
    private var loadingAnim: LoadingAnim!
    private var topView: TopView!
    private let groupSelectButton = UIButton(type: .system)
    private let groupSelectStackView = UIStackView()
    private let groupSelectTextLabel = UILabel()
    private let groupSelectArrowImage = UIImageView()
    private let addImageView = UIImageView()
    private let tableView = UITableView()
    
    private var list: [MemosManager.Memos] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        topView = TopView()
        
        DispatchQueue.main.async(execute: {
            self.setLayout()
            self.loadingAnim = LoadingAnim(view: self.view)
            self.loadingAnim.setColor(color: Colors.shared.buttonSub)
            self.reloadMemo()
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        updateTitle()
    }

    @objc private func pressGroupSelectButton(sender: UIButton) {
        let vc = MemoGroupSelectVC()
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
    
    func pressRightTextButton() {
        let vc = MemoAddVC()
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
    
    func memoAddVcDidAdd() {
        reloadMemo()
    }
    
    func memoReadDidEdit() {
        reloadMemo()
    }
    
    func memoGroupDidEdit() {
        UserDefaultManager.shared.memoGroup = nil
        DispatchQueue.main.async(execute: {
            self.updateTitle()
        })
        reloadMemo()
    }
    
    func memoGroupSelectDidChange() {
        updateTitle()
        reloadMemo()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MemosListCell", for: indexPath) as! MemosListCell
        cell.longPressDelegate = self
        cell.tag = indexPath.row
        cell.setData(data: list[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let memo = list[indexPath.row]
        
        let vc = MemoReadVC()
        vc.delegate = self
        vc.setContents(memo: memo)
        DispatchQueue.main.async(execute: {
            self.present(vc, animated: true, completion: nil)
        })
    }
    
    func memoListCellDidLongPress(cell: MemosListCell, index: Int) {
        let item = list[index]
       
        let alert = UIAlertController(title: item.contents, message: nil, preferredStyle: ViewManager.actionSheetForiPad())
        alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: NSLocalizedString("Delete", comment: ""), style: .destructive, handler: {
            (_) in
            let alert = UIAlertController(title: NSLocalizedString("DeleteMessage", comment: ""), message: nil, preferredStyle: ViewManager.actionSheetForiPad())
            alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil))
            alert.addAction(UIAlertAction(title: NSLocalizedString("Delete", comment: ""), style: .destructive, handler: {
                (_) in
                self.loadingAnim.start()
                
                DispatchQueue.global().async(execute: {
                    MemosManager.deleteMemos(id: item.id, result: {
                        error in
                        self.loadingAnim.stop()
                        
                        if let error = error {
                            let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                            DispatchQueue.main.async(execute: {
                                self.present(alert, animated: true, completion: nil)
                            })
                        } else {
                            self.list.remove(at: index)
                            DispatchQueue.main.async(execute: {
                                self.tableView.reloadData()
                            })
                        }
                    })
                })
            }))
            self.present(alert, animated: true, completion: nil)
        }))
        present(alert, animated: true, completion: nil)
    }

    private func reloadMemo() {
        loadingAnim.start()
        
        DispatchQueue.global().async(execute: {
            let g: String
            if let group = UserDefaultManager.shared.memoGroup {
                g = group
            } else {
                g = ""
            }
            
            MemosManager.getMemos(group: g, result: {
                error, list in
                self.loadingAnim.stop()
                
                if let error = error {
                    let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                    DispatchQueue.main.async(execute: {
                        self.present(alert, animated: true, completion: nil)
                    })
                } else {
                    self.list = list
                    DispatchQueue.main.async(execute: {
                        self.tableView.reloadData()
                    })
                }
            })
        })
    }
    
    private func updateTitle() {
        if let title = UserDefaultManager.shared.memoGroup {
            groupSelectTextLabel.text = title
        } else {
            if let v = UserDefaultManager.shared.memoBasicName {
                groupSelectTextLabel.text = v
            } else {
                groupSelectTextLabel.text = NSLocalizedString("BasicMemoGroup", comment: "")
            }
        }
    }
    
    private func setLayout() {
        view.backgroundColor = Colors.shared.background
        
        topView.show(toView: view)
        topView.setbackcolor(color: Colors.shared.background)
        topView.showRightTextButton(title: NSLocalizedString("NewMemo", comment: ""))
        topView.rightTextButtonDelegate = self
        
        let groupSelectButtonSet = {
            self.view.addSubview(self.groupSelectButton)
            self.groupSelectButton.translatesAutoresizingMaskIntoConstraints = false
            self.groupSelectButton.addTarget(self, action: #selector(self.pressGroupSelectButton(sender:)), for: .touchUpInside)
            
            let bottom = NSLayoutConstraint(item: self.groupSelectButton, attribute: .bottom, relatedBy: .equal, toItem: self.topView, attribute: .bottom, multiplier: 1, constant: 0)
            let height = NSLayoutConstraint(item: self.groupSelectButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Sizes.button2)
            let leading = NSLayoutConstraint(item: self.groupSelectButton, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: Sizes.margin2*2)
            let trailing = NSLayoutConstraint(item: self.groupSelectButton, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: -Sizes.margin2*2)
            NSLayoutConstraint.activate([bottom, height, leading, trailing])
        }
        groupSelectButtonSet()
        
        let groupSelectStackViewSet = {
            self.groupSelectButton.addSubview(self.groupSelectStackView)
            self.groupSelectStackView.translatesAutoresizingMaskIntoConstraints = false
            self.groupSelectStackView.addArrangedSubview(self.groupSelectTextLabel)
            self.groupSelectStackView.addArrangedSubview(self.groupSelectArrowImage)
            self.groupSelectStackView.axis = .horizontal
            self.groupSelectStackView.distribution = .fill
            self.groupSelectStackView.spacing = 0
            self.groupSelectStackView.isUserInteractionEnabled = false

            let centerX = NSLayoutConstraint(item: self.groupSelectStackView, attribute: .centerX, relatedBy: .equal, toItem: self.groupSelectButton, attribute: .centerX, multiplier: 1, constant: 0)
            let centerY = NSLayoutConstraint(item: self.groupSelectStackView, attribute: .centerY, relatedBy: .equal, toItem: self.groupSelectButton, attribute: .centerY, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([centerX, centerY])
            
            let groupSelectTextLabelSet = {
                self.groupSelectTextLabel.textColor = Colors.shared.black
                
                if Sizes.isiPad() {
                    self.groupSelectTextLabel.font = UIFont.systemFont(ofSize: 25, weight: .bold)
                } else {
                    self.groupSelectTextLabel.font = Fonts.title
                }
                
            }
            groupSelectTextLabelSet()
            
            let groupSelectArroImageSet = {
                self.groupSelectArrowImage.translatesAutoresizingMaskIntoConstraints = false
                self.groupSelectArrowImage.image = UIImage(named: "button_arrow_down.png")
                self.groupSelectArrowImage.setImageColor(color: Colors.shared.black)
                
                let width: NSLayoutConstraint
                let height: NSLayoutConstraint
                
                if Sizes.isiPad() {
                    width = NSLayoutConstraint(item: self.groupSelectArrowImage, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 30)
                    height = NSLayoutConstraint(item: self.groupSelectArrowImage, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 30)
                } else {
                    width = NSLayoutConstraint(item: self.groupSelectArrowImage, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 20)
                    height = NSLayoutConstraint(item: self.groupSelectArrowImage, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 20)
                }
                
                NSLayoutConstraint.activate([width, height])
            }
            groupSelectArroImageSet()
        }
        groupSelectStackViewSet()
        
        let tableViewSet = {
            self.view.addSubview(self.tableView)
            self.tableView.translatesAutoresizingMaskIntoConstraints = false
            self.tableView.register(MemosListCell.self, forCellReuseIdentifier: "MemosListCell")
            self.tableView.delegate = self
            self.tableView.dataSource = self
            self.tableView.rowHeight = UITableView.automaticDimension
            self.tableView.separatorStyle = .none
            self.tableView.tableFooterView = UIView()
            self.tableView.backgroundColor = Colors.shared.background
            
            let top = NSLayoutConstraint(item: self.tableView, attribute: .top, relatedBy: .equal, toItem: self.topView, attribute: .bottom, multiplier: 1, constant: Sizes.marginTop)
            let bottom = NSLayoutConstraint(item: self.tableView, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: 0)
            let leading = NSLayoutConstraint(item: self.tableView, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.tableView, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([top, bottom, leading, trailing])
        }
        tableViewSet()
    }
}
