//
//  PurchaseVC.swift
//  M.A.
//
//  Created by Min-Su Kim on 2020/08/09.
//  Copyright © 2020 Min-Su Kim. All rights reserved.
//

import Foundation
import UIKit
import StoreKit
import Lottie

class PurchaseVC: UIViewController, TopViewBackButtonDelegate, SKProductsRequestDelegate, SKPaymentTransactionObserver {
    private var loadingAnim: LoadingAnim!
    private var productAnimationView: AnimationView!
    private let topView = TopView()
    private let centerStackView = UIStackView()
    private let productView = UIView()
    private let productTextStackView = UIStackView()
    private let productNameLabel = UILabel()
    private let productExplainlabel = UILabel()
    private let purchaseButtonView = UIView()
    private let purchaseButton = UIButton(type: .system)

    private var product: SKProduct?
    private var productArray = Array<SKProduct>()
    private var productIDS = Set(["UnlimietedPass2"])
    
    private var message: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Colors.shared.backgroundSub
        topView.show(toView: view)
        topView.showCloseButton()
        topView.backButtonDelegate = self
        
        productAnimationView = AnimationView(name: "product")
        productAnimationView.contentMode = .scaleAspectFit
        productAnimationView.tintColor = Colors.shared.point
        productAnimationView.loopMode = .loop
        productAnimationView.play()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        DispatchQueue.main.async(execute: {
            self.setLayout()
            self.loadingAnim = LoadingAnim(view: self.view)
            self.loadingAnim.setColor(color: Colors.shared.buttonSub)
            SKPaymentQueue.default().add(self)
            self.requestProductData()
            
            if let message = self.message {
                let alert = UIAlertController(title: message, message: nil, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        })
    }
    
    @objc private func pressPurchaseButton(sender: UIButton) {
        EffectManager.vibrate()
        
        var payment: SKPayment!
        for value in productArray {
            if value.productIdentifier == "UnlimietedPass2" {
                payment = SKPayment(product: value)
                break
            }
        }
        SKPaymentQueue.default().add(payment)
    }
    
    func setMessage(message: String) {
        self.message = message
    }
    
    func pressBackButton() {
        dismiss(animated: true, completion: nil)
    }
    
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        loadingAnim.stop()
            
        let products = response.products
        if products.count != 0 {
            for value in products {
                self.product = value
                self.productArray.append(product!)
            }
        } else {
            let alert = UIAlertController(title: NSLocalizedString("CannotLoadItems", comment: ""), message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
        }
            
        let invalidProducts = response.invalidProductIdentifiers
        for pro in invalidProducts {
            print("Produc not founc : \(pro)")
        }

        DispatchQueue.main.async(execute: {
            for value in self.productArray {
                if value.productIdentifier == "UnlimietedPass2" {
                    self.productNameLabel.text = value.localizedTitle
                    self.productExplainlabel.text = value.localizedDescription
                    self.purchaseButton.setTitle("\(value.priceLocale.currencySymbol!) \(value.price)", for: .normal)
                }
            }
        })
    }
        
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        DispatchQueue.main.async {
            for transaction in transactions as [SKPaymentTransaction] {
                switch transaction.transactionState {
                case .purchased:
                    self.loadingAnim.stop()
                    SKPaymentQueue.default().finishTransaction(transaction)
                    self.deleverProduct()
                case .purchasing:
                    self.loadingAnim.start()
                case .failed:
                    let alert = UIAlertController(title: NSLocalizedString("PurchaseFailed", comment: ""), message: nil, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    self.loadingAnim.stop()
                    SKPaymentQueue.default().finishTransaction(transaction)
                case .deferred:
                    self.loadingAnim.stop()
                    SKPaymentQueue.default().finishTransaction(transaction)
                case .restored:
                    self.loadingAnim.stop()
                    SKPaymentQueue.default().finishTransaction(transaction)
                @unknown default:
                    self.loadingAnim.stop()
                    fatalError()
                }
            }
        }
    }
    
    private func deleverProduct() {
        loadingAnim.stop()
        
        DispatchQueue.global().async(execute: {
            ProductManager.deleverProduct(product: "unlimited", result: {
                error in
                self.loadingAnim.stop()
                
                if let error = error {
                    let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                    DispatchQueue.main.async(execute: {
                        self.present(alert, animated: true, completion: nil)
                    })
                } else {
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+1.0, execute: {
                        SKStoreReviewController.requestReview()
                        self.finishPurchase()
                    })
                }
            })
        })
    }
    
    private func requestProductData() {
        if SKPaymentQueue.canMakePayments() {
            let request = SKProductsRequest(productIdentifiers: productIDS)
            loadingAnim.start()
            request.delegate = self
            request.start()
        } else {
            let alert = UIAlertController(title: NSLocalizedString("PleaseCheckInAppSetting", comment: ""), message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .default, handler: {
                (_) in
                let url: NSURL? = NSURL(string: UIApplication.openSettingsURLString)
                if url != nil {
                    UIApplication.shared.open(url! as URL, options: [:], completionHandler: nil)
                }
            }))
            present(alert, animated: true, completion: nil)
        }
    }
    
    private func finishPurchase() {
        dismiss(animated: true, completion: nil)
    }
    
    private func setLayout() {
        let centerStackViewSet = {
            self.view.addSubview(self.centerStackView)
            self.centerStackView.translatesAutoresizingMaskIntoConstraints = false
            self.centerStackView.addArrangedSubview(self.productView)
            self.centerStackView.addArrangedSubview(self.productTextStackView)
            self.centerStackView.addArrangedSubview(self.purchaseButtonView)
            self.centerStackView.axis = .vertical
            self.centerStackView .distribution = .fill
            
            let centerY = NSLayoutConstraint(item: self.centerStackView, attribute: .centerY, relatedBy: .equal, toItem: self.view, attribute: .centerY, multiplier: 1, constant: 0)
            let leading = NSLayoutConstraint(item: self.centerStackView, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: Sizes.margin3)
            let trailing = NSLayoutConstraint(item: self.centerStackView, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: -Sizes.margin3)
            NSLayoutConstraint.activate([centerY, leading, trailing])
        }
        centerStackViewSet()
        
        let productImageViewSet = {
            self.productView.addSubview(self.productAnimationView)
            self.productAnimationView.translatesAutoresizingMaskIntoConstraints = false
          
            let width = NSLayoutConstraint(item: self.productAnimationView!, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 150)
            let height = NSLayoutConstraint(item: self.productAnimationView!, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 150)
            let top = NSLayoutConstraint(item: self.productAnimationView!, attribute: .top, relatedBy: .equal, toItem: self.productView, attribute: .top, multiplier: 1, constant: 0)
            let bottom = NSLayoutConstraint(item: self.productAnimationView!, attribute: .bottom, relatedBy: .equal, toItem: self.productView, attribute: .bottom, multiplier: 1, constant: -Sizes.marginTop)
            let centerX = NSLayoutConstraint(item: self.productAnimationView!, attribute: .centerX, relatedBy: .equal, toItem: self.productView, attribute: .centerX, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([width, height, top, bottom, centerX])
        }
        productImageViewSet()
        
        let productTextStackViewSet = {
            self.productTextStackView.addArrangedSubview(self.productNameLabel)
            self.productTextStackView.addArrangedSubview(self.productExplainlabel)
            self.productTextStackView.axis = .vertical
            self.productTextStackView.distribution = .fill
            self.productTextStackView.spacing = Sizes.margin1*0.5
        }
        productTextStackViewSet()
        
        let productNameLabelSet = {
            self.productNameLabel.textColor = Colors.shared.black
            self.productNameLabel.textAlignment = .center
            
            if Sizes.isiPad() {
                self.productNameLabel.font = UIFont.systemFont(ofSize: 25, weight: .bold)
            } else {
                self.productNameLabel.font = Fonts.title
            }
            
        }
        productNameLabelSet()
        
        let productExplanLabelSet = {
            self.productExplainlabel.textColor = Colors.shared.textSub
            self.productExplainlabel.textAlignment = .center
            self.productExplainlabel.numberOfLines = 0
            
            if Sizes.isiPad() {
                self.productExplainlabel.font = UIFont.systemFont(ofSize: 20, weight: .medium)
            } else {
                self.productExplainlabel.font = Fonts.type1
            }
        }
        productExplanLabelSet()
        
        let purchaseButtonSet = {
            self.purchaseButtonView.addSubview(self.purchaseButton)
            self.purchaseButton.translatesAutoresizingMaskIntoConstraints = false
            self.purchaseButton.addTarget(self, action: #selector(self.pressPurchaseButton(sender:)), for: .touchUpInside)
            self.purchaseButton.backgroundColor = Colors.shared.backgroundSub
            self.purchaseButton.layer.cornerRadius = Sizes.radius
            self.purchaseButton.setTitleColor(Colors.shared.point, for: .normal)
            self.purchaseButton.layer.shadowColor = UIColor.black.cgColor
            self.purchaseButton.layer.shadowOpacity = 0.1
            self.purchaseButton.layer.shadowRadius = 16
            self.purchaseButton.layer.shadowOffset = CGSize(width: 0, height: 8)
            
            let height: NSLayoutConstraint
            
            if Sizes.isiPad() {
                height = NSLayoutConstraint(item: self.purchaseButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Sizes.button*1.5)
                self.purchaseButton.titleLabel?.font = UIFont.systemFont(ofSize: 32, weight: .bold)
            } else {
                self.purchaseButton.titleLabel?.font = UIFont.systemFont(ofSize: 25, weight: .bold)
                height = NSLayoutConstraint(item: self.purchaseButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Sizes.button)
            }
            

            let top = NSLayoutConstraint(item: self.purchaseButton, attribute: .top, relatedBy: .equal, toItem: self.purchaseButtonView, attribute: .top, multiplier: 1, constant: Sizes.marginTop)
            let bottom = NSLayoutConstraint(item: self.purchaseButton, attribute: .bottom, relatedBy: .equal, toItem: self.purchaseButtonView, attribute: .bottom, multiplier: 1, constant: 0)
            let leading = NSLayoutConstraint(item: self.purchaseButton, attribute: .leading, relatedBy: .equal, toItem: self.purchaseButtonView, attribute: .leading, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.purchaseButton, attribute: .trailing, relatedBy: .equal, toItem: self.purchaseButtonView, attribute: .trailing, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([height, top, bottom, leading, trailing])
        }
        purchaseButtonSet()
    }
}
